@section('content')

<h3>Registro de Pasajes</h3><hr/>

<div class="content">
    {{ Form::open(array('action' => 'BeneficiosController@postPasaje', 'method' => 'POST')) }}
	{{ Form::hidden('id_paciente',$id) }}
	<div class="row">
		<div class="form-group col-md-3">
			{{ Form::label('origen', 'Origen') }}
			{{ Form::text('origen', null, array('placeholder' => 'Origen...', 'class' => 'form-control')) }}
			<p class="alert-danger">{{$errors->first("origen")}}</p>
		</div>
		<div class="form-group col-md-3">
			{{ Form::label('destino', 'Destino (Opcional)') }}
			{{ Form::text('destino', null, array('placeholder' => 'Destino...', 'class' => 'form-control')) }}
			<p class="alert-danger">{{$errors->first("destino")}}</p>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-3">
			{{ Form::label('fecha', 'Fecha') }}
			{{ Form::text('fecha', null, array('placeholder' => 'dd/mm/aaaa', 'class' => 'form-control datepicker' , 'autocomplete' => 'off')) }}
			<p class="alert-danger">{{$errors->first("fecha")}}</p>
		</div>
		<div class="form-group col-md-3">
			{{ Form::label('cantidad', 'Cantidad de Pasajes') }}
			{{ Form::number('cantidad', null, array('placeholder' => 'cantidad', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
			<p class="alert-danger">{{$errors->first("cantidad")}}</p>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-3">
			{{ Form::label('monto', 'Monto Total (Opcional)') }}
			{{ Form::text('monto', null, array('placeholder' => '$ 0.00', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
			<p class="alert-danger">{{$errors->first("monto")}}</p>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-lg-4">
			{{Form::label('tipo_pasaje','Tipo de Pasaje')}}
			<br>
			<!--<div class="radio-inline">-->
				<label class="radio">
					
					{{ Form::radio('tipo_pasaje', 'urbano',true) }}
					Urbano
				</label>
				<label class="radio">
					
					{{ Form::radio('tipo_pasaje', 'interurbano') }}
					Interurbano
				</label>
				<label class="radio">
					
					{{ Form::radio('tipo_pasaje', 'larga_distancia') }}
					Larga Distancia
				</label>
			<!--</div>-->
		</div>
	</div>
	
	
	
	<div class="row">
		<div class="form-group col-md-3">
			{{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}
			<?php echo HTML::link('pacientes/beneficios/'.$id.'','Cancelar',['class' => 'btn btn-default']) ?>
		</div>
	</div>
	{{ Form::close() }}
</div>
@show

@section('js')
<script>
	$(function(){
	  $(".tablesorter").tablesorter();
	});
</script>
@stop