<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAlimentosAsignados extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('alimentos_asignados', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('paciente_id');
			$table->date('fecha_otorgamiento');
			$table->integer('cantidad');
			$table->string("observaciones",400);

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists("alimentos_asignados");
	}

}
