@section('content')

<h3>¿Qué desea hacer con el paciente {{ $paciente->nombre }} - DNI: {{ $paciente->dni }}?</h3><hr/>

<div class="content">
	
	<div class="row">
		<div class="form-group col-md-3">
			<div class="btn-group-vertical">
				<a href="/beneficios/donacionycompra/{{ $paciente->id }}"><button type="button" style="width: 290px;" class="btn btn-success btn-lg pull-left"><i class="fa fa-shopping-cart"></i> Registrar Donación / Compra</button></a>
				<a href="/beneficios/alimento/{{ $paciente->id }}"><button  type="button" style="width: 290px;" class="btn btn-success btn-lg pull-left"><i class="fa fa-apple "></i> Dar Alimento</button></a>
				<a href="/beneficios/pasaje/{{ $paciente->id }}"><button type="button" style="width: 290px;" class="btn btn-success btn-lg pull-left"><i class="fa fa-automobile"></i> Dar Pasaje</button></a>
				<a href="/beneficios/ropero/{{ $paciente->id }}"><button type="button" style="width: 290px;" class="btn btn-success btn-lg pull-left"><i class="fa fa-mortar-board"></i> Dar Ropero</button></a>
				<a href="/beneficios/juguete/{{ $paciente->id }}"><button type="button" style="width: 290px;" class="btn btn-success btn-lg pull-left"><i class="fa fa-gift"></i> Dar Juguete</button></a>
				<a href="/beneficios/prestamo/{{ $paciente->id }}"><button type="button" style="width: 290px;" class="btn btn-success btn-lg pull-left"><i class="fa fa-pencil-square-o"></i> Registrar Pr&eacute;stamo</button></a>
				<a href="/beneficios/tramite/{{ $paciente->id }}"><button type="button" style="width: 290px;" class="btn btn-success btn-lg pull-left"><i class="fa fa-list-alt"></i> Registrar Otros Tr&aacute;mites</button></a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			{{ HTML::link("pacientes","Ir a Pacientes",array("class"=>"btn btn-default btn-group-lg")) }}
		</div>
	</div>
	<br>
      
</div>
@stop