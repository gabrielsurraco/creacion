<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableColaboradoresCreate extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('colaboradores', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nombre', 100);
			$table->string('dni', 20);
			$table->date('fecha_ingreso');
			$table->date('fecha_nacimiento');
			$table->string('profesion', 150)->nullable();
			$table->string('domicilio', 150)->nullable();
			$table->integer('localidad_id')->nullable();
			$table->string('tel_contacto', 40)->nullable();
			$table->boolean('es_socio')->default(false);
			$table->boolean('es_voluntario')->default(false);
			$table->boolean('es_benefactor')->default(false);
//			$table->enum('tipo_colaborador', array('ninguno', 'primaria', 'secundaria', 'terciario', 'universitario'));
			$table->string('email', 100);
			$table->string('lugar_de_pago', 150);
			$table->string('colabora_en', 400)->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists("colaboradores");
	}

}
