@section('content')

<h3>Registro de Préstamos de Elementos</h3><hr/>

<div class="content">
	
	{{ Form::open(array('action' => 'BeneficiosController@postPrestamo', 'method' => 'POST')) }}
	
	@if(isset($es_colaborador) && $es_colaborador == true)
		{{ Form::hidden('es_colaborador',$es_colaborador) }}
	@endif
	
	{{ Form::hidden('id',$id) }}
	<div class="row">
		<div class="form-group col-md-6">
			{{ Form::label('descripcion', 'Descripción') }}
			{{ Form::textarea('descripcion', null, array('placeholder' => 'Descripción del elemento...', 'class' => 'form-control' , 'autocomplete' => 'off', 'rows' => '2')) }}
			<p class="alert-danger">{{$errors->first("descripcion")}}</p>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-3">
			{{ Form::label('fecha_salida', 'Fecha de Salida') }}
			{{ Form::text('fecha_salida', null, array('placeholder' => 'dd/mm/aaaa', 'class' => ' form-control datepicker' , 'autocomplete' => 'off')) }}
			<p class="alert-danger">{{$errors->first("fecha_salida")}}</p>
		</div>
		<div class="form-group col-md-3">
			{{ Form::label('cantidad', 'Cantidad') }}
			{{ Form::number('cantidad', null, array('placeholder' => 'cantidad', 'class' => ' form-control' , 'autocomplete' => 'off')) }}
			<p class="alert-danger">{{$errors->first("cantidad")}}</p>
		</div>
	</div>
	
	<div class="row">
		<div class="form-group col-md-3">
			{{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}
			<?php echo HTML::link('pacientes/beneficios/'.$id.'','Cancelar',['class' => 'btn btn-default']) ?>
		</div>
	</div>
	{{ Form::close() }}
</div>
@show

@section('js')
<script>
	$(function(){
	  $(".tablesorter").tablesorter();
	});
</script>
@stop