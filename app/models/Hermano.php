<?php

class Hermano extends \LaravelBook\Ardent\Ardent  {

	protected $table = 'hermanos';

	protected $guarded = array();

	public static $rules = array(
		'nombre' => 'required',
		'dni' => 'required'
	);

	// <editor-fold desc="Relaciones">
    public function paciente()
    {
        return $this->belongsTo('Paciente', 'paciente_id');
    }
	

	// </editor-fold>
	public function setFechaNacimientoAttribute($value) {
		if(empty($value))
			$this->attributes['fecha_nacimiento'] = date("Y-m-d");
		elseif ( preg_match("/^[0-9]{2}\\/[0-9]{2}\\/[0-9]{4}$/", $value ) )
			$this->attributes['fecha_nacimiento'] = DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
	}
	
	public function getFechaNacimientoAttribute() {
		if(!isset($this->attributes['fecha_nacimiento']))
			return "";
		return DateTime::createFromFormat('Y-m-d', $this->attributes['fecha_nacimiento'])->format('d/m/Y');
//		return '15/10/2014';
	}


}