@section('content')

<h3>Administraci&oacute;n de Colaboradores</h3><hr/>

<div class="content">
    <div class="row">
        <div class="col-lg-8">
            @if(Auth::user()->es_admin)
                {{HTML::link("colaboradores/crear","Registrar Colaborador",array("class"=>"btn btn-primary"))}}
            @else
                {{HTML::link("#","Registrar Colaborador",array("class"=>"btn btn-primary disabled"))}}
            @endif
            
        </div>
    </div>
	<br/>
	{{ Form::open(array('action' => 'ColaboradoresController@getIndex', 'method' => 'POST')) }}
		<div class="row">
			<div class="form-group col-md-3">
				{{ Form::label('nombre', 'Apellido y Nombre') }}
				{{ Form::text('nombre', null , array('placeholder' => 'Colaborador a buscar..', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
			</div>
			<div class="form-group col-md-3">
				{{ Form::label('dni', 'Número de Documento') }}
				{{ Form::text('dni', null , array('placeholder' => 'Ingrese el DNI...', 'class' => 'form-control')) }}
			</div>
			<div class="form-group col-md-3">
				{{ Form::label('colabora_en', 'Colabora en') }}
				{{ Form::text('colabora_en', null , array('placeholder' => 'Colaborador en...', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
			</div>
		</div>
	
		<div class="row">
			<div class="form-group col-md-6">
				{{ Form::label('colaborador', 'Tipo de Colaborador') }}
				<div class="checkbox-inline">
					<label class="checkbox-inline">
								{{ Form::checkbox('es_socio', true , isset($filtros["es_socio"]) ? true : false) }}
							Socio
					</label>
					<label class="checkbox-inline">
								{{ Form::checkbox('es_voluntario', true , isset($filtros["es_voluntario"]) ? true : false) }}
							Voluntario
					</label>
					<label class="checkbox-inline">
								{{ Form::checkbox('es_benefactor', true , isset($filtros["es_benefactor"]) ? true : false) }}
							Benefactor
					</label>
				</div>

			</div>
		</div>
	<div class="row">
		<div class="form-group col-md-3">
			<button type="submit" class="btn btn-default "><i class="fa fa-search"></i> Buscar</button>
		</div>
	</div>
	
	{{ Form::close() }}
	
	<div class="row">
		<div class="col-lg-12">
			@if(isset($colaboradores[0]))
			<a href="colaboradores/index/1"  style="margin-bottom: 10px;"  class="btn btn-sm btn-success pull-right"> <i class="fa fa-file-excel-o "> </i>  Exportar Resultados</a>
			@else
				<a href="#" style="margin-bottom: 10px;"  class="btn btn-sm btn-success pull-right disabled"> <i class="fa fa-file-excel-o "> </i>  Exportar Resultados</a>
			@endif
            <div class="table-responsive">
                <table class="table table-hover table-striped tablesorter">
                    <thead>
                        <tr>
                            <th>Apellido y Nombre</th>
                            <th>DNI</th>
                            <th>Tel&eacute;fono</th>
                            <th>Email</th>
                            <th>Colabora en</th>
                            <th>Acci&oacute;nes</th>
                        </tr>
                    </thead>
                    <tbody>
						@foreach($colaboradores as $colaborador)
                                <tr>     
                                    <td>{{ $colaborador->nombre }}</td>
                                    <td>{{ $colaborador->dni }}</td>
                                    <td>{{ $colaborador->tel_contacto }}</td>
                                    <td>{{ $colaborador->email }}</td>
                                    <td>{{ $colaborador->colabora_en }}</td>
                                    <td id="example">
										<a href="/colaboradores/info/{{ $colaborador->id }}"  class="btn btn-sm btn-default"> <i class="fa fa-eye"></i> Ver</a>
										@if(Auth::user()->es_admin)
											<a href="/colaboradores/editar/{{ $colaborador->id }}"  class="btn btn-sm btn-default"> <i class="fa fa-edit"></i> Editar</a>
											<a href="/colaboradores/agregarpago/{{ $colaborador->id }}"  class="btn btn-sm btn-primary"> <i class="fa fa-money"></i> Agregar Pago</a>
											<a href="/beneficios/prestamocolaborador/{{ $colaborador->id }}"  class="btn btn-sm btn-success"> <i class="fa fa-pencil-square-o"></i> Prestar</a>
										@else
											<a href="#"  class="btn btn-sm btn-default disabled"> <i class="fa fa-edit"></i> Editar</a>
											<a href="#"  class="btn btn-sm btn-primary disabled"> <i class="fa fa-money"></i> Agregar Pago</a>
											<a href="#"  class="btn btn-sm btn-success disabled"> <i class="fa fa-pencil-square-o"></i> Prestar</a>
										@endif
									</td>
                                </tr>
						@endforeach
                        
                    </tbody>
                </table>
                    <div class="text-center">{{ $colaboradores->links() }}</div> 
            </div>
        </div>
        
    </div>
	
</div>
@show

@section('js')
<script>
	$(function(){
	  $(".tablesorter").tablesorter();
	  
	  
	});
</script>
@stop