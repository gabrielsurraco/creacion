<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class Usuario extends Eloquent implements UserInterface, RemindableInterface{
	// <editor-fold desc="Configuración" defaultstate="collapsed">

	protected $guarded = array("id", "created_at", "updated_at");

	protected $table = 'usuarios';

	protected $hidden = array('password');
	
	
	// </editor-fold>

	// <editor-fold desc="Auth" defaultstate="collapsed">
	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	public function getRememberToken()
	{
		return $this->remember_token;
	}

	public function setRememberToken($value)
	{
		$this->remember_token = $value;
	}

	public function getRememberTokenName()
	{
		return 'remember_token';
	}
	// </editor-fold>

	//	// <editor-fold desc="Relaciones" defaultstate="collapsed">
//    public function inversiones()
//    {
//        return $this->hasMany('Inversion', 'usuario_id');
//    }
//
//    public function clientes()
//    {
//        return $this->hasMany('Cliente', 'vendedor_usuario_id');
//    }
//
//    public function acreedores()
//    {
//        return $this->hasMany('Cliente', 'cobrador_usuario_id');
//    }
//
//    public function prestamos()
//    {
//        return $this->hasManyThrough('Prestamo', 'Cliente', 'vendedor_usuario_id');
//    }
//
//    public function cuentas()
//    {
//        return $this->hasMany('Cuenta', 'usuario_id');
//    }
//
//    public function cajas()
//    {
//        return $this->hasMany('Caja', 'usuario_id');
//    }
//
//	/**
//	 * Retorna pagos cobrados a clientes
//	 * @method
//	 */
//    public function cobros()
//    {
//        return $this->hasMany('Pago', 'cobrador_usuario_id');
//    }
//	// </editor-fold>

}
