<?php

class DiagnosticosTableSeeder extends Seeder {

	public function run() {
		DB::table('diagnosticos')->delete();
		Diagnostico::create(array(
			'nombre' => 'Diagnóstico 1',
		));
		
		Diagnostico::create(array(
			'nombre' => 'Diagnóstico 2',
		));
		
		Diagnostico::create(array(
			'nombre' => 'Diagnóstico 3',
		));
		
	}

}
