<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Helpers
 *
 * @author Ing. Gabriel Surraco
 */
class Helpers {
	public static function doMessage() {
        $message = 'Hello';
        return $message;
    }
	
	public static function diferenciaDias($fecha){
		if( preg_match("/^[0-9]{2}\\/[0-9]{2}\\/[0-9]{4}$/", $fecha )){
			return "Agregar Fecha Diagnóstico";
		}
		$datetime1 = new DateTime(DateTime::createFromFormat('d/m/Y', $fecha)->format('Y-m-d'));
		$datetime2 = new DateTime(date('Y-m-d'));
		$interval = $datetime1->diff($datetime2);
		
		return $interval->format('%r%a')." D&iacute;as";
	}
	
	public static function getDiagnostico($id){
		if($id == 0){
			$value = "N/A";
		}else{
			$d = Diagnostico::findOrFail($id);
			$value = $d->nombre;
		}
		
		return $value;
	}
	
	public static function getLugarTratamiento($id){
		if($id == 0){
			$value = "N/A";
		}else{
			$d = Instituto::findOrFail($id);
			$value = $d->nombre;
		}
		
		return $value;
	}
	
	public static function getLocalidad($id){
		if($id == 0){
			$value = "N/A";
		}else{
			$d = Localidad::findOrFail($id);
			$value = $d->nombre;
		}
			
		return $value;
	}
	
}
