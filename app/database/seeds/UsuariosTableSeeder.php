<?php

class UsuariosTableSeeder extends Seeder {

	public function run() {
		DB::table('usuarios')->delete();
		Usuario::create(array(
			'email' => 'gabriel_surraco@hotmail.com',
			'password' => Hash::make('admin'),
			'nombre' => 'Gabriel Surraco',
			'es_admin' => true
		));
		
		Usuario::create(array(
			'email' => 'creacionchicosoncologicos.mnes@gmail.com',
			'password' => Hash::make('admin'),
			'nombre' => 'Nadia Chávez',
			'es_default' => true
		));

	}

}
