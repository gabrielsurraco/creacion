@section('content')

<h3>Agregar pago de {{$colaborador->nombre}} - DNI: {{$colaborador->dni}}</h3><hr/>

<div class="content">
	{{ Form::open(array('method' => 'POST'), array('role' => 'form')) }}
		{{ Form::hidden('id', $id) }}
	<div class="row">
		<div class="form-group col-md-3">
			{{ Form::label('fecha_pago', 'Fecha de Pago') }}
			{{ Form::text('fecha_pago', empty($colaborador->id) ? null : $colaborador->fecha_pago , array('placeholder' => 'Fecha de Pago', 'class' => 'form-control datepicker' , 'autocomplete' => 'off')) }}
			<p class="alert-danger">{{$errors->first("fecha_pago")}}</p>
		</div>
		<div class="form-group col-md-3">
			{{ Form::label('monto', 'Monto (Opcional)') }}
			{{ Form::text('monto',empty($colaborador->id) ? null : $colaborador->monto, array('placeholder' => '$', 'class' => 'form-control', 'autocomplete' => 'off')) }}
			<p class="alert-danger">
				{{$errors->first("monto")}}
			</p>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-6">
			{{ Form::label('observaciones', 'Observaciones') }}
			{{ Form::textarea('observaciones', empty($colaborador->id) ? null : $colaborador->observaciones, array('placeholder' => 'Descripcion...', 'class' => 'form-control' , 'autocomplete' => 'off', 'rows' => '4')) }}
		</div>
	</div>
       
        
            {{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}
            {{ HTML::link("colaboradores","Cancelar",array("class"=>"btn btn-default")) }}
	{{ Form::close() }}
   
</div>
@stop