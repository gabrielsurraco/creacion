<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<link rel="icon" href="{{ URL::to('images/favicon.ico') }}" />
    <title>{{ $title or 'Administración' }}</title>

    {{ HTML::style('css/bootstrap.css') }}
    {{ HTML::style('css/sb-admin.css') }}
    {{ HTML::style('font-awesome/css/font-awesome.min.css') }}
	
    {{ HTML::style('css/style.css') }}

    {{ HTML::style('css/datepicker.css') }}
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<!--{{ HTML::style('css/newicons.min.css') }}-->
  </head>

  <body>

    <div id="wrapper">

      <!-- Sidebar -->
	  @section('sidebar')
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">{{ $title_header or 'Sistema Creación' }}</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav side-nav">
			@if ($usuario->es_admin or $usuario->es_default)
				<li><a href="{{ url('/dashboard/principal') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			@endif
			
			
			@if ($usuario->es_admin or $usuario->es_default)
				<li><a href="{{ url('/notas') }}"><i class="fa fa-file-text"></i> Notas</a></li>
			@endif
			
			@if ($usuario->es_admin or $usuario->es_default)
				<li><a href="{{ url('/colaboradores') }}"><i class="fa fa-male"></i> Colaboradores</a></li>
			@endif

			@if ($usuario->es_admin or $usuario->es_default)
				<li><a href="{{ url('/pacientes') }}"><i class="fa fa-child"></i> Pacientes</a></li>
			@endif
			
			@if ($usuario->es_admin or $usuario->es_default)
				<li><a href="{{ url('/stock') }}"><i class="fa fa-cubes"></i> Stock</a></li>
			@endif

			@if ($usuario->es_admin)
				<li><a href="{{ url('/usuarios') }}"><i class="fa fa-users"></i> Usuarios</a></li>
			@endif
			@if ($usuario->es_admin)
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-cog"></i> Configuraci&oacute;n <b class="caret"></b>
					</a>
					<ul class="dropdown-menu">
						<li><a href="{{ url('/diagnosticos') }}">Diagn&oacute;sticos</a></li>
						<li><a href="{{ url('/institutos') }}">Institutos</a></li>
						<li><a href="{{ url('/localidades') }}">Localidades</a></li>
					</ul>
				</li>
			@endif
          </ul>

          <ul class="nav navbar-nav navbar-right navbar-user">

			  <li class="dropdown user-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{ $usuario->nombre }}<b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="{{ url('/usuarios/editar/' . $usuario->id) }}"><i class="fa fa-user"></i> Mi Cuenta</a></li>
				<!--
				<li><a href="#"><i class="fa fa-envelope"></i> Inbox <span class="badge">7</span></a></li>
                <li><a href="#"><i class="fa fa-gear"></i> Settings</a></li>
				-->
                <li class="divider"></li>
                <li><a href="{{ url('/logout') }}"><i class="fa fa-power-off"></i> Salir</a></li>
              </ul>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </nav>
	  @show

      <div class="container-fluid">
           
            @if(Session::has('info'))        
                <div class="alert alert-info alert-dismissable">
                       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                       {{ Session::get('info') }}
                </div>  
            @endif
            
            @if(Session::has('success')) 
                <div class="alert alert-success alert-dismissable">
                       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                       {{ Session::get('success') }}
                </div>
            @endif
            
            @if(Session::has('warning'))
                <div class="alert alert-warning alert-dismissable">
                       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                       {{ Session::get('warning') }}
                </div>
            @endif

            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissable">
                       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                       {{ Session::get('error') }}
                </div>
            @endif
              
            @yield('content')
      </div><!-- /#page-wrapper -->
	  
    </div><!-- /#wrapper -->

	{{-- JavaScript --}}
	
    {{ HTML::script('js/jquery-1.10.2.js')}}
    {{ HTML::script('js/bootstrap.js')}}
    {{ HTML::script('js/jquery.tablesorter.min.js')}}
    {{ HTML::script('js/bootsrap-datepicker.js')}}
    {{ HTML::script('js/jquery.ba-throttle-debounce.min.js')}}
    <!--{{ HTML::script('js/bootstrap-tooltip.js')}}-->
	<!--{{ HTML::script('js/bootstrap-confirmation.js')}}-->
 

	@yield('js')
        
    <script>
        $(function() {
            $(".datepicker").datepicker({
              format: 'dd/mm/yyyy'
            });
			

			// Manejo de números decimales
			$(".decimal").on("keypress", function(e){
				v = $(this).val();
				// ,
				if(e.key == ","){
					if(v.indexOf(".")===-1)
						$(this).val(v + ".");
					
					return false;
				}
				// .
				if(e.key == "."){
					if(v.indexOf(".")!==-1)
						return false;
				}
				return true;
			});
        });
    </script>
        
  </body>
</html>