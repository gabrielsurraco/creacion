<?php

class InstitutosController extends BaseController {
	
    public function getIndex()
    {
        $institutos = Instituto::all();
        $this->layout->content = View::make('institutos/index',compact("institutos"));
    }
    
    public function getEditar($id = null)
    {
		
		$usuario = Auth::user();
		
		if (!($usuario->es_admin)) {
			return Redirect::to('institutos')->with('error', 'Acceso no permitido');
		}
		
        $view = View::make('institutos/registro');
        
        if(empty($id)){
            $view->with(array(
                "accion" => "add"
                , "id" => ""
            ));
        }else{
            $datos = Instituto::findOrFail($id);
            $accion = "update";
            $view->with(compact("datos", "id", "accion"));
        }
        
        $this->layout->content = $view;
    }
////    
    // Editar un registro
    public function postEditar($id=null){
		$usuario = Auth::user();
		
		if (!($usuario->es_admin)) {
			return Redirect::to('institutos')->with('error', 'Acceso no permitido');
		}
		
        $inputs = Input::All();
        
        $reglas = array(
            'nombre'=>'required'
        );
        
        $mensajes = array(
            'required'=>'Debe completar el campo!'
        );
        
        $validar=Validator::make($inputs,$reglas,$mensajes);
        
        if($validar->fails()){            
            return Redirect::back()->withErrors($validar)->withInput();
        }else{
            if(empty($id))
                $instituto= new Instituto();
            else
                $instituto = Instituto::findOrFail($id);
            
            $instituto->nombre = $inputs['nombre'];
           
            $instituto->save();
            Session::flash('success', 'El registro ha sido guardado exitosamente!');
			if(Auth::user()->es_admin)
				return Redirect::to('/institutos');
			else
				return Redirect::to('/home');
        }
    }
//    
    public function getEliminar($id) {
		$usuario = Auth::user();
		
		if (!($usuario->es_admin)) {
			return Redirect::to('institutos')->with('error', 'Acceso no permitido');
		}
		
		$pacientes = Paciente::where("lugar_tratamiento_id" , $id)->get();
		if(count($pacientes) > 0){
			return Redirect::to('/institutos')->with('warning', 'El Instituto no puede ser eliminado debido a que esta siendo utilizado');
		}else{
			$instituto = Instituto::findOrFail($id);
			$instituto->delete();
			return Redirect::to('institutos')->with('info', 'El instituto ha sido eliminado correctamente.');
		}
		
    }

}

