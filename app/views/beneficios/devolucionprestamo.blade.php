@section('content')

<h3>Registro de Devolución</h3><hr/>

<div class="content">
    {{ Form::open(array('action' => 'BeneficiosController@postDevolucionprestamo', 'method' => 'POST')) }}
	{{ Form::hidden('prestamo_id',$prestamo[0]->id) }}
	<div class="row">
		<div class="form-group col-md-3">
			{{ Form::label('fecha_salida', 'Fecha de Salida') }}
			{{ Form::text('fecha_salida', $prestamo[0]->fecha_salida, array( 'disabled', 'class' => ' form-control' , 'autocomplete' => 'off')) }}
		</div>
		<div class="form-group col-md-3">
			{{ Form::label('cantidad', 'Cantidad') }}
			{{ Form::text('cantidad', $prestamo[0]->cantidad, array('disabled', 'class' => ' form-control' , 'autocomplete' => 'off')) }}
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-6">
			{{ Form::label('d', 'Descripción') }}
			{{ Form::textarea('d', $prestamo[0]->descripcion, array('disabled', 'class' => 'form-control' , 'autocomplete' => 'off', 'rows' => '2')) }}
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-3">
			{{ Form::label('descripcion_devolucion', 'Descripción de la devolución (Opcional)') }}
			{{ Form::textarea('descripcion_devolucion', null, array('placeholder' => 'Agregue alguna descripción del estado de los elementos...', 'class' => 'form-control' , 'autocomplete' => 'off', 'rows' => '4')) }}
			<p class="alert-danger">{{$errors->first("descripcion_devolucion")}}</p>
		</div>
		<div class="form-group col-md-3">
			{{ Form::label('fecha_devolucion', 'Fecha de Devolución') }}
			{{ Form::text('fecha_devolucion', null, array( 'placeholder' => 'dd/mm/aaaa', 'class' => ' form-control datepicker' , 'autocomplete' => 'off')) }}
			<p class="alert-danger">{{$errors->first("fecha_devolucion")}}</p>
		</div>
	</div>
	
	<div class="row">
		<div class="form-group col-md-3">
			{{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}
			<?php echo HTML::link('colaboradores','Cancelar',['class' => 'btn btn-default']) ?>
		</div>
	</div>
	{{ Form::close() }}
</div>
@show

@section('js')
<script>
	$(function(){
	  $(".tablesorter").tablesorter();
	});
</script>
@stop