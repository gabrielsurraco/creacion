<?php

//use Carbon\Carbon;

class PacientesController extends BaseController {

	public function generar_informe($filtros) {
//		var_dump($filtros);die;
		$query = Paciente::with('hermanos');

		if (!empty($filtros["nombre"])) {
			$query->where("nombre", "LIKE", "%" . $filtros["nombre"] . "%");
		}

		if (!empty($filtros["dni"])) {
			$query->where("dni", "=", $filtros["nombre"]);
		}

		if (!empty($filtros["diagnostico_id"]) && $filtros["diagnostico_id"] !== 'default') {
			$query->where("diagnostico_id", "=", $filtros["diagnostico_id"]);
		}

		if (!empty($filtros["localidad_id"]) && $filtros["localidad_id"] != 'default') {
			$query->where("localidad_id", "=", $filtros["localidad_id"]);
		}

		if (!empty($filtros["lugar_tratamiento_id"]) && $filtros["lugar_tratamiento_id"] != 'default') {
			$query->where("lugar_tratamiento_id", "=", $filtros["lugar_tratamiento_id"]);
		}

		if (!empty($filtros["estado_paleativo"]) && $filtros["estado_paleativo"] == 1) {
			$query->where("estado_paleativo", "=", 1);
		}

		if (!empty($filtros["prioridad_alta"]) && $filtros["prioridad_alta"] == 1) {
			$query->where("prioridad_alta", "=", 1);
		}

		if (!empty($filtros["prioridad_media"]) && $filtros["prioridad_media"] == 1) {
			$query->where("prioridad_media", "=", 1);
		}


		$pacientes = $query->orderBy("id", "desc")->get();

		Excel::create('Pacientes', function($excel) use($pacientes) {
			$excel->sheet('Sheetname', function($sheet) use($pacientes) {

				$i = 1;
				$p = array();
				foreach ($pacientes as $paciente) {

					$p[] = $paciente->nombre;
					$p[] = $paciente->fecha_ingreso;
					$p[] = $paciente->fecha_nacimiento;
					$p[] = $paciente->dni;
					$p[] = $paciente->nombre_padre;
					$p[] = $paciente->dni_padre;
					$p[] = $paciente->estudio_padre;
					$p[] = $paciente->estudio_padre_estado;
					$p[] = $paciente->nombre_madre;
					$p[] = $paciente->dni_madre;
					$p[] = $paciente->estudio_madre;
					$p[] = $paciente->estudio_madre_estado;
					$p[] = $paciente->nombre_tutor;
					$p[] = $paciente->dni_tutor;
					$p[] = $paciente->estudio_tutor;
					$p[] = $paciente->estudio_tutor_estado;
					$p[] = $paciente->ocupacion_vivienda;
					$p[] = $paciente->direccion;
					$p[] = $paciente->tel_contacto;
					$p[] = $paciente->cel_contacto;
					$p[] = $paciente->cel_contacto_2;
					$p[] = $paciente->medio_devida;
					$p[] = $paciente->distancia_camino;

					if (!empty($paciente->diagnostico_id) && $paciente->diagnostico_id !== 'default') {
						$p[] = Helpers::getDiagnostico($paciente->diagnostico_id);
					} else {
						$p[] = 'N/A';
					}
					if (!empty($paciente->lugar_tratamiento_id) && $paciente->lugar_tratamiento_id != 'default')
						$p[] = Helpers::getLugarTratamiento($paciente->lugar_tratamiento_id);
					else
						$p[] = 'N/A';

					if (!empty($paciente->localidad_id) && $paciente->localidad_id != 'default')
						$p[] = Helpers::getLocalidad($paciente->localidad_id);
					else
						$p[] = 'N/A';


					$p[] = ($paciente->techo_zinc == 1) ? "Si" : "No";
					$p[] = ($paciente->techo_carton == 1) ? "Si" : "No";
					$p[] = $paciente->techo_otro;
					$p[] = ($paciente->pared_madera == 1) ? "Si" : "No";
					$p[] = ($paciente->pared_material == 1) ? "Si" : "No";
					$p[] = $paciente->pared_otro;
					$p[] = ($paciente->piso_ceramica == 1) ? "Si" : "No";
					$p[] = ($paciente->piso_alisado == 1) ? "Si" : "No";
					$p[] = $paciente->piso_otro;
					$p[] = ($paciente->agua_pozo_balde == 1) ? "Si" : "No";
					$p[] = ($paciente->agua_canilla_publica == 1) ? "Si" : "No";
					$p[] = $paciente->agua_otro;
					$p[] = ($paciente->sanitario_interno == 1) ? "Si" : "No";
					$p[] = ($paciente->sanitario_externo == 1) ? "Si" : "No";
					$p[] = ($paciente->sanitario_completo == 1) ? "Si" : "No";
					$p[] = $paciente->nro_habitantes_casa;
					$p[] = $paciente->nro_habitaciones;
					$p[] = $paciente->nro_personas_que_trabajan;
					$p[] = ($paciente->trabajo_fijo == 1) ? "Si" : "No";
					$p[] = ($paciente->trabajo_temporario == 1) ? "Si" : "No";
					$p[] = ($paciente->trabajo_porsucuenta == 1) ? "Si" : "No";
					$p[] = (!empty($paciente->trasladado)) ? $paciente->trasladado : "No";
					$p[] = ($paciente->fallecio == 1) ? "Si" : "No";
					$p[] = ($paciente->estado_paleativo == 1) ? "Si" : "No";
					$p[] = ($paciente->prioridad_alta == 1) ? "Si" : "No";
					$p[] = ($paciente->prioridad_media == 1) ? "Si" : "No";
					$p[] = $paciente->observaciones;

					$sheet->row($i, $p);
					$sheet->row($i, function($row) {
						$row->setBackground('#D8D8D8');
					});

					$i++;
					$sheet->row($i, array("Hermano", 'DNI', 'Fecha Nacimiento'));
					$sheet->row($i, function($row) {
						$row->setFontWeight('bold');
					});
					foreach ($paciente["hermanos"] as $hermano) {
						$i++;
						$sheet->row($i, array($hermano->nombre, $hermano->dni, $hermano->fecha_nacimiento));
					}
					$i++;

					$p = array();
					$i++;
				}

				$sheet->prependRow(1, array(
					'NOMBRE DEL PACIENTE', 'FECHA DE INGRESO', 'FECHA NACIMIENTO', 'DNI', 'NOMBRE DEL PADRE', 'DNI DEL PADRE', 'ESTUDIOS DEL PADRE','ESTADO DE ESTUDIOS', 'NOMBRE DE LA MADRE', 'DNI DE LA MADRE', 'ESTUDIOS DE LA MADRE','ESTADO DE ESTUDIOS', 'NOMBRE DEL TUTOR', 'DNI DEL TUTOR', 'ESTUDIOS DEL TUTOR','ESTADO DE ESTUDIOS', 'OCUPACION DE VIVIENDA', 'DIRECCION', 'TELEFONO', 'CELULAR', 'CELULAR 2', 'MEDIO DE VIDA', 'DISTANCIA CAMINO TERRADO', 'DIAGNOSTICO', 'LUGAR DE TRATAMIENTO', 'LOCALIDAD', 'TECHO DE ZINC', 'TECHO DE CARTON', 'TECHO (OTRO)', 'PARED DE MADERA', 'PARED DE MATERIAL', 'PARED (OTRO)', 'PISO DE CERAMICA', 'PISO ALISADO', 'PISO (OTRO)', 'AGUA DE POZO', 'AGUA DE CANILLA PUBLICA', 'AGUA (OTRO)', 'SANITARIO INTERNO', 'SANITARIO EXTERNO', 'SANITARIO COMPLETO', 'NRO. HAB. EN LA CASA', 'NRO. DE HABITACIONES', 'PERSONAS QUE TRABAJAN', 'TRABAJO FIJO', 'TRABAJO TEMPORARIO', 'TRABAJO POR SU CUENTA', 'FUE TRASLADADO?', 'ESTADO PALEATIVO', 'PRIORIDAD ALTA', 'PRIORIDAD MEDIA', 'OBSERVACIONES'
				));

				$sheet->row(1, function($row) {
					$row->setFontWeight('bold');
				});
			});
		})->export('xls');
	}
	
//	public function getEliminar($id = null){
//		$usuario = Auth::user();
//
//		if (!($usuario->es_admin )) {
//			return Redirect::to('home')->with('error', 'Acceso no permitido');
//		}
//		
//		try {
//			// Inicio transaccion
//			DB::beginTransaction();
//
//			Hermano::where("id",$id)->delete();
//			Alimento::where("id",$id)->delete();
//			Derivacion::where("id",$id)->delete();
//			Donacionycompra::where("id",$id)->delete();
//			Pasaje::where("id",$id)->delete();
//			Prestamo::where("id",$id)->delete();
//			Ropero::where("id",$id)->delete();
//			Tramite::where("id",$id)->delete();
//			Paciente::where("id",$id)->delete();
//		
//			// Commit
//			DB::commit();
//			return Redirect::to('/pacientes')->with('success', 'Paciente eliminado correctamente!');
//		} catch (\PDOException $e) {
//			DB::rollBack();
//			throw $e;
//		}
//		
//	}

	public function getInfo($id = null) {
		$usuario = Auth::user();

		if (!($usuario->es_admin || $usuario->es_default)) {
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}

		$paciente = Paciente::with('hermanos', 'hermanos.paciente')->findOrFail($id);

		//SE OBTIENEN LOS ULTIMOS 6 ALIMENTOS DADOS Y LA CANTIDAD TOTAL
		$alimentos = Alimento::where("paciente_id", $id)->orderBy("fecha_otorgamiento", "DESC")->limit(10)->get();
		$alimentos_total = Alimento::select(DB::raw('SUM(cantidad) as cantidad_total'))->where("paciente_id", $id)->get();
		
		$asistencias = Asistencia::where("paciente_id", $id)->orderBy("fecha_cierre", "DESC")->get();

		//SE OBTIENEN LOS ULTIMOS 10 ROPEROS ASIGNADOS, Y EL TOTAL DE NUEVOS Y USADOS
		$roperos_total = Ropero::select("tipo", DB::raw('SUM(cantidad) as cantidad_total'))->where("paciente_id", $id)->groupBy("tipo")->get();
		$roperos = Ropero::where("paciente_id", $id)->orderBy("fecha_otorgamiento", "DESC")->limit(10)->get();
		
		//SE OBTIENEN LOS ULTIMOS 10 ROPEROS ASIGNADOS, Y EL TOTAL DE NUEVOS Y USADOS
		$juguetes_total = Juguete::select("tipo", DB::raw('SUM(cantidad) as cantidad_total'))->where("paciente_id", $id)->groupBy("tipo")->get();
		$juguetes = Juguete::where("paciente_id", $id)->orderBy("fecha_otorgamiento", "DESC")->limit(10)->get();

		//SE OBTIENEN LAS ULTIMAS 10 DONACIONES / COMPRAS 
		$donaciones_compras = Donacionycompra::where("paciente_id", $id)->orderBy("fecha", "DESC")->limit(10)->get();

		//SE OBTIENEN LOS ULTIMOS 10 PASAJES COMPRADOS
		$pasajes = Pasaje::where("paciente_id", $id)->orderBy("fecha", "DESC")->limit(10)->get();

		//SE OBTIENEN TODOS LOS PRESTAMOS ADEUDADOS
		$prestamos = Prestamo::where("paciente_id", $id)->where("colaborador_id", NULL)->where("fecha_devolucion", NULL)->orderBy("created_at", "DESC")->get();

		//SE OBTIENEN LOS ULTIMOS 10 TRAMITES
		$tramites = Tramite::where("paciente_id", $id)->orderBy("fecha", "DESC")->limit(10)->get();
		
		$derivaciones = Derivacion::where("paciente_id", $id)->orderBy("fecha", "DESC")->limit(10)->get();
		
		$this->layout->content = View::make('pacientes/info', compact(
								"paciente", "alimentos","asistencias", "alimentos_total", "roperos","derivaciones", "roperos_total","juguetes","juguetes_total", "donaciones_compras", "pasajes", "prestamos", "tramites"
						)
		);
	}

	public function getIndex($imprimir = null) {
		$usuario = Auth::user();

		if (!($usuario->es_admin || $usuario->es_default)) {
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}

		//Cargo los diagnosticos
		$diagnosticos = Diagnostico::all()->toArray();
		$lista_diagnosticos = array();
		foreach ($diagnosticos as $diagnostico) {
			$lista_diagnosticos[$diagnostico["id"]] = $diagnostico["nombre"];
		}

		//Cargo Localidades
		$localidades = Localidad::all()->toArray();
		$lista_localidades = array();
		foreach ($localidades as $localidad) {
			$lista_localidades[$localidad["id"]] = $localidad["nombre"];
		}

		//Cargo los institutos
		$institutos = Instituto::all()->toArray();
		$lista_institutos = array();
		foreach ($institutos as $instituto) {
			$lista_institutos[$instituto["id"]] = $instituto["nombre"];
		}

		if (isset($imprimir)) {
			if (Request::isMethod('post'))
				$filtros = Input::all();
			else
				$filtros = Session::get('pacientes.index.filtros', array());

			$this->generar_informe($filtros);
		}

		// Recupero filtros
		if (Request::isMethod('post')) {
			$filtros = Input::all();
		} else {
			if (Input::has('page')) {
				$filtros = Session::get('pacientes.index.filtros', array());
			} else {
				$filtros = array();
				Session::forget('pacientes.index.filtros');
			}
		}

		$query = Paciente::with('hermanos');

		if (!empty($filtros["nombre"])) {
			$query->where("nombre", "LIKE", "%" . $filtros["nombre"] . "%");
		}

		if (!empty($filtros["dni"])) {
			$query->where("dni", "=", $filtros["nombre"]);
		}

		if (!empty($filtros["diagnostico_id"]) && $filtros["diagnostico_id"] !== 'default') {
			$query->where("diagnostico_id", "=", $filtros["diagnostico_id"]);
		}

		if (!empty($filtros["localidad_id"]) && $filtros["localidad_id"] != 'default') {
			$query->where("localidad_id", "=", $filtros["localidad_id"]);
		}

		if (!empty($filtros["lugar_tratamiento_id"]) && $filtros["lugar_tratamiento_id"] != 'default') {
			$query->where("lugar_tratamiento_id", "=", $filtros["lugar_tratamiento_id"]);
		}

		if (!empty($filtros["estado_paleativo"]) && $filtros["estado_paleativo"] == 1) {
			$query->where("estado_paleativo", "=", 1);
		}
		
		if (!empty($filtros["fallecio"]) && $filtros["fallecio"] == 1) {
			$query->where("fallecio", "=", 1);
		}

		if (!empty($filtros["prioridad_alta"]) && $filtros["prioridad_alta"] == 1) {
			$query->where("prioridad_alta", "=", 1);
		}

		if (!empty($filtros["prioridad_media"]) && $filtros["prioridad_media"] == 1) {
			$query->where("prioridad_media", "=", 1);
		}


		$query->orderBy("id", "desc");

		$pacientes = $query->paginate(10);
//		$pacientes = $query->get();
		// Guardo filtro en sesion
		if (!Input::has('page') && count($filtros) > 0) {
			Session::put('pacientes.index.filtros', $filtros);
		}

		$this->layout->content = View::make('pacientes/index', compact("usuario", "lista_diagnosticos", "lista_localidades", "lista_institutos", "pacientes","filtros"));
	}

	public function postIndex() {
		return $this->getIndex();
	}

	public function getBeneficios($id = null) {
		$usuario = Auth::user();

		if (!($usuario->es_admin)) {
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}
		
		$paciente = Paciente::findOrFail($id);
		$this->layout->content = View::make('pacientes/beneficios', compact("paciente"));
	}

	public function getCrear() {
		$usuario = Auth::user();
		if (!($usuario->es_admin)) {
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}

		//Cargo los diagnosticos
		$diagnosticos = Diagnostico::all()->toArray();
		$lista_diagnosticos = array();
		foreach ($diagnosticos as $diagnostico) {
			$lista_diagnosticos[$diagnostico["id"]] = $diagnostico["nombre"];
		}

		//Cargo los institutos
		$institutos = Instituto::all()->toArray();
		$lista_institutos = array();
		foreach ($institutos as $instituto) {
			$lista_institutos[$instituto["id"]] = $instituto["nombre"];
		}

		//Cargo Localidades
		$localidades = Localidad::all()->toArray();
		$lista_localidades = array();
		foreach ($localidades as $localidad) {
			$lista_localidades[$localidad["id"]] = $localidad["nombre"];
		}

		$paciente = new Paciente();

//		Carbon::today()->addMonth()->day(1)->format("d/m/Y");

		$this->layout->content = View::make('pacientes.editar')->with(compact("paciente", "lista_diagnosticos", "lista_institutos", "lista_localidades"));
	}

//
	public function getEditar($id) {
		$usuario = Auth::user();

		// Validaciones
		if (!($usuario->es_admin)) {
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}

		//Cargo los diagnosticos
		$diagnosticos = Diagnostico::all()->toArray();
		$lista_diagnosticos = array();
		foreach ($diagnosticos as $diagnosabltico) {
			$lista_diagnosticos[$diagnosabltico["id"]] = $diagnosabltico["nombre"];
		}

		//Cargo los institutos
		$institutos = Instituto::all()->toArray();
		$lista_institutos = array();
		foreach ($institutos as $instituto) {
			$lista_institutos[$instituto["id"]] = $instituto["nombre"];
		}

		//Cargo Localidades
		$localidades = Localidad::all()->toArray();
		$lista_localidades = array();
		foreach ($localidades as $localidad) {
			$lista_localidades[$localidad["id"]] = $localidad["nombre"];
		}

		$paciente = Paciente::with('hermanos', 'hermanos.paciente')->findOrFail($id);

		$this->layout->content = View::make("pacientes.editar")->with(compact("paciente", "lista_diagnosticos", "lista_institutos", "lista_localidades"));
	}

//    
//    public function getInfo($id){
//        $prestamo = Prestamo::findOrFail($id);
//        
//        $cuotas = $prestamo->cuotas;
//        
//        $infoCuotas = array(
//			"cuotasVencidas" => 0,
//			"cuotasPagas" => 0,
//			"cuotasPorPagar" => 0,
//			"capitalAdeudado" => 0,
//			"montoAcumuladoPorMora" => 0
//		);
//        
//        foreach($cuotas as $cuota){
//            $fecha = Carbon::createFromFormat('Y-m-d', $cuota->vencimiento);
//            if(($fecha->isPast() and !$fecha->isToday()) and $cuota->estado == Cuota::ESTADO_PENDIENTE)
//                $infoCuotas["cuotasVencidas"]++;
//            
//            if($cuota->estado == Cuota::ESTADO_PAGADA)
//                $infoCuotas["cuotasPagas"]++;
//            
//            if($cuota->estado == Cuota::ESTADO_PENDIENTE){
//                $infoCuotas["cuotasPorPagar"]++;
//                $infoCuotas["capitalAdeudado"]+= $cuota->capital;
//                $infoCuotas["montoAcumuladoPorMora"]+= $cuota->mora;
//            }
//            
//        }
//        
//        $this->layout->content = View::make("prestamos.info",compact("prestamo", "infoCuotas"));
//    }
//
	public function postUpdate($id = null) {

		$usuario = Auth::user();

		if (!($usuario->es_admin)) {
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}

//		if($usuario->es_admin)
//			Paciente::activarValidacionesAdmin();



		$inputs = Input::except('hermanos');
		$inputs_hermanos = Input::get('hermanos');
//		var_dump($inputs);die;
		$reglas = array();

		$reglas['nombre'] = 'required';
		if ($id == null)
			$reglas['dni'] = 'required|unique:pacientes|numeric';
		else
			$reglas['dni'] = 'required|numeric';
		$reglas['direccion'] = 'required';
		$reglas['tel_contacto'] = 'min:10|max:11';
		$reglas['cel_contacto'] = 'min:12|max:13';
		$reglas['cel_contacto_2'] = 'min:12|max:13';
		$reglas['fecha_nacimiento'] = 'required';
//		$reglas['fecha_ingreso'] = 'required';
		if ($id == null && !empty($inputs["nombre_padre"]))
			$reglas['dni_padre'] = 'required';
		if ($id == null && !empty($inputs["nombre_madre"]))
			$reglas['dni_madre'] = 'required';
		if ($id == null && !empty($inputs["nombre_tutor"]))
			$reglas['dni_tutor'] = 'required';


		$mensajes = array(
			'required' => 'Debe completar el campo!',
			'unique' => 'El paciente ya existe!',
			'numeric' => 'El dni debe ser un número',
			'min' => 'Digitos menor al mínimo permitido',
			'max' => 'Digitos mayor al máximo permitido'
		);

		$validar = Validator::make($inputs, $reglas, $mensajes);

		if ($validar->fails()) {
			return Redirect::back()->withErrors($validar)->withInput();
		} else {
			if (is_null($id)) {

				// Alta
				$paciente = new Paciente($inputs);

				if (!$paciente->save()) {
					return Redirect::back()->withErrors($paciente->errors())->withInput()->with(compact("paciente"));
				}

				// Guarda hermanos
				if (isset($inputs_hermanos)) {
					foreach ($inputs_hermanos as $datos_hermanos) {
						$hermano = new Hermano($datos_hermanos);
						$paciente->hermanos()->save($hermano);
					}
				}

				return Redirect::to('pacientes')->with('success', 'El paciente ha sido guardado correctamente');
			} else {

				// Modificación
				$paciente = Paciente::findOrFail($id);

				if ($usuario->es_admin) {
					// Actualiza inversiones
					try {
						// Inicio transaccion
						DB::beginTransaction();

						// Elimina Inversiones
						foreach ($paciente->hermanos as $hermano) {
							$hermano->delete();
						}

						// Guarda inversiones
						if (isset($inputs_hermanos)) {
							foreach ($inputs_hermanos as $datos_hermano) {
								$hermano = new Hermano($datos_hermano);
								$paciente->hermanos()->save($hermano);
							}
						}

						// Commit
						DB::commit();
					} catch (\PDOException $e) {
						DB::rollBack();
						throw $e;
					}

					$paciente->fill(Input::only(
									'nombre', 'fecha_ingreso', 'fecha_nacimiento', 'dni', 'nombre_padre', 'dni_padre', 'estudio_padre','estudio_padre_estado', 'nombre_madre', 'dni_madre', 'estudio_madre','estudio_madre_estado', 'nombre_tutor', 'dni_tutor', 'estudio_tutor','estudio_tutor_estado', 'ocupacion_vivienda', 'direccion', 'tel_contacto', 'cel_contacto', 'cel_contacto_2', 'medio_devida', 'distancia_camino', 'diagnostico_id', 'lugar_tratamiento_id', 'localidad_id', 'techo_zinc', 'techo_carton', 'techo_otro', 'pared_madera', 'pared_material', 'pared_otro', 'piso_ceramica', 'piso_alisado', 'piso_otro', 'agua_pozo_balde', 'agua_canilla_publica', 'agua_otro', 'sanitario_interno', 'sanitario_externo', 'sanitario_completo', 'sanitario_otro', 'nro_habitantes_casa', 'nro_habitaciones', 'nro_personas_que_trabajan', 'trabajo_fijo', 'trabajo_temporario', 'trabajo_porsucuenta', 'trasladado', 'estado_paleativo', 'prioridad_alta','fallecio', 'prioridad_media', 'observaciones')
					);

					try {
						if (!$paciente->update())
							return Redirect::back()->withErrors($paciente->errors());
						elseif ($usuario->es_admin)
							return Redirect::to('pacientes/editar/' . $paciente->id)->with('success', 'El paciente ha sido guardado correctamente');
					} catch (ValidationException $e) {
						return Redirect::back()->with('error', $e->getMessage())->withInput();
					}
				}
			}//termina modificacion
		}//termina el validar campos
	}

	public function getDerivar($id = null) {
		$usuario = Auth::user();

		if (!($usuario->es_admin)) {
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}

		$id = strip_tags($id);

		$paciente = Paciente::findOrFail($id);

		$view = View::make('pacientes/derivar');
		$view->with(compact("id", "usuario", "paciente"));
//        if(empty($id)){
//            $view->with(array(
//                "accion" => "add"
//                , "id" => ""
//            ));
//        }else{
//            $datos = Usuario::findOrFail($id);
//            $accion = "update";
//            $view->with(compact("datos", "id", "accion"));
//        }
//        
		$this->layout->content = $view;
	}

	public function postDerivar() {
		$usuario = Auth::user();

		if (!($usuario->es_admin)) {
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}

		$inputs = Input::all();

		$reglas = array();
		$reglas['fecha'] = 'required';
		$reglas['lugar'] = 'required';

		$mensajes = array(
			'required' => 'Debe completar el campo!'
		);

		$validar = Validator::make($inputs, $reglas, $mensajes);

		if ($validar->fails()) {
			return Redirect::back()->withErrors($validar)->withInput();
		}

		$derivacion = new Derivacion();
		$derivacion->paciente_id = $inputs["id"];
		$derivacion->fecha = $inputs["fecha"];
		$derivacion->lugar = $inputs["lugar"];
		$derivacion->referencia = $inputs["referencia"];


		if (!$derivacion->save()) {
			return Redirect::back()->withErrors($derivacion->errors())->withInput()->with(compact("derivacion"));
		}

		return Redirect::to('/pacientes')->with('success', 'El paciente ha sido derivado correctamente');
	}

//
//    public function getEliminar($id) {
//        $prestamo = Prestamo::findOrFail($id);
//        $usuario = Auth::user();
//        if (!($usuario->es_admin || $usuario->es_vendedor)) {
//            return Redirect::to('prestamos')->with('error', 'Acceso no permitido');
//        }
//
//        $this->validarAcceso($prestamo);
//
//        if ($prestamo->estado == Prestamo::ESTADO_BORRADOR || $prestamo->estado == Prestamo::ESTADO_CARGADO) {
//            $prestamo->delete();
//            return Redirect::to('prestamos')->with('info', 'El préstamo ha sido eliminado correctamente.');
//        } else {
//            return Redirect::back()->withErrors("Sólo se pueden eliminar prestamos en borrador");
//        }
//    }
//
//    private function validarAcceso(Prestamo $prestamo) {
//        // Control de acceso
//        $usuario = Auth::user();
//        $permitido = true;
//        if (!$usuario->es_admin && $usuario->es_vendedor) {
//            if ($prestamo->cliente->vendedor_usuario_id != $usuario->id) {
//                $permitido = false;
//            }
//        } elseif (!$usuario->es_admin) {
//            $permitido = false;
//        }
//
//        if (!$permitido) {
//            return Redirect::to('prestamos')->with('error', 'Acceso no permitido');
//        }
//    }
//
}
