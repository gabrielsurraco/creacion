<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDerivaciones extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('derivaciones', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('paciente_id');
			$table->date('fecha');
			$table->string("lugar",200);
			$table->string("referencia",50);

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('derivaciones');
	}

}
