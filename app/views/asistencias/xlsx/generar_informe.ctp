<?php

$tabla_hdr = array(
    array('label' => __('# Numero'), 'width' => 'auto'),
    array('label' => __('Fecha'), 'width' => 'auto'),
    array('label' => __('Bultos')),
    array('label' => __('Peso')),
    array('label' => __('Valor')),
    array('label' => __('Chofer'), 'width' => 'auto'),
    array('label' => __('Acompañante'), 'width' => 'auto'),
    array('label' => __('Vehículo'), 'width' => 'auto'),
    array('label' => __('Monitoreo'), 'width' => 'auto'),
    array('label' => __('Estado'), 'width' => 'auto'),
);

// Setup

$this->PhpExcel->createWorksheet()
	->setDefaultFont('Calibri', 12)
    ;
$this->PhpExcel->setSheetName("Hoja de Ruta");

$this->PhpExcel->getActiveSheet()->getPageSetup()
		->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE)
		->setFitToWidth(true)
	;

// Cabecera

$this->PhpExcel->addTableRow(array("Hoja de Ruta"));
$this->PhpExcel->addTableRow();

$this->PhpExcel->addTableHeader($tabla_hdr, array('name' => 'Cambria', 'bold' => true));

$this->PhpExcel->addTableRow(array(
     $hdr["HojasDeRuta"]["id"]
    ,$hdr["HojasDeRuta"]["fecha"] 
    ,$hdr["HojasDeRuta"]["cant_bultos"]
    ,$hdr["HojasDeRuta"]["peso_total"]." Kg"
    ,"$ ".$hdr["HojasDeRuta"]["valor_declarado"]
    ,$hdr["Chofer"]["nom_y_ape"]." - ".$hdr["Chofer"]["dni"]
    ,($hdr["HojasDeRuta"]["acompanante"]) ? "Si" : "No"
    ,$hdr["Vehiculo"]["nombre"] . " - " . $hdr["Vehiculo"]["patente"]
    ,($hdr["HojasDeRuta"]["monitoreo"]) ? "Si" : "No"
    ,ucfirst($hdr["HojasDeRuta"]["estado"])
));

if(!empty($hdr["HojasDeRuta"]["observaciones"])){
	$this->PhpExcel->addTableRow(array(
		"Observaciones:",
		$hdr["HojasDeRuta"]["observaciones"]
	));
}

// Comprobantes

$this->PhpExcel->addTableRow();
$this->PhpExcel->addTableRow(array("Comprobantes"));
$this->PhpExcel->addTableRow();


$tabla_comprobantes = array(
    array('label' => __('Número'), 'width' => 'auto'),
    array('label' => __('Fecha de Emisión'), 'width' => 'auto'),
    array('label' => __('Días Antigüedad'), 'width' => 'auto'),
    array('label' => __('Cliente'), 'width' => 'auto'),
    array('label' => __('# Dirección')),
    array('label' => __('Dirección de Entrega'), 'width' => 'auto'),
    array('label' => __('Bultos')),
    array('label' => __('Peso')),
    array('label' => __('Valor')),
    array('label' => __('Sector')),
);

$this->PhpExcel->addTableHeader($tabla_comprobantes, array('name' => 'Cambria', 'bold' => true));

foreach ($hdr["Comprobantes"] as $a) {
    $ss = "";
    foreach ($a["Comprobante"]["Entrega"] as $entrega) {
        foreach ($entrega["Sector"] as $sector) {
            if ($ss != "")
                $ss .= ", ";
                $ss .= $this->App->sector($sector);
        }
    }
    if ($ss == "")
        $ss = "N/A";
    
    $this->PhpExcel->addTableRow(array(
        $this->App->comprobante($a["Comprobante"])
       ,$a["Comprobante"]["fecha"] 
       ,$this->App->diasHabiles($a["Comprobante"]["id"], $a["Comprobante"]["fecha"], date("Y-m-d"))
       ,$a["Comprobante"]["Cliente"]["nombre"]
       ,$a["Comprobante"]["Cliente"]["codigo_erp"]
       ,(isset($a["Comprobante"]["Domicilio"]["identificador"])) ? $a["Comprobante"]["Domicilio"]["identificador"] : "N/A"
       ,$a["Comprobante"]["bultos"]
       ,$a["Comprobante"]["peso"] . " Kg"
       ,"$ " . $a["Comprobante"]["valuacion"]
       ,$ss
    ));
}

$this->PhpExcel->addTableFooter();
