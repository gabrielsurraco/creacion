@section('content')

<h3>Actualizaci&oacute;n del Stock</h3><hr/>

<div class="content">
	
	{{ Form::open(array('action' => 'StockController@postActualizar', 'method' => 'POST')) }}
	
    <div class="row">
		<div class="form-group col-md-3">
			{{ Form::label('cantidad', 'Cantidad de Artículos') }}
			{{ Form::number('cantidad', null, array('placeholder' => 'Cantidad de Artículos', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
			<p class="alert-danger">{{$errors->first("cantidad")}}</p>
		</div>
		
        
    </div>
	<div class="row">
		<div class="form-group col-lg-4">
			{{Form::label('articulo','Artículo a Actualizar')}}
			<br>
			<!--<div class="radio-inline">-->
				<label class="radio" style="font-weight: normal">
					
					{{ Form::radio('tipo', 'alimento',true) }}
					Alimento
				</label>
				<label class="radio" style="font-weight: normal">
					
					{{ Form::radio('tipo', 'ropero_nuevo') }}
					Ropero Nuevo
				</label>
				<label class="radio" style="font-weight: normal">
					
					{{ Form::radio('tipo', 'ropero_usado') }}
					Ropero Usado
				</label>
				<label class="radio" style="font-weight: normal">
					
					{{ Form::radio('tipo', 'juguete_nuevo') }}
					Juguete Nuevo
				</label>
				<label class="radio" style="font-weight: normal">
					
					{{ Form::radio('tipo', 'juguete_usado') }}
					Juguete Usado
				</label>
			<!--</div>-->
		</div>
	</div>
	<div class="row">
		<div class="form-group col-lg-3">
			{{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}
			{{ HTML::link("pacientes","Cancelar",array("class"=>"btn btn-default")) }}
		</div>
	</div>
	
	
	{{ Form::close() }}
</div>
@show

@section('js')
<script>
	$(function(){
	  $(".tablesorter").tablesorter();
	});
</script>
@stop