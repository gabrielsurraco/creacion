@section('content')
<div class="row">
	<div class="col-lg-12">
		<h3>Dashboard</h3><hr>
	</div>
</div>

<div class="row">
	<div class="col-md-3" >
		<h5><strong>Cumplen años hoy:</strong></h5>
		<ul class="fa-ul">
			@foreach($colaboradores as $colaborador)
				<li><i class="fa-li fa fa-birthday-cake text-danger"></i>{{ $colaborador->nombre }}</li>
				<a style="margin-top: 5px; margin-bottom: 5px;" class="btn btn-sm btn-success" href="/colaboradores/sendmailbirthday/{{ $colaborador->id }}"  > <i class="fa fa-send"></i> Enviar Saludo</a>
			@endforeach
			@if(empty($colaboradores[0]))
				<li><i class="fa fa-flag"></i>  Hoy no hay cumpleaños.</li>
			@endif
		</ul>
		
	</div>
	<div class="col-md-5">
		<h5><strong>Socios que no abonan hace 6 meses o más:</strong></h5>
		
		<ul class="fa-ul">
			<?php $hay_deudores = false;?>
			@foreach($colaboradores_con_pagos as $colaborador_con_pagos)
				@if(empty($colaborador_con_pagos->pagos[0]))
					<?php $hay_deudores = true; ?>
				<li><i class="fa-li fa fa-male"></i>{{ $colaborador_con_pagos->nombre}} </li>
				<a style="margin-top: 5px; margin-bottom: 5px;" class="btn btn-sm btn-default" href="/colaboradores/sendmailrememberfee/{{ $colaborador_con_pagos->id }}"  > <i class="fa fa-send"></i> Enviar Recordatorio</a>
				@endif
			@endforeach
			@if(!$hay_deudores)
				<li><i class="fa fa-flag"></i>  No hay socios que adeuden. </li>
			@endif
		</ul>
	</div>

</div>

@stop

