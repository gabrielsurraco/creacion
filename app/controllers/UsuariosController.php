<?php

class UsuariosController extends BaseController {
	
    public function getIndex()
    {
        $usuarios = Usuario::all();
		
        $this->layout->content = View::make('usuarios/index',compact("usuarios"));
    }
    
    public function getEditar($id = null)
    {
		$usuario = Auth::user();

		if (!($usuario->es_admin ||$usuario->es_default)) {
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}
		
        $view = View::make('usuarios/registro');
        
        if(empty($id)){
            $view->with(array(
                "accion" => "add"
                , "id" => ""
            ));
        }else{
            $datos = Usuario::findOrFail($id);
            $accion = "update";
            $view->with(compact("datos", "id", "accion"));
        }
        
        $this->layout->content = $view;
    }
//    
    // Editar un registro
    public function postEditar($id=null){
		$usuario = Auth::user();

		if (!($usuario->es_admin)) {
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}
		
        $inputs = Input::All();
        
        $reglas = array(
            'email'=>'required|email|unique:usuarios',
            'nombre'=>'required|min:5',
            'password'=>'required|same:password_confirmation'
        );
        
        $mensajes = array(
            'required'=>'Debe completar el campo!',
            'email'=>'Debe ingresar un email válido',
            'min'=>'Debe tener mas de 5 caracteres',
            'same'=>'Las contraseñas no coinciden',
            'unique'=>'El usuario ya esta registrado!'
        );
        
        // Edicion
        if(!empty($id)){
            // Si no vino la clave, retirar la validacion de la misma
            if(empty($inputs['password'])){
                unset($reglas["password"]);
            }
            
            // TODO: Mejorar esto
            $reglas['email'] = 'required|email';
        }
        
        $validar=Validator::make($inputs,$reglas,$mensajes);
        
        if($validar->fails()){            
            return Redirect::back()->withErrors($validar)->withInput();
        }else{
            if(empty($id))
                $usuario= new Usuario();
            else
                $usuario = Usuario::findOrFail($id);
            
            $usuario->email = $inputs['email'];
            $usuario->nombre = $inputs['nombre'];
            
            //
			if(Auth::user()->es_admin){
				$usuario->es_admin = (Input::has('admin'));
				$usuario->es_default = (Input::has('default'));
//				$usuario->es_inversor = (Input::has('inversor'));
//				$usuario->es_vendedor = (Input::has('vendedor'));
			}
           
            if(!empty($inputs["password"]))
                $usuario->password=Hash::make($inputs['password']);
            
            $usuario->save();
            Session::flash('success', 'El registro ha sido guardado exitosamente!');
			if(Auth::user()->es_admin)
				return Redirect::to('/usuarios');
			else
				return Redirect::to('/home');
        }
    }
//    
    public function getEliminar($id) {
		$usuario = Auth::user();

		if (!($usuario->es_admin)) {
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}
		
        $user = Usuario::findOrFail($id);
        $user->delete();
        return Redirect::to('usuarios')->with('info', 'El usuario ha sido eliminado correctamente.');
    }

}

