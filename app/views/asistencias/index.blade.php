@section('content')

<h3>Asistencias en la casita</h3><hr/>

<div class="content">
    <div class="row">
        <div class="col-lg-8">
            @if(Auth::user()->es_admin)
                {{HTML::link("asistencias/editar","Registrar Asistencia",array("class"=>"btn btn-primary"))}}
            @else
                {{HTML::link("#","Registrar Asistencia",array("class"=>"btn btn-primary disabled"))}}
            @endif
            
        </div>
    </div>
	<br/>
	<div class="row">
		{{ Form::open(array('action' => 'AsistenciasController@getIndex', 'method' => 'POST')) }}
		<div class="form-group col-md-2">
			{{ Form::text('fecha_desde', empty($filtros["fecha_desde"]) ? null : $filtros["fecha_desde"], array('placeholder' => 'Fecha Desde...', 'class' => 'form-control datepicker' , 'autocomplete' => 'off')) }}
		</div>
		<div class="form-group col-md-2">
			{{ Form::text('fecha_hasta', empty($filtros["fecha_hasta"]) ? null : $filtros["fecha_hasta"] , array('placeholder' => 'Fecha Hasta...', 'class' => 'form-control datepicker' , 'autocomplete' => 'off')) }}
		</div>
		<div class="form-group col-md-3">
			<button type="submit" class="btn btn-default btn-group-lg"><i class="fa fa-search"></i> Buscar</button>
		</div>
		{{ Form::close() }}
	</div>
	<div class="row">

        <div class="col-lg-8">
			@if(isset($asistencias[0]))
			<a href="asistencias/index/1"  style="margin-bottom: 10px;"  class="btn btn-sm btn-success pull-right"> <i class="fa fa-file-excel-o "> </i>  Exportar Resultados</a>
			@else
				<a href="#" style="margin-bottom: 10px;"  class="btn btn-sm btn-success pull-right disabled"> <i class="fa fa-file-excel-o "> </i>  Exportar Resultados</a>
			@endif
            <div class="table-responsive">
                <table class="table table-hover table-striped tablesorter">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Nro. Niños</th>
                            <th>Nro. Adultos</th>
                            <th>Observaciones</th>
							@if(Auth::user()->es_admin)
							<th>Acciones</th>
							@endif
                        </tr>
                    </thead>
                    <tbody>
						@foreach($asistencias as $asistencia)
                                <tr>     
                                    <td>{{ $asistencia->fecha_asistencia }}</td>
                                    <td style="text-align: center;">{{ $asistencia->nro_chicos }}</td>
                                    <td style="text-align: center;">{{ $asistencia->nro_adultos }}</td>
                                    <td>{{ $asistencia->observaciones }}</td>
									@if(Auth::user()->es_admin)
                                    <td>
											<a href="asistencias/editar/{{ $asistencia->id }}"  class="btn btn-sm btn-default"> <i class="fa fa-edit"></i> Editar</a>
									</td>
									@endif
                                </tr>
						@endforeach
                        
                    </tbody>
                </table>
                    <div class="text-center">{{ $asistencias->links() }}</div> 
            </div>
        </div>
    </div>
</div>
@show

@section('js')
<script>
	$(function(){
	  $(".tablesorter").tablesorter();
	});
</script>
@stop