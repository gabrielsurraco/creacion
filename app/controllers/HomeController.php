<?php

class HomeController extends BaseController {
	protected $layout = 'layouts.master';
	

	public function showHome(){
		return Redirect::to('dashboard');
	}
	
	public function showLogin(){
		return View::make('login');
	}
	
	public function doLogin(){
		
		// validate the info, create rules for the inputs
		$rules = array(
			'email'    => 'required|email',
//			'password' => 'required|alphaNum|min:3'
		);
		
		
		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);
		
		// if the validator fails, redirect back to the form
		if ($validator->fails()) {
			return Redirect::to('login')
				->withErrors($validator) // send back all errors to the login form
				->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
		} else {
			
			// create our user data for the authentication
			$userdata = array(
				'email' 	=> Input::get('email'),
				'password' 	=> Input::get('password')
			);
			
			// attempt to do the login
			if (Auth::attempt($userdata)) {
				return Redirect::to('/dashboard/principal');
			} else {
				// validation not successful, send back to form
				return Redirect::to('login');

			}
		}
	}
	
	public function doLogout()
	{
		if (Auth::check())
			Auth::logout(); // log the user out of our application
		return Redirect::to('login'); // redirect the user to the login screen
	}
}
