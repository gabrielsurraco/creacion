<?php

class StockController extends BaseController {
	
    public function getIndex()
    {
//        $localidades = Localidad::all();
		$stock = Stock::all();
        $this->layout->content = View::make('stock/index',compact("stock"));
    }
	
	public function getActualizar(){
		$usuario = Auth::user();
		
		if(!($usuario->es_admin)){
			return Redirect::to('stock')->with('error', 'Acceso no permitido');
		}
		
		$this->layout->content = View::make('stock/actualizar');
	}
	
	public function postActualizar(){
		$usuario = Auth::user();
		
		if(!($usuario->es_admin)){
			return Redirect::to('stock')->with('error', 'Acceso no permitido');
		}
		
		$inputs = Input::all();
		
		$reglas = array();

		$reglas['tipo'] = 'required';
		$reglas['cantidad'] = 'required|numeric|min:1';

		$mensajes = array(
			'required' => 'Debe completar el campo!',
			'numeric' => 'Debe ser un número',
			'min' => '1 es el valor mínimo',
			
		);

		$validar = Validator::make($inputs, $reglas, $mensajes);

		if ($validar->fails()) 
			return Redirect::back()->withErrors($validar)->withInput();
		
		
		
		// Actualiza Stock
		try{
			// Inicio transaccion
			DB::beginTransaction();
			
			Stock::where("tipo",$inputs["tipo"])->increment('cantidad', $inputs["cantidad"],array(
				"usuario_actualizo" => $usuario->email
			));
			
			// Commit
			DB::commit();
			return Redirect::to('stock')->with('success', 'Stock Actualizado!');
			
			
		} catch (\PDOException $e) {
			DB::rollBack();
			throw $e;
		}
		
		
	}
    
//    public function getEditar($id = null)
//    {
//        $view = View::make('localidades/registro');
//        
//        if(empty($id)){
//            $view->with(array(
//                "accion" => "add"
//                , "id" => ""
//            ));
//        }else{
//            $datos = Localidad::findOrFail($id);
//            $accion = "update";
//            $view->with(compact("datos", "id", "accion"));
//        }
//        
//        $this->layout->content = $view;
//    }
//////    
//    // Editar un registro
//    public function postEditar($id=null){
//        $inputs = Input::All();
//        
//        $reglas = array(
//            'nombre'=>'required'
//        );
//        
//        $mensajes = array(
//            'required'=>'Debe completar el campo!'
//        );
//        
//        $validar=Validator::make($inputs,$reglas,$mensajes);
//        
//        if($validar->fails()){            
//            return Redirect::back()->withErrors($validar)->withInput();
//        }else{
//            if(empty($id))
//                $localidad= new Localidad();
//            else
//                $localidad = Localidad::findOrFail($id);
//            
//            $localidad->nombre = $inputs['nombre'];
//           
//            $localidad->save();
//            Session::flash('success', 'El registro ha sido guardado exitosamente!');
//			if(Auth::user()->es_admin)
//				return Redirect::to('/localidades');
//			else
//				return Redirect::to('/home');
//        }
//    }
////    
//    public function getEliminar($id) {
//		
//		$pacientes = Paciente::where("localidad_id" , $id)->get();
//		if(count($pacientes) > 0){
//			return Redirect::to('/localidades')->with('warning', 'La localidad no puede ser eliminado debido a que esta siendo utilizada');
//		}else{
//			$localidad = Localidad::findOrFail($id);
//			$localidad->delete();
//			return Redirect::to('localidades')->with('info', 'La localidad ha sido eliminada correctamente.');
//		}
//    }

}

