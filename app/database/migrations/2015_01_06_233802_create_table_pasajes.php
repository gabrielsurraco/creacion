<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePasajes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pasajes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('paciente_id');
			$table->string("origen",100);
			$table->string("destino",100);
			$table->enum('tipo_pasaje',array('urbano','interurbano','larga_distancia'));
			$table->integer('cantidad');
			$table->date('fecha');
			$table->float('monto')->default(0);

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists("pasajes");
	}

}
