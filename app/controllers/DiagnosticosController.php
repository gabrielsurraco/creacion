<?php

class DiagnosticosController extends BaseController {
	
    public function getIndex()
    {
        $diagnosticos = Diagnostico::all();
		
        $this->layout->content = View::make('diagnosticos/index',compact("diagnosticos"));
    }
    
    public function getEditar($id = null)
    {
		$usuario = Auth::user();
		
		if (!($usuario->es_admin)) {
			return Redirect::to('diagnosticos')->with('error', 'Acceso no permitido');
		}
		
        $view = View::make('diagnosticos/registro');
        
        if(empty($id)){
            $view->with(array(
                "accion" => "add"
                , "id" => ""
            ));
        }else{
            $datos = Diagnostico::findOrFail($id);
            $accion = "update";
            $view->with(compact("datos", "id", "accion"));
        }
        
        $this->layout->content = $view;
    }
////    
    // Editar un registro
    public function postEditar($id=null){
		$usuario = Auth::user();
		
		if (!($usuario->es_admin)) {
			return Redirect::to('diagnosticos')->with('error', 'Acceso no permitido');
		}
		
        $inputs = Input::All();
        
        $reglas = array(
            'nombre'=>'required'
        );
        
        $mensajes = array(
            'required'=>'Debe completar el campo!'
        );
        
        $validar=Validator::make($inputs,$reglas,$mensajes);
        
        if($validar->fails()){            
            return Redirect::back()->withErrors($validar)->withInput();
        }else{
            if(empty($id))
                $diagnostico= new Diagnostico();
            else
                $diagnostico = Diagnostico::findOrFail($id);
            
            $diagnostico->nombre = $inputs['nombre'];
           
            $diagnostico->save();
            Session::flash('success', 'El registro ha sido guardado exitosamente!');
			if(Auth::user()->es_admin)
				return Redirect::to('/diagnosticos');
			else
				return Redirect::to('/home');
        }
    }
//    
    public function getEliminar($id) {
		$usuario = Auth::user();
		
		if (!($usuario->es_admin)) {
			return Redirect::to('diagnosticos')->with('error', 'Acceso no permitido');
		}
		
		$pacientes = Paciente::where("diagnostico_id" , $id)->get();
		if(count($pacientes) > 0){
			return Redirect::to('/diagnosticos')->with('warning', 'El diagnóstico no puede ser eliminado debido a que esta siendo utilizado');
		}else{
			$diagnostico = Diagnostico::findOrFail($id);
			$diagnostico->delete();
			return Redirect::to('/diagnosticos')->with('info', 'El diagnóstico ha sido eliminado correctamente.');
		}
    }

}

