@section('content')
<h3>Administraci&oacute;n de Colaboradores</h3><hr/>    
<div class="content">
    <div class="panel panel-primary">
        <div class="panel-heading">Alta de Colaboradores</div>
        <div class="panel-body">

            <div id="mensajedealerta" class="alert alert-danger alert-dismissable hidden">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>

			
			{{ Form::model($colaborador, array('route' => array('colaboradores.update', $colaborador->id), 'method' => 'POST')) }}
				<div class="row">

					<div class="form-group col-md-3">
						{{ Form::label('nombre', 'Apellido y Nombre') }}
						{{ Form::text('nombre', empty($colaborador->id) ? null : $colaborador->nombre, array('placeholder' => 'Nombre del Colaborador...', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
						<p class="alert-danger">{{$errors->first("nombre")}}</p>
					</div>
					
					<div class="form-group col-md-3">
						{{ Form::label('dni', 'Número de Documento') }}
						{{ Form::text('dni', empty($colaborador->id) ? null : $colaborador->dni, array('placeholder' => 'Documento del colaborador...', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
						<p class="alert-danger">{{$errors->first("dni")}}</p>
					</div>
					
					<div class="form-group col-md-3">
						{{ Form::label('fecha_nacimiento', 'Fecha de Nacimiento') }}
						{{ Form::text('fecha_nacimiento', empty($colaborador->id) ? null : $colaborador->fecha_nacimiento , array('placeholder' => 'Fecha de Nacimiento', 'class' => 'form-control datepicker' , 'autocomplete' => 'off')) }}
						<p class="alert-danger">{{$errors->first("fecha_nacimiento")}}</p>
					</div>
					
				</div>
			
				<div class="row">
					<div class="form-group col-md-3">
						{{ Form::label('profesion', 'Profesión') }}
						{{ Form::text('profesion', empty($colaborador->id) ? null : $colaborador->profesion, array('placeholder' => 'Profesión', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
						<p class="alert-danger">{{$errors->first("profesion")}}</p>
					</div>
					<div class="form-group col-md-3">
						{{ Form::label('domicilio', 'Domicilio') }}
						{{ Form::text('domicilio', empty($colaborador->id) ? null : $colaborador->domicilio, array('placeholder' => 'Domicilio', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
						<p class="alert-danger">{{$errors->first("domicilio")}}</p>
					</div>
					<div class="form-group col-md-3">
						{{ Form::label('localidad_id', 'Localidad') }}
						{{ Form::select("localidad_id", array('default' => 'Seleccionar...') + $lista_localidades, empty($colaborador->id) ? 'default' : $colaborador->localidad_id, array("class"=>"form-control")) }}
						<p class="alert-danger">{{$errors->first("localidad_id")}}</p>
					</div>

				</div>
			
				<div class="row">
					<div class="form-group col-md-3">
						{{ Form::label('tel_contacto', 'Tel. de Contacto') }}
						{{ Form::text('tel_contacto', empty($colaborador->id) ? null : $colaborador->tel_contacto, array('placeholder' => 'Ingrese Nro. Teléfono', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
						<p class="alert-danger">{{$errors->first("tel_contacto")}}</p>
					</div>
					<div class="form-group col-md-3">
						{{ Form::label('email', 'Email') }}
						{{ Form::email('email', empty($colaborador->id) ? null : $colaborador->email, array('placeholder' => 'Ingrese el e-mail', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
						<p class="alert-danger">{{$errors->first("email")}}</p>
					</div>
					<div class="form-group col-md-3">
						{{ Form::label('lugar_de_pago', 'Lugar de pago de cuota') }}
						{{ Form::text('lugar_de_pago', empty($colaborador->id) ? null : $colaborador->lugar_de_pago, array('placeholder' => 'Ingrese lugar de pago', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
						<p class="alert-danger">{{$errors->first("lugar_de_pago")}}</p>
					</div>
				</div>
			
				<div class="row">
					<div class="form-group col-md-3">
						{{ Form::label('fecha_ingreso', 'Fecha de Ingreso') }}
						{{ Form::text('fecha_ingreso', empty($colaborador->id) ? null : $colaborador->fecha_ingreso , array('placeholder' => 'Fecha de Ingreso', 'class' => 'form-control datepicker' , 'autocomplete' => 'off')) }}
						<p class="alert-danger">{{$errors->first("fecha_ingreso")}}</p>
					</div>
				</div>
			
				<div class="row">
					<div class="form-group col-md-3">
						{{ Form::label('colaborador', 'Tipo de Colaborador') }}
						<div class="checkbox">
                            <label>
                                    {{ Form::checkbox('es_socio', true , false) }}
                                Socio
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                    {{ Form::checkbox('es_voluntario', true , false) }}
                                Voluntario
                            </label>
                        </div>
						<div class="checkbox">
                            <label>
                                    {{ Form::checkbox('es_benefactor', true , false) }}
                                Benefactor
                            </label>
                        </div>
					</div>
					<div class="form-group col-md-6">
						{{ Form::label('colabora_en', 'En que puede colaborar') }}
						{{ Form::textarea('colabora_en', empty($colaborador->id) ? null : $colaborador->colabora_en, array('placeholder' => 'Colabora en...', 'class' => 'form-control' , 'autocomplete' => 'off', 'rows' => '4')) }}
						<p class="alert-danger">{{$errors->first("colabora_en")}}</p>
					</div>
				</div>
			
				{{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}
				{{ HTML::link("colaboradores","Cancelar",array("class"=>"btn btn-default")) }}
			
			{{ Form::close() }}
        </div>
    </div>

</div>
@show

@section('js')
<script>


</script>
@show
