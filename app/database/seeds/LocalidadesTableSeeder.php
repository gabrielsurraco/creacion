<?php

class LocalidadesTableSeeder extends Seeder {

	public function run() {
		DB::table('localidades')->delete();
		Localidad::create(array(
			'nombre' => 'Localidad 1'
		));
		
		Localidad::create(array(
			'nombre' => 'Localidad 2'
		));
		
		Localidad::create(array(
			'nombre' => 'Localidad 3'
		));
		
	}

}
