@section('content')
<a name="inicio"></a>
<h3>Datos del Paciente</h3><hr/>    
<div class="content">
	<div class="row">
		<div class="form-group col-md-12" style="display: inline-block">
			@if(Auth::user()->es_admin)
				<a class="btn btn-warning" href="/pacientes/derivar/{{$paciente->id}}"  > <i class="fa fa-car"></i> Derivar</a>
			@else
				<a class="btn btn-warning disabled" href="#"  > <i class="fa fa-car disabled"></i> Derivar</a>
			@endif
			@if(Auth::user()->es_admin)
				<a class="btn btn-success" href="/pacientes/beneficios/{{$paciente->id}}"  > <i class="fa fa-hand-o-up"></i> Dar Beneficio</a>
			@else
				<a class="btn btn-success disabled" href="#"  > <i class="fa fa-hand-o-up"></i> Dar Beneficio</a>
			@endif
			@if(Auth::user()->es_admin)
				<a class="btn btn-primary" href="/asistencias/crear/{{$paciente->id}}"  > <i class="fa fa-calendar"></i> Registrar Asistencia</a>
			@else
				<a class="btn btn-primary disabled" href="#"  > <i class="fa fa-calendar"></i> Registrar Asistencia</a>
			@endif
			{{ HTML::link("pacientes","Cancelar",array("class"=>"btn btn-default")) }}
		</div>
		
	</div>

	<div class="bs-example">
		<ul class="nav nav-tabs" style="margin-bottom: 15px;">
			<li class="active"><a href="#ficha" data-toggle="tab">Ficha</a></li>
			<li class=""><a href="#asistencia" data-toggle="tab">Asistencias</a></li>
			<li class=""><a href="#alimento" data-toggle="tab">Alimentos</a></li>
			<li class=""><a href="#derivacion" data-toggle="tab">Derivaciones</a></li>
			<li class=""><a href="#ropero" data-toggle="tab">Roperos</a></li>
			<li class=""><a href="#juguete" data-toggle="tab">Juguetes</a></li>
			<li class=""><a href="#donacion" data-toggle="tab">Donaciones / Compras</a></li>
			<li class=""><a href="#pasajes" data-toggle="tab">Pasajes</a></li>
			<li class=""><a href="#prestamos" data-toggle="tab">Préstamos</a></li>
			<li class=""><a href="#tramites" data-toggle="tab">Otros Trámites</a></li>
		</ul>
		<div id="myTabContent" class="tab-content">
			<div class="tab-pane fade active in" id="ficha">
				<div class="panel panel-primary">
					<div class="panel-heading">Información del Paciente</div>
					<div class="panel-body">
						<div class="row">

							<div class="form-group col-md-3">
								{{ Form::label('nombre', 'Apellido y Nombre') }}
								{{ Form::text('nombre', empty($paciente->id) ? null : $paciente->nombre, array( "disabled",'placeholder' => 'Apellido y Nombre del Paciente', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>

							<div class="form-group col-md-3">
								{{ Form::label('dni', 'Número de Documento') }}
								{{ Form::text('dni', empty($paciente->id) ? null : $paciente->dni, array("disabled",'placeholder' => 'Número del paciente', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>

							<div class="form-group col-md-3">
								{{ Form::label('fecha_nacimiento', 'Fecha de Nacimiento') }}
								{{ Form::text('fecha_nacimiento', empty($paciente->id) ? null : $paciente->fecha_nacimiento, array("disabled",'placeholder' => '', 'class' => 'form-control ' , 'autocomplete' => 'off')) }}
							</div>

						</div>

						<div class="row">
							<div class="form-group col-md-3">
								{{ Form::label('diagnostico_id', 'Diagnóstico') }}
								{{ Form::text('diagnostico_id', empty($paciente->id) ? null : Helpers::getDiagnostico($paciente->diagnostico_id), array("disabled",'placeholder' => '', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>
							<div class="form-group col-md-3">
								{{ Form::label('fecha_ingreso', 'Fecha de Diagnóstico') }}
								{{ Form::text('fecha_ingreso', empty($paciente->id) ? null : $paciente->fecha_ingreso, array("disabled",'placeholder' => 'Fecha de Diagnóstico', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>

							<div class="form-group col-md-3">
								{{ Form::label('lugar_tratamiento_id', 'Lugar de Tratamiento') }}
								{{ Form::text('lugar_tratamiento_id', empty($paciente->id) ? null : Helpers::getLugarTratamiento($paciente->lugar_tratamiento_id), array("disabled",'placeholder' => 'Lugar de Tratamiento', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>

						</div>

						<div class="row">
							<div class="form-group col-md-3">
								{{ Form::label('trasladado', 'Trasladado a') }}
								{{ Form::text('trasladado', empty($paciente->id) ? null : $paciente->trasladado, array("disabled",'placeholder' => 'Lugar de traslado', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>
							<div class="form-group col-md-3">
								{{Form::label('roles','Prioridad')}}
								<br>
								<label class="checkbox-inline">
                                    {{ Form::checkbox('prioridad_alta', true , $paciente->prioridad_alta) }}
									Alta
								</label>

								<label class="checkbox-inline">
                                    {{ Form::checkbox('prioridad_media', true , $paciente->prioridad_media) }}
									Media
								</label>
							</div>
							<div class="form-group col-md-3">
								<label>
									{{ Form::checkbox('estado_paleativo', true , $paciente->estado_paleativo) }}
									Estado Paleativo
								</label>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<h4>Datos del Hogar</h4>
								<hr/>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3">
								{{ Form::label('nombre_padre', 'Nombre del Padre') }}
								{{ Form::text('nombre_padre', empty($paciente->id) ? null : $paciente->nombre_padre, array("disabled",'placeholder' => 'Nombre del Padre', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>
							<div class="form-group col-md-3">
								{{ Form::label('dni_padre', 'Número de documento') }}
								{{ Form::text('dni_padre', empty($paciente->id) ? null : $paciente->dni_padre, array("disabled",'placeholder' => 'Documento del Padre', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>

							<div class="form-group col-md-3">
								{{ Form::label('estudio_padre', 'Estudios del Padre') }}
								{{ Form::text('estudio_padre', empty($paciente->id) ? null : $paciente->estudio_padre, array("disabled",'placeholder' => 'Estudios del Padre', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>
							
							<div class="form-group col-md-3">
								{{ Form::label('estudio_padre_estado', 'Estado del Estudio') }}
								{{ Form::text('estudio_padre_estado', empty($paciente->id) ? null : ucfirst($paciente->estudio_padre_estado), array("disabled", 'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>
							
						</div>
						<div class="row">
							<div class="form-group col-md-3">
								{{ Form::label('nombre_madre', 'Nombre de la Madre') }}
								{{ Form::text('nombre_madre', empty($paciente->id) ? null : $paciente->nombre_madre, array("disabled",'placeholder' => 'Nombre de la Madre', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>
							<div class="form-group col-md-3">
								{{ Form::label('dni_madre', 'Número de documento') }}
								{{ Form::text('dni_madre', empty($paciente->id) ? null : $paciente->dni_madre, array("disabled",'placeholder' => 'Documento de la Madre', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>
							<div class="form-group col-md-3">
								{{ Form::label('estudio_madre', 'Estudios de la Madre') }}
								{{ Form::text('estudio_madre', empty($paciente->id) ? null : $paciente->estudio_madre, array("disabled",'placeholder' => 'Estudios de la Madre', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>
							<div class="form-group col-md-3">
								{{ Form::label('estudio_madre_estado', 'Estado del Estudio') }}
								{{ Form::text('estudio_madre_estado', empty($paciente->id) ? null : ucfirst($paciente->estudio_madre_estado), array("disabled", 'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3">
								{{ Form::label('nombre_tutor', 'Nombre del Tutor') }}
								{{ Form::text('nombre_tutor', empty($paciente->id) ? null : $paciente->nombre_tutor, array("disabled",'placeholder' => 'Nombre del Tutor', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>
							<div class="form-group col-md-3">
								{{ Form::label('dni_tutor', 'Número de documento') }}
								{{ Form::text('dni_tutor', empty($paciente->id) ? null : $paciente->dni_tutor, array("disabled",'placeholder' => 'Documento del Tutor', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>
							<div class="form-group col-md-3">
								{{ Form::label('estudio_tutor', 'Estudios del Tutor') }}
								{{ Form::text('estudio_tutor', empty($paciente->id) ? null : $paciente->estudio_tutor, array("disabled",'placeholder' => 'Estudios del Tutor', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>
							<div class="form-group col-md-3">
								{{ Form::label('estudio_tutor_estado', 'Estado del Estudio') }}
								{{ Form::text('estudio_tutor_estado', empty($paciente->id) ? null : ucfirst($paciente->estudio_tutor_estado), array("disabled", 'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>
						</div>

						<div class="row">
							<div class="table-responsive col-md-9">
								{{ Form::label('hermanos', 'Hermanos') }}
								<table  class="table table-bordered table-hover tablesorter">
									<thead>
										<tr>
											<th>Nombre</th>
											<th>DNI</th>
											<th>Fecha Nacimiento</th>
										</tr>
									</thead>
									<tbody>
										@foreach($paciente->hermanos as $hermano)
										<tr>
											<td id="nombre">
												{{ $hermano->nombre }}
											</td>
											<td id="dni">
												{{ $hermano->dni }}
											</td>
											<td id="edad">
												{{ $hermano->fecha_nacimiento }}
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>



						<div class="row">
							<div class="form-group col-md-3">
								{{ Form::label('ocupacion_vivienda', 'Ocupación de la Vivienda') }}
								{{ Form::text('ocupacion_vivienda', empty($paciente->id) ? null : $paciente->ocupacion_vivienda, array("disabled",'placeholder' => 'Ocupación de la Vivienda', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>
							<div class="form-group col-md-3">
								{{ Form::label('medio_devida', 'Medio de vida') }}
								{{ Form::text('medio_devida', empty($paciente->id) ? null : $paciente->medio_devida, array("disabled",'placeholder' => 'Medio de vida', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>
							<div class="form-group col-md-3">
								{{ Form::label('distancia_camino', 'Distancia de camino terrado (Km)') }}
								{{ Form::text('distancia_camino', empty($paciente->id) ? null : $paciente->distancia_camino, array("disabled",'placeholder' => 'Distancia de camino terrado', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>

						</div>
						<div class="row">
							<div class="form-group col-md-3">
								{{ Form::label('localidad_id', 'Localidad') }}
								{{ Form::text('localidad_id', empty($paciente->id) ? null : Helpers::getLocalidad($paciente->localidad_id), array("disabled",'placeholder' => 'Distancia de camino terrado', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>
							<div class="form-group col-md-3">
								{{ Form::label('direccion', 'Dirección') }}
								{{ Form::text('direccion', empty($paciente->id) ? null : $paciente->direccion, array("disabled",'placeholder' => 'Dirección del Hogar', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>
							<div class="form-group col-md-3">
								{{ Form::label('tel_contacto', 'Teléfono fijo') }}
								{{ Form::text('tel_contacto', empty($paciente->id) ? null : $paciente->tel_contacto, array("disabled",'placeholder' => 'ej. 03764425896', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3">
								{{ Form::label('cel_contacto', 'Celular *(incluir 0 y 15)') }}
								{{ Form::text('cel_contacto', empty($paciente->id) ? null : $paciente->cel_contacto, array("disabled",'placeholder' => 'ej. 0376154225566', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>
							<div class="form-group col-md-3">
								{{ Form::label('cel_contacto_2', 'Celular *(incluir 0 y 15)') }}
								{{ Form::text('cel_contacto_2', empty($paciente->id) ? null : $paciente->cel_contacto_2, array("disabled",'placeholder' => 'ej. 0376154225588', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-6">
								{{ Form::label('techo', 'Techo de') }}
								<div class="checkbox-inline">
									<label class="checkbox-inline">
										{{ Form::checkbox('techo_zinc', true , $paciente->techo_zinc) }}
										Zinc
									</label>
									<label class="checkbox-inline">
										{{ Form::checkbox('techo_carton', true , $paciente->techo_carton) }}
										Cartón
									</label>
								</div>

								{{ Form::text('techo_otro', empty($paciente->id) ? null : $paciente->techo_otro, array("disabled",'placeholder' => 'Otros (Techo)' ,'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-6">
								{{ Form::label('paredes', 'Paredes de') }}
								<div class="checkbox-inline">
									<label class="checkbox-inline">
										{{ Form::checkbox('pared_madera', true , $paciente->pared_madera) }}
										Madera
									</label>
									<label class="checkbox-inline">
										{{ Form::checkbox('pared_material', true , $paciente->pared_material) }}
										Material
									</label>
								</div>
								{{ Form::text('pared_otro', empty($paciente->id) ? null : $paciente->pared_otro, array("disabled",'placeholder' => 'Otros (Pared)' ,'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-6">
								{{ Form::label('pisos', 'Pisos de') }}
								<div class="checkbox-inline">
									<label class="checkbox-inline">
										{{ Form::checkbox('piso_ceramica', true , $paciente->piso_ceramica) }}
										Cerámica
									</label>
									<label class="checkbox-inline">
										{{ Form::checkbox('piso_alisado', true , $paciente->piso_alisado) }}
										Alisado
									</label>
								</div>
								{{ Form::text('piso_otro', empty($paciente->id) ? null : $paciente->piso_otro, array("disabled",'placeholder' => 'Otros (Piso)' ,'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-6">
								{{ Form::label('agua', 'Suministro de agua potable') }}
								<div class="checkbox-inline">
									<label class="checkbox-inline">
										{{ Form::checkbox('agua_pozo_balde', true , $paciente->agua_pozo_balde) }}
										Pozo de balde
									</label>
									<label class="checkbox-inline">
										{{ Form::checkbox('agua_canilla_publica', true , $paciente->agua_canilla_publica) }}
										Canilla pública
									</label>
								</div>
								{{ Form::text('agua_otro', empty($paciente->id) ? null : $paciente->agua_otro, array("disabled",'placeholder' => 'Otros (Agua)' ,'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>
						</div>

						<div class="row">
							<div class="form-group col-md-6">
								{{ Form::label('sanitario', 'Sanitarios') }}
								<div class="checkbox-inline">
									<label class="checkbox-inline">
										{{ Form::checkbox('sanitario_interno', true , $paciente->sanitario_interno) }}
										Interno
									</label>
									<label class="checkbox-inline">
										{{ Form::checkbox('sanitario_externo', true , $paciente->sanitario_externo) }}
										Externo
									</label>
									<label class="checkbox-inline">
										{{ Form::checkbox('sanitario_completo', true , $paciente->sanitario_completo) }}
										Completo
									</label>
								</div>
								{{ Form::text('sanitario_otro', empty($paciente->id) ? null : $paciente->sanitario_otro, array("disabled",'placeholder' => 'Otros (Sanitarios)' ,'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3">
								{{ Form::label('nro_habitantes_casa', 'Número de Habitantes') }}
								{{ Form::number('nro_habitantes_casa', empty($paciente->id) ? null : $paciente->nro_habitantes_casa, array("disabled",'placeholder' => 'Nro. de habitantes en el hogar', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>
							<div class="form-group col-md-3">
								{{ Form::label('nro_habitaciones', 'Número de Habitaciones') }}
								{{ Form::number('nro_habitaciones', empty($paciente->id) ? null : $paciente->nro_habitaciones, array("disabled",'placeholder' => 'Nro. de habitaciones', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3">
								{{ Form::label('nro_personas_que_trabajan', 'Habitantes que trabajan') }}
								{{ Form::number('nro_personas_que_trabajan', empty($paciente->id) ? null : $paciente->nro_personas_que_trabajan, array("disabled",'placeholder' => 'Nro. de habitantes que trabajan', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
							</div>
							<br/>
							<div class="form-group col-md-6">
								{{ Form::label('trabajo_tipo', 'Tipo') }}
								<div class="checkbox-inline">
									<label class="checkbox-inline">
										{{ Form::checkbox('trabajo_fijo', true , $paciente->trabajo_fijo) }}
										Fijo
									</label>
									<label class="checkbox-inline">
										{{ Form::checkbox('trabajo_temporario', true , $paciente->trabajo_temporario) }}
										Temporario
									</label>
									<label class="checkbox-inline">
										{{ Form::checkbox('trabajo_porsucuenta', true , $paciente->trabajo_porsucuenta) }}
										Por su cuenta
									</label>
								</div>

							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-6">
								{{ Form::label('observaciones', 'Otras Observaciones') }}
								{{ Form::textarea('observaciones', empty($paciente->id) ? null : $paciente->observaciones, array("disabled",'placeholder' => 'Observaciones...', 'class' => 'form-control' , 'autocomplete' => 'off', 'rows' => '2')) }}
							</div>
						</div>

						

					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="asistencia">
				<div class="row">
					<div class="table-responsive col-md-9">
						<h4 class="text-info">Asistencias del paciente por período.</h4>
						<table  class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>Fecha de Inicio</th>
									<th>Fecha de Cierre</th>
									<th>Nro. Asistencias</th>
									<th>Observaciones</th>
									@if(Auth::user()->es_admin)
										<th>Acción</th>
									@endif
									
								</tr>
							</thead>
							<tbody>
								@foreach($asistencias as $asistencia)
								<tr>
									<td>
										{{ $asistencia->fecha_inicio }}
									</td>
									<td>
										{{ $asistencia->fecha_cierre }}
									</td>
									<td>
										{{ $asistencia->nro_asistencias }}
									</td>
									<td>
										@if(!empty($asistencia->observaciones))
											{{ $asistencia->observaciones }}
										@else
											N/A
										@endif
									</td>
									@if(Auth::user()->es_admin)
										<td>
											<a href="asistencias/editar/{{ $asistencia->id }}"  class="btn btn-sm btn-primary"> <i class="fa fa-edit"></i> Editar</a>
										</td>
									@endif
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="alimento">
				<div class="col-md-3">
					<p><strong>Cantidad total asignada:</strong> @if(!empty($alimentos_total[0])) {{ $alimentos_total[0]->cantidad_total }}  @endif </p>
				</div>
				<div class="row">
					<div class="table-responsive col-md-9">
						<h4 class="text-info">Últimos 10 alimentos registrados.</h4>
						<table  class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>Descripción del Alimento</th>
									<th>Cantidad</th>
									<th>Fecha</th>
								</tr>
							</thead>
							<tbody>
								@foreach($alimentos as $alimento)
								<tr>
									<td>
										@if(!empty($alimento->observaciones))
										{{ $alimento->observaciones }}
										@else
										N/A
										@endif
									</td>
									<td>
										{{ $alimento->cantidad }}
									</td>
									<td>
										{{ $alimento->fecha_otorgamiento }}
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="derivacion">
				<div class="row">
					<div class="table-responsive col-md-9">
						<h4 class="text-info">Últimas 10 derivaciones registradas.</h4>
						<table  class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>Lugar</th>
									<th>Referencia</th>
									<th>Fecha</th>
								</tr>
							</thead>
							<tbody>
								@foreach($derivaciones as $derivacion)
								<tr>
									<td>
										@if(!empty($derivacion->lugar))
											{{ $derivacion->lugar }}
										@else
											N/A
										@endif
									</td>
									<td>
										{{ ucfirst($derivacion->referencia) }}
									</td>
									<td>
										{{ $derivacion->fecha }}
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="ropero">
				<div class="col-md-3">
					<p><strong>Cantidad total Roperos Nuevos:</strong> @if(!empty($roperos_total[0])){{ $roperos_total[0]->cantidad_total }} @else 0 @endif</p>
					<p><strong>Cantidad total Roperos Usados:</strong> @if(!empty($roperos_total[1])){{ $roperos_total[1]->cantidad_total }} @else 0 @endif</p>
				</div>
				<div class="row">
					<div class="table-responsive col-md-9">
						<h4 class="text-info">Últimos 10 roperos registrados.</h4>
						<table  class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>Descripción</th>
									<th>Cantidad</th>
									<th>Fecha</th>
								</tr>
							</thead>
							<tbody>
								@foreach($roperos as $ropero)
								<tr>
									<td>
										@if(!empty($ropero->observaciones))
										{{ $ropero->observaciones }}
										@else
										N/A
										@endif

									</td>
									<td>
										{{ $ropero->cantidad }}
									</td>
									<td>
										{{ $ropero->fecha_otorgamiento }}
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="juguete">
				<div class="col-md-3">
					<p><strong>Cantidad total Juguetes Nuevos:</strong> @if(!empty($juguetes_total[0])){{ $juguetes_total[0]->cantidad_total }} @else 0 @endif</p>
					<p><strong>Cantidad total Juguetes Usados:</strong> @if(!empty($juguetes_total[1])){{ $juguetes_total[1]->cantidad_total }} @else 0 @endif</p>
				</div>
				<div class="row">
					<div class="table-responsive col-md-9">
						<h4 class="text-info">Últimos 10 juguetes registrados.</h4>
						<table  class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>Descripción</th>
									<th>Cantidad</th>
									<th>Fecha</th>
								</tr>
							</thead>
							<tbody>
								@foreach($juguetes as $juguete)
								<tr>
									<td>
										@if(!empty($juguete->observaciones))
										{{ $juguete->observaciones }}
										@else
										N/A
										@endif

									</td>
									<td>
										{{ $juguete->cantidad }}
									</td>
									<td>
										{{ $juguete->fecha_otorgamiento }}
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="donacion">
				<div class="row">
					<div class="table-responsive col-md-9">
						<h4 class="text-info">Últimas 10 Donaciones / Compras.</h4>
						<table  class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>Descripción</th>
									<th>Cantidad</th>
									<th>Procedencia</th>
									<th>Total</th>
									<th>Fecha</th>
								</tr>
							</thead>
							<tbody>
								@foreach($donaciones_compras as $d)
								<tr>
									<td>
										@if(!empty($d->descripcion))
											{{ $d->descripcion }}
										@else
											N/A
										@endif
									</td>
									<td>
										{{ $d->cantidad }}
									</td>
									<td>
										@if($d->procedencia)
											Donación
										@else
											Compra
										@endif
									</td>
									<td>
										@if(!empty($d->montototal))
										$ {{ $d->montototal }}
										@else
										$ 0.00
										@endif

									</td>
									<td>
										{{ $d->fecha }}
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="pasajes">
				<div class="row">
					<div class="table-responsive col-md-9">
						<h4 class="text-info">Últimos 10 pasajes registrados.</h4>
						<table  class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>Origen</th>
									<th>Destino</th>
									<th>Tipo Pasaje</th>
									<th>Cantidad</th>
									<th>Fecha</th>
									<th>Monto</th>
								</tr>
							</thead>
							<tbody>
								@foreach($pasajes as $pasaje)
								<tr>
									<td>
										@if(!empty($pasaje->origen))
										{{ $pasaje->origen }}
										@else
										N/A
										@endif

									</td>
									<td>
										@if(!empty($pasaje->destino))
										{{ $pasaje->destino }}
										@else
										N/A
										@endif
									</td>
									<td>
										{{ ucfirst($pasaje->tipo_pasaje) }}
									</td>
									<td>
										{{ $pasaje->cantidad }}
									</td>
									<td>
										{{ $pasaje->fecha }}
									</td>
									<td>
										@if(!empty($pasaje->monto))
										{{ $pasaje->monto }}
										@else
										N/A
										@endif
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="prestamos">
				<div class="row">
					<div class="table-responsive col-md-9">
						<h4 class="text-info">Préstamos que faltan devolver.</h4>
						<table  class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>Descripción</th>
									<th>Cantidad</th>
									<th>Fecha Salida</th>
									<th>Acción</th>
								</tr>
							</thead>
							<tbody>
								@foreach($prestamos as $prestamo)
								<tr>
									<td>
										@if(!empty($prestamo->descripcion))
										{{ $prestamo->descripcion }}
										@else
										N/A
										@endif

									</td>
									<td>
										@if(!empty($prestamo->cantidad))
										{{ $prestamo->cantidad }}
										@else
										N/A
										@endif
									</td>
									<td>
										@if(!empty($prestamo->fecha_salida))
										{{ $prestamo->fecha_salida }}
										@else
										N/A
										@endif
									</td>
									<td>
										@if(Auth::user()->es_admin)
											<a href="beneficios/devolucionprestamo/{{ $prestamo->id }}"  class="btn btn-sm btn-success"> <i class="fa fa-edit"></i> Registrar Devolución</a>
										@else
											<a href="#"  class="btn btn-sm btn-success disabled"> <i class="fa fa-edit"></i> Registrar Devolución</a>
										@endif
										
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="tramites">
				<div class="row">
					<div class="table-responsive col-md-9">
						<h4 class="text-info">Últimos 10 trámites registrados.</h4>
						<table  class="table table-bordered table-hover tablesorter">
							<thead>
								<tr>
									<th>Descripción</th>
									<th>Monto</th>
									<th>Fecha</th>
								</tr>
							</thead>
							<tbody>
								@foreach($tramites as $tramite)
								<tr>
									<td>
										@if(!empty($tramite->descripcion))
										{{ $tramite->descripcion }}
										@else
										N/A
										@endif

									</td>
									<td>
										@if(!empty($tramite->monto))
										$ {{ $tramite->monto }}
										@else
										$ 0.00
										@endif
									</td>
									<td>
										@if(!empty($tramite->fecha))
										{{ $tramite->fecha }}
										@else
										N/A
										@endif
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br>




</div>
@show

@section('js')
<script>


</script>
@show