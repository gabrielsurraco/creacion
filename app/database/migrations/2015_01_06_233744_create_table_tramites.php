<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTramites extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tramites', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('paciente_id');
			$table->string("descripcion",300);
			$table->float('monto')->default(0);
			$table->date('fecha');
			
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists("tramites");
	}

}
