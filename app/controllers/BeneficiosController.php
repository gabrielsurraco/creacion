<?php

/**
 * Description of BeneficiosController
 *
 * @author Ing. Gabriel Surraco
 */
class BeneficiosController extends BaseController{
	
	
	
	
	//put your code here
	public function getAlimento($id = null){
		$usuario = Auth::user();
		
		if(!($usuario->es_admin)){
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}
		
		$id = strip_tags($id);
		
		$this->layout->content = View::make("beneficios/alimento" , compact('id'));
	}
	
	public function getRopero($id = null){
		$usuario = Auth::user();
		
		if(!($usuario->es_admin)){
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}
		
		$id = strip_tags($id);
		
		$this->layout->content = View::make("beneficios/ropero" , compact('id'));
	}
	
	public function getJuguete($id = null){
		$usuario = Auth::user();
		
		if(!($usuario->es_admin)){
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}
		
		$id = strip_tags($id);
		
		$this->layout->content = View::make("beneficios/juguete" , compact('id'));
	}
	
	public function getDonacionycompra($id = null){
		$usuario = Auth::user();
		
		if(!($usuario->es_admin)){
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}
		
		$id = strip_tags($id);
		
//		$paciente = Paciente::with('donacionesycompras', 'donacionesycompras.paciente')->findOrFail($id);
		
		$this->layout->content = View::make("beneficios/donacionycompra" , compact('id'/*,'paciente'*/));
	}
	
	public function getTramite($id = null){
		$usuario = Auth::user();
		
		if(!($usuario->es_admin)){
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}
		
		$id = strip_tags($id);
		
		$this->layout->content = View::make("beneficios/tramite" , compact('id'));
	}
	
	public function postTramite(){
		$usuario = Auth::user();

		if (!($usuario->es_admin)) {
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}
		
		$inputs = Input::all();
		
		$reglas = array();
		
		$reglas["fecha"] = 'required';
		$reglas["monto"] = 'numeric';
		$reglas["descripcion"] = 'required|max:300';
		
		$mensajes = array(
			'required' => 'Debe completar el campo!',
			'max' => 'Superó el máximo de 300 caracteres',
			'numeric' => "Debe ser un número"
		);
		
		$validar = Validator::make($inputs, $reglas, $mensajes);

		if ($validar->fails()) 
			return Redirect::back()->withErrors($validar)->withInput();
		
		// Actualiza Tramite
		try{
			// Inicio transaccion
			DB::beginTransaction();
			
			$tramite = new Tramite();
			
			$tramite->paciente_id = $inputs["id_paciente"];
			$tramite->fecha = $inputs["fecha"];
			if(!empty($inputs["monto"]))
				$tramite->monto = $inputs["monto"];
			$tramite->descripcion = $inputs["descripcion"];
			
			$tramite->save();
				
			// Commit
			DB::commit();
			
			if(!empty($inputs["id_paciente"]) && isset($inputs["id_paciente"]))
				return Redirect::to('pacientes/beneficios/'.$inputs["id_paciente"])->with('success', 'Trámite registrado correctamente!!');
			else
				return Redirect::to('pacientes')->with('success', 'Trámite registrado correctamente!!');
			
			
		} catch (\PDOException $e) {
			DB::rollBack();
			throw $e;
		}
		
	}
	
	public function getPrestamocolaborador($id = null){
		$usuario = Auth::user();
		
		if(!($usuario->es_admin)){
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}
		
		$id = strip_tags($id);
		$es_colaborador = true;
		
		$this->layout->content = View::make("beneficios/prestamo" , compact('id','es_colaborador'));
	}
	
//	public function postPrestamocolaborador(){
//		
//	}
	
	
	public function getPrestamo($id = null){
		$usuario = Auth::user();
		
		if(!($usuario->es_admin)){
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}
		
		$id = strip_tags($id);
		
		$this->layout->content = View::make("beneficios/prestamo" , compact('id'));
		
	}
	
	public function postPrestamo(){
		$usuario = Auth::user();

		if (!($usuario->es_admin)) {
			return Redirect::to('pacientes')->with('error', 'Acceso no permitido');
		}
		
		$inputs = Input::all();
		
		$reglas = array();
		
		$reglas["descripcion"] = 'required';
		$reglas["fecha_salida"] = 'required';
		$reglas["cantidad"] = 'required|numeric|min:1';
		
		$mensajes = array(
			'required' => 'Debe completar el campo!',
			'min' => 'El valor mínimo debe ser 1',
			'numeric' => "Debe ser un número"
		);
		
		$validar = Validator::make($inputs, $reglas, $mensajes);
		
		if ($validar->fails()) 
			return Redirect::back()->withErrors($validar)->withInput();
		
		// Actualiza Pasaje
		try{
			// Inicio transaccion
			DB::beginTransaction();
			
			$prestamo = new Prestamo();
			if(isset($inputs["es_colaborador"]) && $inputs["es_colaborador"])
				$prestamo->colaborador_id = $inputs["id"];
			else
				$prestamo->paciente_id = $inputs["id"];
			
			$prestamo->descripcion = $inputs["descripcion"];
			$prestamo->fecha_salida = $inputs["fecha_salida"];
			$prestamo->cantidad = $inputs["cantidad"];
			
			$prestamo->save();
				
			// Commit
			DB::commit();
			if(isset($inputs["es_colaborador"]) && $inputs["es_colaborador"] == true)
				return Redirect::to('colaboradores')->with('success', 'Préstamo registrado correctamente');
			else
				return Redirect::to('pacientes/beneficios/'.$inputs["id"])->with('success', 'Préstamo registrado correctamente');
			
			
		} catch (\PDOException $e) {
			DB::rollBack();
			throw $e;
		}
	}
	
	

	public function getPasaje($id = null){
		
		$usuario = Auth::user();
		
		if(!($usuario->es_admin)){
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}
		
		$id = strip_tags($id);
		
		$this->layout->content = View::make("beneficios/pasaje" , compact('id'));
	}

	public function postPasaje(){
		$usuario = Auth::user();

		if (!($usuario->es_admin)) {
			return Redirect::to('pacientes')->with('error', 'Acceso no permitido');
		}
		
		$inputs = Input::all();
		
		$reglas = array();
		
		$reglas["origen"] = 'required';
		$reglas["fecha"] = 'required';
		$reglas["cantidad"] = 'required|numeric|min:1';
		$reglas["monto"] = 'numeric';
		
		$mensajes = array(
			'required' => 'Debe completar el campo!',
			'min' => 'El valor mínimo debe ser 1',
			'numeric' => "Debe ser un número"
		);
		
		$validar = Validator::make($inputs, $reglas, $mensajes);

		if ($validar->fails()) 
			return Redirect::back()->withErrors($validar)->withInput();
		
		// Actualiza Pasaje
		try{
			// Inicio transaccion
			DB::beginTransaction();
			
			$pasaje = new Pasaje();
			
			$pasaje->paciente_id = $inputs["id_paciente"];
			$pasaje->origen = $inputs["origen"];
			if(!empty($inputs["destino"]))
				$pasaje->destino = $inputs["destino"];
			$pasaje->fecha = $inputs["fecha"];
			$pasaje->cantidad = $inputs["cantidad"];
			if(!empty($inputs["monto"]))
				$pasaje->monto = $inputs["monto"];
			$pasaje->tipo_pasaje = $inputs["tipo_pasaje"];
			
			$pasaje->save();
				
			// Commit
			DB::commit();
			return Redirect::to('pacientes/beneficios/'.$inputs["id_paciente"])->with('success', 'Pasaje registrado correctamente');
			
			
		} catch (\PDOException $e) {
			DB::rollBack();
			throw $e;
		}
		
	}

	public function postDonacionycompra(){
		$usuario = Auth::user();

		if (!($usuario->es_admin)) {
			return Redirect::to('pacientes')->with('error', 'Acceso no permitido');
		}
		
		$inputs = Input::all();
		
		$reglas = array();
		
		$reglas["fecha"] = 'required';
		$reglas["cantidad"] = 'required|numeric|min:1';
		$reglas["descripcion"] = 'required';
		
		$mensajes = array(
			'required' => 'Debe completar el campo!',
			'min' => 'El valor mínimo debe ser 1',
			'numeric' => "Debe ser un número"
		);	
			
		$validar = Validator::make($inputs, $reglas, $mensajes);

		if ($validar->fails()) 
			return Redirect::back()->withErrors($validar)->withInput();
			
		// Actualiza DonacionyCompra
		try{
			// Inicio transaccion
			DB::beginTransaction();
			
			$donacion_compra = new Donacionycompra();
			
			$donacion_compra->paciente_id = $inputs["id_paciente"];
			$donacion_compra->fecha = $inputs["fecha"];
			$donacion_compra->descripcion = $inputs["descripcion"];
			$donacion_compra->cantidad = $inputs["cantidad"];
			$donacion_compra->montototal = $inputs["montototal"];
			$donacion_compra->procedencia = $inputs["procedencia"];
			
			$donacion_compra->save();
				
			// Commit
			DB::commit();
			
			
			if(!empty($inputs["id_paciente"]) && isset($inputs["id_paciente"]))
				return Redirect::to('/pacientes/beneficios/'.$inputs["id_paciente"])->with('success', 'Compra / Donación registrado correctamente!');
			else
				return Redirect::to('/pacientes')->with('success', 'Compra / Donación registrado correctamente!');
			
			
		} catch (\PDOException $e) {
			DB::rollBack();
			throw $e;
		}
			
	}
	
	public function postJuguete(){
		$usuario = Auth::user();
		
		if(!($usuario->es_admin)){
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}
		
		$cantidad_nuevo = Stock::select("cantidad")->where("tipo","juguete_nuevo")->get();
		$cantidad_usado = Stock::select("cantidad")->where("tipo","juguete_usado")->get();
		
		
		$inputs = Input::all();
		
		$reglas = array();
		
		$reglas['fecha_otorgamiento'] = 'required';
		if($inputs["tipo"] == "nuevo")
			$reglas['cantidad'] = 'required|numeric|min:1|max:'.$cantidad_nuevo[0]->cantidad.'';
		elseif($inputs["tipo"] == "usado")
			$reglas['cantidad'] = 'required|numeric|min:1|max:'.$cantidad_usado[0]->cantidad.'';
		
		$mensajes = array(
			'required' => 'Debe completar el campo!',
			'numeric' => 'Debe ser un número',
			'min' => '1 es el valor mínimo',
			'max' => 'Stock solicitado mayor al existente'
			
		);
		
		$validar = Validator::make($inputs, $reglas, $mensajes);

		if ($validar->fails()) 
			return Redirect::back()->withErrors($validar)->withInput();
		
		
		// Actualiza Stock
		try{
			// Inicio transaccion
			DB::beginTransaction();
			
			if($inputs["tipo"] == "nuevo")
				Stock::where("tipo","juguete_nuevo")->decrement('cantidad', $inputs["cantidad"]);
			elseif($inputs["tipo"] == "usado")
				Stock::where("tipo","juguete_usado")->decrement('cantidad', $inputs["cantidad"]);
			
			$juguete_paciente = new Juguete();
			
			$juguete_paciente->paciente_id = $inputs["id_paciente"];
			$juguete_paciente->fecha_otorgamiento = $inputs["fecha_otorgamiento"];
			$juguete_paciente->cantidad = $inputs["cantidad"];
			$juguete_paciente->tipo = $inputs["tipo"];
			
			if(!empty($inputs["observaciones"]))
				$juguete_paciente->observaciones = $inputs["observaciones"];
			
			$juguete_paciente->save();
				
			// Commit
			DB::commit();
			if(!empty($inputs["id_paciente"]) && isset($inputs["id_paciente"]))
				return Redirect::to('pacientes/beneficios/'.$inputs["id_paciente"])->with('success', 'Juguete/s asignado/s correctamente!');
			else
				return Redirect::to('pacientes')->with('success', 'Juguete/s asignado/s correctamente!');
			
		} catch (\PDOException $e) {
			DB::rollBack();
			throw $e;
		}
		
	}
	
	public function postRopero(){
		$usuario = Auth::user();
		
		if(!($usuario->es_admin)){
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}
		
		$cantidad_nuevo = Stock::select("cantidad")->where("tipo","ropero_nuevo")->get();
		$cantidad_usado = Stock::select("cantidad")->where("tipo","ropero_usado")->get();
		
		
		$inputs = Input::all();
		
		$reglas = array();
		
		$reglas['fecha_otorgamiento'] = 'required';
		if($inputs["tipo"] == "nuevo")
			$reglas['cantidad'] = 'required|numeric|min:1|max:'.$cantidad_nuevo[0]->cantidad.'';
		elseif($inputs["tipo"] == "usado")
			$reglas['cantidad'] = 'required|numeric|min:1|max:'.$cantidad_usado[0]->cantidad.'';
		
		$mensajes = array(
			'required' => 'Debe completar el campo!',
			'numeric' => 'Debe ser un número',
			'min' => '1 es el valor mínimo',
			'max' => 'Stock solicitado mayor al existente'
			
		);
		
		$validar = Validator::make($inputs, $reglas, $mensajes);

		if ($validar->fails()) 
			return Redirect::back()->withErrors($validar)->withInput();
		
		
		// Actualiza Stock
		try{
			// Inicio transaccion
			DB::beginTransaction();
			
			if($inputs["tipo"] == "nuevo")
				Stock::where("tipo","ropero_nuevo")->decrement('cantidad', $inputs["cantidad"]);
			elseif($inputs["tipo"] == "usado")
				Stock::where("tipo","ropero_usado")->decrement('cantidad', $inputs["cantidad"]);
			
			
			$ropero_paciente = new Ropero();
			
			$ropero_paciente->paciente_id = $inputs["id_paciente"];
			$ropero_paciente->fecha_otorgamiento = $inputs["fecha_otorgamiento"];
			$ropero_paciente->cantidad = $inputs["cantidad"];
			$ropero_paciente->tipo = $inputs["tipo"];
			
			if(!empty($inputs["observaciones"]))
				$ropero_paciente->observaciones = $inputs["observaciones"];
			
			$ropero_paciente->save();
				
			// Commit
			DB::commit();
			if(!empty($inputs["id_paciente"]) && isset($inputs["id_paciente"]))
				return Redirect::to('pacientes/beneficios/'.$inputs["id_paciente"])->with('success', 'Ropero asignado correctamente!');
			else
				return Redirect::to('pacientes')->with('success', 'Ropero asignado correctamente!');
			
		} catch (\PDOException $e) {
			DB::rollBack();
			throw $e;
		}
		
	}
	
	public function postAlimento(){
		$usuario = Auth::user();
		
		if(!($usuario->es_admin)){
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}
		
		$cantidad = Stock::select("cantidad")->where("tipo","alimento")->get();
		
		
		$inputs = Input::all();
		
		$reglas = array();

		$reglas['fecha_otorgamiento'] = 'required';
		$reglas['cantidad'] = 'required|numeric|min:1|max:'.$cantidad[0]->cantidad.'';

		$mensajes = array(
			'required' => 'Debe completar el campo!',
			'numeric' => 'Debe ser un número',
			'min' => '1 es el valor mínimo',
			'max' => 'Stock solicitado mayor al existente'
			
		);

		$validar = Validator::make($inputs, $reglas, $mensajes);

		if ($validar->fails()) 
			return Redirect::back()->withErrors($validar)->withInput();
		
		// Actualiza Stock
		try{
			// Inicio transaccion
			DB::beginTransaction();
			
			Stock::where("tipo","alimento")->decrement('cantidad', $inputs["cantidad"]);
			
			$alimento_paciente = new Alimento();
			
			if(isset($inputs["id_paciente"]))
				$alimento_paciente->paciente_id = $inputs["id_paciente"];
			if(isset($inputs["fecha_otorgamiento"]))
				$alimento_paciente->fecha_otorgamiento = $inputs["fecha_otorgamiento"];
			if(isset($inputs["cantidad"]))
				$alimento_paciente->cantidad = $inputs["cantidad"];
			if(isset($inputs["observaciones"]))
				$alimento_paciente->observaciones = $inputs["observaciones"];
			
			$alimento_paciente->save();
				
			// Commit
			DB::commit();
			if(!empty($inputs["id_paciente"]) && isset($inputs["id_paciente"]))
				return Redirect::to('pacientes/beneficios/'.$inputs["id_paciente"])->with('success', 'Alimento asignado correctamente!');
			else
				return Redirect::to('pacientes')->with('success', 'Alimento asignado correctamente!');
				
		} catch (\PDOException $e) {
			DB::rollBack();
			throw $e;
		}
		
		
		
	}
	
	public function getDevolucionprestamo($id = null){
		$usuario = Auth::user();
		
		if(!($usuario->es_admin)){
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}
		
		$id = strip_tags($id);
		
		$prestamo = Prestamo::select("id","descripcion","cantidad","fecha_salida")->where("id",$id)->get();
		
		$this->layout->content = View::make("beneficios/devolucionprestamo" , compact('prestamo'));
	}
	
	public function postDevolucionprestamo(){
		$usuario = Auth::user();
		
		if(!($usuario->es_admin)){
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}
		
		$inputs = Input::all();
//		return $inputs;
//		$prestamo2 = Prestamo::select("colaborador_id")->where('id',$inputs["prestamo_id"])->get();
//		return $prestamo2[0]->colaborador_id;
		$reglas = array();

		$reglas['fecha_devolucion'] = 'required';

		$mensajes = array(
			'required' => 'Debe completar el campo!'
		);

		$validar = Validator::make($inputs, $reglas, $mensajes);

		if ($validar->fails()) 
			return Redirect::back()->withErrors($validar)->withInput();
		
		// Actualiza Stock
		try{
			// Inicio transaccion
			DB::beginTransaction();
			
			$prestamo = Prestamo::findOrFail($inputs["prestamo_id"]);
			
				$prestamo->fecha_devolucion = $inputs["fecha_devolucion"];
				$prestamo->descripcion_devolucion = $inputs["descripcion_devolucion"];
			
			$prestamo->update();
				
			// Commit
			DB::commit();
			$prestamo2 = Prestamo::select("colaborador_id","paciente_id")->where('id',$inputs["prestamo_id"])->get();
			
			if($prestamo2[0]->colaborador_id == NULL)
				return Redirect::to('pacientes/info/'.$prestamo2[0]->paciente_id)->with('success', 'Se ah registrado la devolución correctamente!');
			else
				return Redirect::to('colaboradores/info/'.$prestamo2[0]->colaborador_id)->with('success', 'Se ah registrado la devolución correctamente!');
			
			
		} catch (\PDOException $e) {
			DB::rollBack();
			throw $e;
		}
	}

	
}
