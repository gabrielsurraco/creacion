<?php

class NotasController extends BaseController {
	
    public function getIndex()
    {
		$usuario = Auth::user();

		if (!($usuario->es_admin ) && !($usuario->es_default )) {
			return Redirect::to('dashboard/principal')->with('error', 'Acceso no permitido');
		}
		
		
        $this->layout->content = View::make('notas/index');
    }
	
	public function getDownload($id = null){
		
		$usuario = Auth::user();

		if (!($usuario->es_admin ) && !($usuario->es_default )) {
			return Redirect::to('dashboard/principal')->with('error', 'Acceso no permitido');
		}
		
        //PDF file is stored under project/public/download/info.pdf
		if($id == 1){
			$file= public_path(). "/descargas/fichas_pacientes.pdf";
			$nombre = "fichas_pacientes.pdf";
		}
		
		if($id == 2){
			$file= public_path(). "/descargas/nota_creacion.docx";
			$nombre = "Nota Creacion.docx";
		}
		
        $headers = array(
			'Content-Type: application/pdf',
			'Content-Type: application/docx',
		);
		
        return Response::download($file, $nombre, $headers);
	}
    
    


}

