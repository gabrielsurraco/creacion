@section('content')

<h3>Diagn&oacute;sticos Habilitados</h3><hr/>

<div class="content">
	<div class="row">
		<div class="form-group col-lg-3">
			@if(Auth::user()->es_admin)
                {{HTML::link("diagnosticos/editar","Crear Diagnóstico",array("class"=>"btn btn-primary"))}}
            @else
                {{HTML::link("#","Crear Diagnóstico",array("class"=>"btn btn-primary disabled"))}}
            @endif
		</div>
	</div>
    <div class="row">
        <div class="col-lg-6">
            
            <div class="table-responsive">
                <table class="table table-hover table-striped tablesorter">
                    <thead>
                        <tr>
                            <th>Nombre <i class="fa fa-sort"></i></th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach($diagnosticos as $diagnostico)
                        <tr>
                                
							<td>{{ $diagnostico->nombre }}</td>
							<td>
								@if(Auth::user()->es_admin)
									<a href="diagnosticos/editar/{{$diagnostico->id}}"  class="btn btn-sm btn-primary"><i class="fa fa-edit"></i> Editar</a>
									<a href="diagnosticos/eliminar/{{$diagnostico->id}}"  class="btn btn-sm btn-default"><i class="fa fa-trash-o"></i> Eliminar</a>
								@else
									<a href="#"  class="btn btn-sm btn-primary disabled"><i class="fa fa-edit"></i> Editar</a>
									<a href="#"  class="btn btn-sm btn-default disabled"><i class="fa fa-trash-o"></i> Eliminar</a>
								@endif
							</td>

                        </tr>
                        @endforeach
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@show

@section('js')
<script>
	$(function(){
	  $(".tablesorter").tablesorter();
	});
</script>
@stop