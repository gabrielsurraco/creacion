@section('content')

<h3>Administración de Pacientes</h3><hr/>

<div class="content">
    <div class="row">
        <div class="col-lg-12">
			@if(Auth::user()->es_admin)
			{{HTML::link("pacientes/crear","Crear Paciente",array("class"=>"btn btn-primary"))}}
			@else
			{{HTML::link("#","Crear Paciente",array("class"=>"btn btn-primary disabled"))}}
			@endif
        </div>
    </div>
	<br/>

	{{ Form::open(array('action' => 'PacientesController@getIndex', 'method' => 'POST')) }}
	<div class="row">
		<div class="form-group col-md-3">
			{{ Form::label('nombre', 'Apellido y Nombre') }}
			{{ Form::text('nombre', empty($filtros["nombre"]) ? null : $filtros["nombre"], array('placeholder' => 'Apellido y Nombre...', 'class' => 'form-control')) }}
		</div>
		<div class="form-group col-md-3">
			{{ Form::label('dni', 'Número de Documento') }}
			{{ Form::text('dni', empty($filtros["dni"]) ? null : $filtros["dni"], array('placeholder' => 'Ingrese el DNI...', 'class' => 'form-control')) }}
		</div>
		<div class="form-group col-md-3">
			{{ Form::label('diagnostico_id', 'Diagnóstico') }}
			{{ Form::select("diagnostico_id", array('default' => 'Seleccionar...') + $lista_diagnosticos, empty($filtros["diagnostico_id"]) ? null : $filtros["diagnostico_id"], array("class"=>"form-control")) }}
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-3">
			{{ Form::label('localidad_id', 'Localidad') }}
			{{ Form::select("localidad_id", array('default' => 'Seleccionar...') + $lista_localidades, empty($filtros["localidad_id"]) ? null : $filtros["localidad_id"], array("class"=>"form-control")) }}
		</div>
		<div class="form-group col-md-3">
			{{ Form::label('lugar_tratamiento_id', 'Lugar de Tratamiento') }}
			{{ Form::select("lugar_tratamiento_id", array('default' => 'Seleccionar...') + $lista_institutos, empty($filtros["lugar_tratamiento_id"]) ? null : $filtros["lugar_tratamiento_id"], array("class"=>"form-control")) }}
		</div>
		<!--<br/>-->
		<!--			<div class="form-group col-md-3">
						<label>
								{{ Form::checkbox('estado_paleativo', true , false) }}
							Estado Paleativo
						</label>
					</div>-->
		<div class="form-group col-md-6">
			{{Form::label('roles','Paciente')}}
			<br>
			<label class="checkbox-inline">
				{{ Form::checkbox('estado_paleativo', true , isset($filtros["estado_paleativo"]) ? true : false) }}
				Estado Paleativo
			</label>
			<label class="checkbox-inline">
				{{ Form::checkbox('prioridad_alta', true , isset($filtros["prioridad_alta"]) ? true : false) }}
				Prioridad Alta
			</label>

			<label class="checkbox-inline">
				{{ Form::checkbox('prioridad_media', true , isset($filtros["prioridad_media"]) ? true : false) }}
				Prioridad Media
			</label>
			<label class="checkbox-inline">
				{{ Form::checkbox('fallecio', true , isset($filtros["fallecio"]) ? true : false) }}
				Falleció
			</label>
		</div>
	</div>

	<div class="row">
		<div class="form-group col-lg-3">
			<br/>
			<button type="submit" class="btn btn-default btn-group-lg"><i class="fa fa-search"></i> Buscar</button>
		</div>
	</div>
	{{ Form::close() }}

	<div class="row">

        <div class="col-lg-11">
			

			<!-- Modal -->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">Referencias</h4>
						</div>
						<div class="modal-body">
							<ul>
								<li style='color: turquoise '>Paciente trasladado</li>
								<li style='color: red '>El paciente falleció</li>
								<li style="background: #f2dede;">Paciente con prioridad alta</li>
								<li style="background: #fcf8e3;">Paciente con prioridad media</li>
								<li style="color: #E4670A ">Paciente en estado paleativo</li>
							</ul>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						</div>
					</div>
				</div>
			</div>

			@if(isset($pacientes[0]))
			<a href="pacientes/index/1"  style="margin-bottom: 10px;"  class="btn btn-sm btn-success pull-right"> <i class="fa fa-file-excel-o "> </i>  Exportar Resultados</a>
			@else
			<a href="#" style="margin-bottom: 10px;"  class="btn btn-sm btn-success pull-right disabled"> <i class="fa fa-file-excel-o "> </i>  Exportar Resultados</a>
			@endif
			<button style="margin-right: 5px;" type="button" class="btn btn-sm btn-default pull-right " data-toggle="modal" data-target="#myModal">
				<i class="fa fa-info"></i> Referencias
			</button>
            <div class="table-responsive">
                <table class="table table-hover table-striped tablesorter">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>DNI</th>
                            <th>Fecha Nacimiento</th>
                            <th>Diagnóstico</th>
                            <th title="Días transcurridos desde la fecha de diagnóstico">En Tratamiento</th>
                            <th>Dirección</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
						@foreach($pacientes as $paciente)
						<tr <?php
						if ($paciente->prioridad_alta)
							echo "class = 'danger'";
						if ($paciente->prioridad_media)
							echo "class = 'warning'";
						if ($paciente->trasladado != "")
							echo "style='color: turquoise '";
						if ($paciente->estado_paleativo)
							echo "style='color: #E4670A '";
						if ($paciente->fallecio == 1)
							echo "style='color: red '"
							?>>     
							<td>{{ $paciente->nombre }}</td>
							<td>{{ $paciente->dni }}</td>
							<td>{{ $paciente->fecha_nacimiento }}</td>
							<td>
								@if($paciente->diagnostico_id != 0)
<?php echo Helpers::getDiagnostico($paciente->diagnostico_id); ?>
								@else
<?php echo "Sin Dian&oacute;stico"; ?>
								@endif
							</td>
							<td><?php //echo Help--ers::diferenciaDias($paciente->fecha_ingreso); ?></td>
							<td>{{ $paciente->direccion }}</td>
							<td >
								<a href="pacientes/info/{{$paciente->id}}"  class="btn btn-sm btn-default"> <i class="fa fa-eye"></i></a>
								@if(Auth::user()->es_admin)
								<a href="pacientes/editar/{{$paciente->id}}"  class="btn btn-sm btn-primary"> <i class="fa fa-edit"></i></a>
								@else
								<a href="#"  class="btn btn-sm btn-primary disabled"> <i class="fa fa-edit"></i></a>
								@endif
								<!--<a href="#"  class="btn btn-sm btn-warning"> <i class="fa fa-car"></i> Derivar</a>-->
								<!--<a href="#"  class="btn btn-sm btn-primary"> <i class="fa fa-hand-o-up"></i> Dar Beneficio</a>-->
								<!--											<div class="btn-group">
																				<button type="button" class="btn btn-primary btn-sm"> <i class="fa fa-eye"></i> Ver</button>
																				<button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
																				<ul class="dropdown-menu pull-right">
																					<li><a href="pacientes/derivar/{{$paciente->id}}"  > <i class="fa fa-car"></i> Derivar</a></li>
																					<li><a href="pacientes/beneficios/{{$paciente->id}}"  > <i class="fa fa-hand-o-up"></i> Dar Beneficio</a></li>
																				</ul>
																			</div>-->

<!--<a title="Eliminar" href="/pacientes/eliminar/" onclick="return confirm('Esta seguro que lo desea eliminar?')"  class="btn btn-sm btn-danger"> <i class="fa fa-trash-o"></i></a>-->

							</td>
						</tr>
						@endforeach

                    </tbody>
                </table>
				<div class="text-center"><?php echo $pacientes->links(); ?></div> 
            </div>
        </div>
    </div>
</div>
@show

@section('js')
<script>
	$(function() {
		$(".tablesorter").tablesorter();
	});

//	function confirmar(){ 
//			confirmar=confirm("¿Desea eliminar el paciente?"); 
//		if (confirmar) 
//			// si pulsamos en aceptar
//			alert('si');
//		else 
//			// si pulsamos en cancelar
//			alert('no'); 
//	} 

</script>
@stop