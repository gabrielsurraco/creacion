@section('content')
<h3>Informaci&oacute;n del Colaborador</h3><hr/>
<div class="content">
    <div class="row">
        <div class="col-lg-5"> 
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped tablesorter">
                    <thead>
                        <tr>
                            <th colspan="2" class="text-center" style="background-color: #428bca; color: white;">Datos del Colaborador</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-right"><strong>Nombre</strong></td>
                            <td>{{ $colaborador->nombre }}</td>
                        </tr>
                        <tr>
                            <td class="text-right"><strong>DNI</strong></td>
                            <td>{{ $colaborador->dni }}</td>
                        </tr>
						<tr>
                            <td class="text-right"><strong>Fecha Nacimiento</strong></td>
                            <td>{{ $colaborador->fecha_nacimiento }}</td>
                        </tr>
                        <tr>
                            <td class="text-right"><strong>Fecha Ingreso</strong></td>
                            <td>{{ $colaborador->fecha_ingreso }}</td>
                        </tr>
                        <tr>
                            <td class="text-right"><strong>Profesi&oacute;n</strong></td>
                            <td>{{ $colaborador->profesion }}</td>
                        </tr>
                        <tr>
                            <td class="text-right"><strong>Domicilio</strong></td>
                            <td>{{ $colaborador->domicilio }}</td>
                        </tr>
                        <tr>
                            <td class="text-right"><strong>Localidad</strong></td>
                            <td>{{ $colaborador->localidad_id }}</td>
                        </tr>
                        <tr>
                            <td class="text-right"><strong>Tel&eacute;fono</strong></td>
                            <td>{{ $colaborador->tel_contacto }}</td>
                        </tr>
                        <tr>
                            <td class="text-right"><strong>Es Socio</strong></td>
                            <td><?php echo ($colaborador->es_socio == 1) ? "Si" : "No"; ?></td>
                        </tr>
                        <tr>
                            <td class="text-right"><strong>Es Voluntario</strong></td>
                            <td><?php echo ($colaborador->es_voluntario == 1) ? "Si" : "No"; ?></td>
                        </tr>
                        <tr>
                            <td class="text-right"><strong>Es Benefactor</strong></td>
                            <td><?php echo ($colaborador->es_benefactor == 1) ? "Si" : "No"; ?></td>
                        </tr>
                        <tr>
                            <td class="text-right"><strong>Email</strong></td>
                            <td>{{ $colaborador->email }}</td>
                        </tr>
                        <tr>
                            <td class="text-right"><strong>Lugar de Pago</strong></td>
                            <td>{{ $colaborador->lugar_de_pago }}</td>
                        </tr>
                        <tr>
                            <td class="text-right"><strong>Colabora en</strong></td>
                            <td>{{ $colaborador->colabora_en }}</td>
                        </tr>
                    </tbody>                    
                </table>
                
            </div>

        </div>
		
		<div class="col-lg-5"> 
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped tablesorter">
                    <thead>
                        <tr>
                            <th colspan="3" class="text-center" style="background-color: #428bca; color: white;">Cuotas</th>
                        </tr>
						@if(!empty($pagos[0]))
							<th>Fecha</th>
							<th>Monto</th>
							<th>Observaciones</th>
						@endif
                    </thead>
                    <tbody>
						@if(!empty($pagos[0]))
							@foreach($pagos as $pago)
								<tr>
									<td>{{ $pago->fecha_pago }}</td>
									<td>{{ $pago->monto }}</td>
									<td>{{ $pago->observaciones }}</td>
								</tr>
							@endforeach
						@else
							<tr>
								<td colspan="3" class="text-right text-center">No existen pagos</td>
							</tr>
						@endif
					</tbody>
				</table>
			</div>
		</div>
				
        
    </div>
	
	<div class="row">
		<div class="col-lg-8"> 
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped tablesorter">
                    <thead>
                        <tr>
                            <th colspan="4" class="text-center" style="background-color: #428bca; color: white;">Listado de Préstamos</th>
                        </tr>
						@if(!empty($prestamos[0]))
							<th style="text-align: center">Elemento</th>
							<th style="text-align: center">Cantidad</th>
							<th style="text-align: center">Fecha Préstamo</th>
							<th style="text-align: center;">Acción</th>
						@endif
                    </thead>
                    <tbody>
						@if(!empty($prestamos[0]))
							@foreach($prestamos as $prestamo)
								<tr>
									<td>{{ $prestamo->descripcion }}</td>
									<td>{{ $prestamo->cantidad }}</td>
									<td>{{ $prestamo->fecha_salida }}</td>
									<td>
										@if(Auth::user()->es_admin)
											<a href="/beneficios/devolucionprestamo/{{ $prestamo->id }}"  class="btn btn-sm btn-success"> <i class="fa fa-edit"></i> Registrar Devolución</a>
										@else
											<a href="#"  class="btn btn-sm btn-success disabled"> <i class="fa fa-edit"></i> Registrar Devolución</a>
										@endif
										
									</td>
								</tr>
							@endforeach
						@else
							<tr>
								<td colspan="4" class="text-right text-center">No posee préstamos</td>
							</tr>
						@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
    
    <div class="row">
        <div class="form-group col-lg-1">
        {{ HTML::link("colaboradores","Volver",array("class"=>"btn btn-default")) }}
        </div>
    </div>
</div>

@stop