@section('content')
<h3>Administraci&oacute;n de Pacientes</h3><hr/>    
<div class="content">
    <div class="panel panel-primary">
        <div class="panel-heading">Carga de Pacientes</div>
        <div class="panel-body">

            <div id="mensajedealerta" class="alert alert-danger alert-dismissable hidden">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>


			{{ Form::model($paciente, array('route' => array('pacientes.update', $paciente->id), 'method' => 'POST')) }}
			<div class="row">

				<div class="form-group col-md-3">
					{{ Form::label('nombre', 'Apellido y Nombre') }}
					{{ Form::text('nombre', empty($paciente->id) ? null : $paciente->nombre, array('placeholder' => 'Apellido y Nombre del Paciente', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
					<p class="alert-danger">{{$errors->first("nombre")}}</p>
				</div>

				<div class="form-group col-md-3">
					{{ Form::label('dni', 'Número de Documento') }}
					{{ Form::text('dni', empty($paciente->id) ? null : $paciente->dni, array('placeholder' => 'Número del paciente', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
					<p class="alert-danger">{{$errors->first("dni")}}</p>
				</div>

				<div class="form-group col-md-3">
					{{ Form::label('fecha_nacimiento', 'Fecha de Nacimiento') }}
					{{ Form::text('fecha_nacimiento', empty($paciente->id) ? null : $paciente->fecha_nacimiento, array('placeholder' => 'Fecha de Nacimiento', 'class' => 'form-control datepicker' , 'autocomplete' => 'off')) }}
					<p class="alert-danger">{{$errors->first("fecha_nacimiento")}}</p>
				</div>

			</div>

			<div class="row">
				<div class="form-group col-md-3">
                    {{ Form::label('diagnostico_id', 'Diagnóstico') }}
                    {{ Form::select("diagnostico_id", array('default' => 'Seleccionar...') + $lista_diagnosticos, empty($paciente->id) ? 'default' : $paciente->diagnostico_id, array("class"=>"form-control")) }}
                    <p class="alert-danger">{{$errors->first("diagnostico_id")}}</p>
                </div>
				<div class="form-group col-md-3">
					{{ Form::label('fecha_ingreso', 'Fecha de Diagnóstico') }}
					{{ Form::text('fecha_ingreso', empty($paciente->id) ? null : $paciente->fecha_ingreso, array('placeholder' => 'Fecha de Diagnóstico', 'class' => 'form-control datepicker' , 'autocomplete' => 'off')) }}
					<p class="alert-danger">{{$errors->first("fecha_ingreso")}}</p>
				</div>

				<div class="form-group col-md-3">
                    {{ Form::label('lugar_tratamiento_id', 'Lugar de Tratamiento') }}
                    {{ Form::select("lugar_tratamiento_id", array('default' => 'Seleccionar...') + $lista_institutos, empty($paciente->id) ? 'default' : $paciente->lugar_tratamiento_id, array("class"=>"form-control")) }}
                    <p class="alert-danger">{{$errors->first("lugar_tratamiento_id")}}</p>
                </div>

			</div>

			<div class="row">
				<div class="form-group col-md-3">
					{{ Form::label('trasladado', 'Trasladado a') }}
					{{ Form::text('trasladado', empty($paciente->id) ? null : $paciente->trasladado, array('placeholder' => 'Lugar de traslado', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
					<p class="alert-danger">{{$errors->first("trasladado")}}</p>
				</div>
				<div class="form-group col-md-3">
					{{Form::label('roles','Prioridad')}}
					<br>
					<label class="checkbox-inline">
						{{ Form::checkbox('prioridad_alta', true , false) }}
						Alta
					</label>

					<label class="checkbox-inline">
						{{ Form::checkbox('prioridad_media', true , false) }}
						Media
					</label>
				</div>
				<div class="form-group col-md-3">
					<label>
						{{ Form::checkbox('estado_paleativo', true , false) }}
						Estado Paleativo
					</label>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-3">
					<label>
						{{ Form::checkbox('fallecio', true , false) }}
						Falleció
					</label>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<h4>Datos del Hogar</h4>
					<hr/>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-3">
					{{ Form::label('nombre_padre', 'Nombre del Padre') }}
					{{ Form::text('nombre_padre', empty($paciente->id) ? null : $paciente->nombre_padre, array('placeholder' => 'Nombre del Padre', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
					<p class="alert-danger">{{$errors->first("nombre_padre")}}</p>
				</div>
				<div class="form-group col-md-3">
					{{ Form::label('dni_padre', 'Número de documento') }}
					{{ Form::text('dni_padre', empty($paciente->id) ? null : $paciente->dni_padre, array('placeholder' => 'Documento del Padre', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
					<p class="alert-danger">{{$errors->first("dni_padre")}}</p>
				</div>

				<div class="form-group col-md-3">
                    {{ Form::label('estudio_padre', 'Estudios del Padre') }}
                    {{ Form::select("estudio_padre",array('ninguno' => 'Ninguno', 'primaria' => 'Primaria', 'secundaria' => 'Secundaria', 'terciario' => 'Terciario', 'universitario' => 'Universitario'),$paciente->estudio_padre ,array("class"=>"form-control")) }}
                    <p class="alert-danger">{{$errors->first("estudio_padre")}}</p>
                </div>
				
				<div class="form-group col-md-3">
                    {{ Form::label('estudio_padre_estado', 'Estado de Estudio') }}
                    {{ Form::select("estudio_padre_estado",array('incompleto' => 'Incompleto', 'completo' => 'Completo'),$paciente->estudio_padre_estado ,array("class"=>"form-control")) }}
                    <p class="alert-danger">{{$errors->first("estudio_padre_estado")}}</p>
                </div>
			</div>
			<div class="row">
				<div class="form-group col-md-3">
					{{ Form::label('nombre_madre', 'Nombre de la Madre') }}
					{{ Form::text('nombre_madre', empty($paciente->id) ? null : $paciente->nombre_madre, array('placeholder' => 'Nombre de la Madre', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
					<p class="alert-danger">{{$errors->first("nombre_madre")}}</p>
				</div>
				<div class="form-group col-md-3">
					{{ Form::label('dni_madre', 'Número de documento') }}
					{{ Form::text('dni_madre', empty($paciente->id) ? null : $paciente->dni_madre, array('placeholder' => 'Documento de la Madre', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
					<p class="alert-danger">{{$errors->first("dni_madre")}}</p>
				</div>
				<div class="form-group col-md-3">
                    {{ Form::label('estudio_madre', 'Estudios de la Madre') }}
                    {{ Form::select("estudio_madre",array('ninguno' => 'Ninguno', 'primaria' => 'Primaria', 'secundaria' => 'Secundaria', 'terciario' => 'Terciario', 'universitario' => 'Universitario'),$paciente->estudio_madre ,array("class"=>"form-control")) }}
                    <p class="alert-danger">{{$errors->first("estudio_madre")}}</p>
                </div>
				<div class="form-group col-md-3">
                    {{ Form::label('estudio_madre_estado', 'Estado de Estudio') }}
                    {{ Form::select("estudio_madre_estado",array('incompleto' => 'Incompleto', 'completo' => 'Completo'),$paciente->estudio_madre_estado ,array("class"=>"form-control")) }}
                    <p class="alert-danger">{{$errors->first("estudio_madre_estado")}}</p>
                </div>
			</div>
			<div class="row">
				<div class="form-group col-md-3">
					{{ Form::label('nombre_tutor', 'Nombre del Tutor') }}
					{{ Form::text('nombre_tutor', empty($paciente->id) ? null : $paciente->nombre_tutor, array('placeholder' => 'Nombre del Tutor', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
					<p class="alert-danger">{{$errors->first("nombre_tutor")}}</p>
				</div>
				<div class="form-group col-md-3">
					{{ Form::label('dni_tutor', 'Número de documento') }}
					{{ Form::text('dni_tutor', empty($paciente->id) ? null : $paciente->dni_tutor, array('placeholder' => 'Documento del Tutor', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
					<p class="alert-danger">{{$errors->first("dni_tutor")}}</p>
				</div>
				<div class="form-group col-md-3">
                    {{ Form::label('estudio_tutor', 'Estudios del Tutor') }}
                    {{ Form::select("estudio_tutor",array('ninguno' => 'Ninguno', 'primaria' => 'Primaria', 'secundaria' => 'Secundaria', 'terciario' => 'Terciario', 'universitario' => 'Universitario'),$paciente->estudio_tutor ,array("class"=>"form-control")) }}
                    <p class="alert-danger">{{$errors->first("estudio_tutor")}}</p>
                </div>
				<div class="form-group col-md-3">
                    {{ Form::label('estudio_tutor_estado', 'Estado de Estudio') }}
                    {{ Form::select("estudio_tutor_estado",array('incompleto' => 'Incompleto', 'completo' => 'Completo'),$paciente->estudio_tutor_estado ,array("class"=>"form-control")) }}
                    <p class="alert-danger">{{$errors->first("estudio_tutor_estado")}}</p>
                </div>
			</div>

			<div class="row">
				<div class="table-responsive col-md-9">
                    <table id="tabla" class="table table-bordered table-hover tablesorter">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>DNI</th>
                                <th>Fecha Nacimiento</th>
								<th>Acci&oacute;n</th>
                            </tr>
                        </thead>
                        <tbody>
							<?php $i = 0; ?>
                            @foreach($paciente->hermanos as $hermano)
                            <tr>
                                {{ Form::hidden('hermanos[' . $i . '][paciente_id]', $hermano->paciente_id,array("id"=>"formhidden")) }}
                                <!--<td id="nombrehermano">{{ $hermano->nombre }}</td>-->
                                <td id="nombre">
                                    {{ Form::text('hermanos[' . $i . '][nombre]', $hermano->nombre, array('placeholder' => 'Nombre y Apellido', 'class' => 'form-control', 'autocomplete' => 'off')) }}
                                    <p class="alert-danger">
                                        {{$errors->first("hermanos['" . $i . "'][nombre]")}}
                                    </p>
                                </td>
                                <td id="dni">
                                    {{ Form::text('hermanos[' . $i . '][dni]', $hermano->dni, array('placeholder' => 'Nro. Documento', 'class' => 'form-control', 'autocomplete' => 'off')) }}
                                    <p class="alert-danger">
                                        {{$errors->first("hermanos['" . $i . "'][dni]")}}
                                    </p>
                                </td>
                                <td id="fecha_nacimiento">
									{{ Form::text('hermanos[' . $i . '][fecha_nacimiento]', empty($hermano->id) ? null : $hermano->fecha_nacimiento, array('placeholder' => 'Fecha de Nacimiento', 'class' => 'form-control datepicker' , 'autocomplete' => 'off')) }}
                                    <p class="alert-danger">
                                        {{$errors->first("hermanos[' ". $i ." '][fecha_nacimiento]")}}
                                    </p>
                                </td>
                                <td id="eliminarhermano">
                                    <div class="btn btn-default">
										<li class="fa fa-eraser"></li>Eliminar
									</div>          
                                </td>
								<?php $i++; ?>
                            </tr>
                            @endforeach

                            <tr style="display: none;">
								<td id="nombre">
                                    {{ Form::text('hermanos[{id}][nombre]', null, array('placeholder' => 'Nombre y Apellido', 'class' => 'form-control', 'autocomplete' => 'off')) }}
                                    <p class="alert-danger">
                                        {{$errors->first("hermanos[{id}][nombre]")}}
                                    </p>
                                </td>
                                <td id="dni">
                                    {{ Form::text('hermanos[{id}][dni]', null, array('placeholder' => 'Nro. Documento', 'class' => 'form-control', 'autocomplete' => 'off')) }}
                                    <p class="alert-danger">
                                        {{$errors->first("hermanos[{id}][dni]")}}
                                    </p>
                                </td>
                                <td id="fecha_nacimiento">
									{{ Form::text('hermanos[{id}][fecha_nacimiento]', null, array('placeholder' => 'Fecha de Nacimiento', 'class' => 'form-control datepicker' , 'autocomplete' => 'off')) }}
                                    <p class="alert-danger">
                                        {{$errors->first("hermanos[{id}][fecha_nacimiento]")}}
                                    </p>
                                </td>

                                <td id="eliminarhermano">
                                    <div  class="btn btn-default"><li class="fa fa-eraser"></li>Eliminar</div>          
                                </td>                                    
                            </tr>
                        </tbody>
                    </table>
                </div>
			</div>

			<div class="row">
				<div class="form-group col-md-3">

					<div id="nuevohermano"  class="col-md-2">
						<div class="btn btn-default">Agregar Hermano</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="form-group col-md-3">
                    {{ Form::label('ocupacion_vivienda', 'Ocupación de la Vivienda') }}
                    {{ Form::select("ocupacion_vivienda",array('propia' => 'Propia', 'terreno_fiscal' => 'Terreno Fiscal', 'laboral' => 'Laboral', 'prestada' => 'Prestada', 'temporal' => 'Temporal'), $paciente->ocupacion_vivienda ,array("class"=>"form-control")) }}
                    <p class="alert-danger">{{$errors->first("ocupacion_vivienda")}}</p>
                </div>
				<div class="form-group col-md-3">
                    {{ Form::label('medio_devida', 'Medio de vida') }}
                    {{ Form::select("medio_devida",array('urbano' => 'Urbano', 'rural' => 'Rural'),$paciente->medio_devida ,array("class"=>"form-control")) }}
                    <p class="alert-danger">{{$errors->first("medio_devida")}}</p>
                </div>
				<div class="form-group col-md-3">
					{{ Form::label('distancia_camino', 'Distancia de camino terrado (Km)') }}
					{{ Form::text('distancia_camino', empty($paciente->id) ? null : $paciente->distancia_camino, array('placeholder' => 'Distancia de camino terrado', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
				</div>

			</div>
			<div class="row">
				<div class="form-group col-md-3">
                    {{ Form::label('localidad_id', 'Localidad') }}
                    {{ Form::select("localidad_id", array('default' => 'Seleccionar...') + $lista_localidades, empty($paciente->id) ? 'default' : $paciente->localidad_id, array("class"=>"form-control")) }}
                    <p class="alert-danger">{{$errors->first("localidad_id")}}</p>
                </div>
				<div class="form-group col-md-3">
					{{ Form::label('direccion', 'Dirección') }}
					{{ Form::text('direccion', empty($paciente->id) ? null : $paciente->direccion, array('placeholder' => 'Dirección del Hogar', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
					<p class="alert-danger">{{$errors->first("direccion")}}</p>
				</div>
				<div class="form-group col-md-3">
					{{ Form::label('tel_contacto', 'Teléfono fijo') }}
					{{ Form::text('tel_contacto', empty($paciente->id) ? null : $paciente->tel_contacto, array('placeholder' => 'ej. 03764425896', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
					<p class="alert-danger">{{$errors->first("tel_contacto")}}</p>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-3">
					{{ Form::label('cel_contacto', 'Celular *(incluir 0 y 15)') }}
					{{ Form::text('cel_contacto', empty($paciente->id) ? null : $paciente->cel_contacto, array('placeholder' => 'ej. 0376154225566', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
					<p class="alert-danger">{{$errors->first("cel_contacto")}}</p>
				</div>
				<div class="form-group col-md-3">
					{{ Form::label('cel_contacto_2', 'Celular *(incluir 0 y 15)') }}
					{{ Form::text('cel_contacto_2', empty($paciente->id) ? null : $paciente->cel_contacto_2, array('placeholder' => 'ej. 0376154225588', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
					<p class="alert-danger">{{$errors->first("cel_contacto_2")}}</p>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-6">
					{{ Form::label('techo', 'Techo de') }}
					<div class="checkbox-inline">
						<label class="checkbox-inline">
							{{ Form::checkbox('techo_zinc', true , false) }}
							Zinc
						</label>
						<label class="checkbox-inline">
							{{ Form::checkbox('techo_carton', true , false) }}
							Cartón
						</label>
					</div>

					{{ Form::text('techo_otro', empty($paciente->id) ? null : $paciente->techo_otro, array('placeholder' => 'Otros (Techo)' ,'class' => 'form-control' , 'autocomplete' => 'off')) }}
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-6">
					{{ Form::label('paredes', 'Paredes de') }}
					<div class="checkbox-inline">
						<label class="checkbox-inline">
							{{ Form::checkbox('pared_madera', true , false) }}
							Madera
						</label>
						<label class="checkbox-inline">
							{{ Form::checkbox('pared_material', true , false) }}
							Material
						</label>
					</div>
					{{ Form::text('pared_otro', empty($paciente->id) ? null : $paciente->pared_otro, array('placeholder' => 'Otros (Pared)' ,'class' => 'form-control' , 'autocomplete' => 'off')) }}
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-6">
					{{ Form::label('pisos', 'Pisos de') }}
					<div class="checkbox-inline">
						<label class="checkbox-inline">
							{{ Form::checkbox('piso_ceramica', true , false) }}
							Cerámica
						</label>
						<label class="checkbox-inline">
							{{ Form::checkbox('piso_alisado', true , false) }}
							Alisado
						</label>
					</div>
					{{ Form::text('piso_otro', empty($paciente->id) ? null : $paciente->piso_otro, array('placeholder' => 'Otros (Piso)' ,'class' => 'form-control' , 'autocomplete' => 'off')) }}
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-6">
					{{ Form::label('agua', 'Suministro de agua potable') }}
					<div class="checkbox-inline">
						<label class="checkbox-inline">
							{{ Form::checkbox('agua_pozo_balde', true , false) }}
							Pozo de balde
						</label>
						<label class="checkbox-inline">
							{{ Form::checkbox('agua_canilla_publica', true , false) }}
							Canilla pública
						</label>
					</div>
					{{ Form::text('agua_otro', empty($paciente->id) ? null : $paciente->agua_otro, array('placeholder' => 'Otros (Agua)' ,'class' => 'form-control' , 'autocomplete' => 'off')) }}
				</div>
			</div>

			<div class="row">
				<div class="form-group col-md-6">
					{{ Form::label('sanitario', 'Sanitarios') }}
					<div class="checkbox-inline">
						<label class="checkbox-inline">
							{{ Form::checkbox('sanitario_interno', true , false) }}
							Interno
						</label>
						<label class="checkbox-inline">
							{{ Form::checkbox('sanitario_externo', true , false) }}
							Externo
						</label>
						<label class="checkbox-inline">
							{{ Form::checkbox('sanitario_completo', true , false) }}
							Completo
						</label>
					</div>
					{{ Form::text('sanitario_otro', empty($paciente->id) ? null : $paciente->sanitario_otro, array('placeholder' => 'Otros (Sanitario)' ,'class' => 'form-control' , 'autocomplete' => 'off')) }}
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-3">
					{{ Form::label('nro_habitantes_casa', 'Número de Habitantes') }}
					{{ Form::number('nro_habitantes_casa', empty($paciente->id) ? null : $paciente->nro_habitantes_casa, array('placeholder' => 'Nro. de habitantes en el hogar', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
				</div>
				<div class="form-group col-md-3">
					{{ Form::label('nro_habitaciones', 'Número de Habitaciones') }}
					{{ Form::number('nro_habitaciones', empty($paciente->id) ? null : $paciente->nro_habitaciones, array('placeholder' => 'Nro. de habitaciones', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-3">
					{{ Form::label('nro_personas_que_trabajan', 'Habitantes que trabajan') }}
					{{ Form::number('nro_personas_que_trabajan', empty($paciente->id) ? null : $paciente->nro_personas_que_trabajan, array('placeholder' => 'Nro. de habitantes que trabajan', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
				</div>
				<br/>
				<div class="form-group col-md-6">
					{{ Form::label('trabajo_tipo', 'Tipo') }}
					<div class="checkbox-inline">
						<label class="checkbox-inline">
							{{ Form::checkbox('trabajo_fijo', true , false) }}
							Fijo
						</label>
						<label class="checkbox-inline">
							{{ Form::checkbox('trabajo_temporario', true , false) }}
							Temporario
						</label>
						<label class="checkbox-inline">
							{{ Form::checkbox('trabajo_porsucuenta', true , false) }}
							Por su cuenta
						</label>
					</div>

				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-6">
					{{ Form::label('observaciones', 'Otras Observaciones') }}
					{{ Form::textarea('observaciones', empty($paciente->id) ? null : $paciente->observaciones, array('placeholder' => 'Observaciones...', 'class' => 'form-control' , 'autocomplete' => 'off', 'rows' => '2')) }}
				</div>
			</div>

			{{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}
			{{ HTML::link("pacientes","Cancelar",array("class"=>"btn btn-default")) }}

			{{ Form::close() }}
        </div>
    </div>

</div>
@show

@section('js')
<script>
	$(function() {

		var templateHermano = "<tr style=\"display:none;\" >" + $('#tabla tbody tr:last-child').html() + "</tr>",
				cantidadHermanos = <?php echo $i; ?>;

		$('#tabla tbody tr:last-child').remove();

		$("#nuevohermano").on('click', function() {
			nuevoRegistro = templateHermano;
			re = new RegExp('{id}', 'g');
			nuevoRegistro = nuevoRegistro.replace(re, cantidadHermanos);
			$('#tabla > tbody:last').append(nuevoRegistro);
			$('#tabla tbody tr:last').fadeIn();
			cantidadHermanos++;
		});

		$('body').on('focus', ".datepicker", function() {
			$(this).datepicker({
              format: 'dd/mm/yyyy'
            });
		});
		
		// Selecciona la fila y la elimina 
		$(document).on("click", "#eliminarhermano", function() {
			var parent = $(this).parents().get(0);
			$(parent).fadeOut(200, function() {
				$(this).remove();
			});
		});

	});

</script>
@show
