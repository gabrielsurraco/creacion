@section('content')

<h3>Derivación del Paciente {{$paciente->nombre}} - DNI: {{$paciente->dni}}</h3><hr/>

<div class="content">
	{{ Form::open(array('method' => 'POST'), array('role' => 'form')) }}
		{{ Form::hidden('id', $id) }}
	<div class="row">
		<div class="form-group col-md-2">
			{{ Form::label('fecha', 'Fecha') }}
			{{ Form::text('fecha', null, array('placeholder' => 'Ingrese Fecha', 'class' => 'form-control datepicker' , 'autocomplete' => 'off')) }}
			<p class="alert-danger">{{$errors->first("fecha")}}</p>
		</div>
		<div class="form-group col-md-3">
			{{ Form::label('lugar', 'Lugar a Derivar') }}
			{{ Form::text('lugar', null, array('placeholder' => 'Introduzca el Lugar de Derivación', 'class' => 'form-control')) }}
			<p class="alert-danger">{{$errors->first("lugar")}}</p>
		</div>
	</div>
	@if(Auth::user()->es_admin == 1)
	<div class="row">
				<div class="form-group col-md-4">

					{{Form::label('referencia','Referencias')}}
					<div class="radio">
						<label>
							{{Form::radio('referencia', "envio" , true)}}
							Env&iacute;o (Env&iacute;o)
						</label>
					</div>
					<div class="radio">
						<label>
							{{Form::radio('referencia', "recibo" , false)}}
							Recibo (Recibo)
						</label>
					</div>
				</div>
		</div>
	@endif
        
            {{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}
            {{ HTML::link("pacientes","Cancelar",array("class"=>"btn btn-default")) }}
	{{ Form::close() }}
	<br>
</div>
@stop