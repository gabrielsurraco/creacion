@section('content')

<h3>Administraci&oacute;n de Usuarios</h3><hr/>

<div class="content">
	<div class="row">
		<div class="form-group col-lg-3">
			@if(Auth::user()->es_admin)
                {{HTML::link("usuarios/editar","Crear Usuario",array("class"=>"btn btn-primary"))}}
            @else
                {{HTML::link("#","Crear Usuario",array("class"=>"btn btn-primary disabled"))}}
            @endif
		</div>
	</div>
    <div class="row">
        <div class="col-lg-8">
            
            <div class="table-responsive">
                <table class="table table-hover table-striped tablesorter">
                    <thead>
                        <tr>
                            <th>Nombre <i class="fa fa-sort"></i></th>
                            <th>Email <i class="fa fa-sort"></i></th>
                            
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach($usuarios as $usuario)
                        <tr>
                                    @if(Auth::user()->id == $usuario->id and Auth::user()->es_admin != 1)
                                        <td>{{ $usuario->nombre }}</td>
                                        <td>{{ $usuario->email }}</td>
                                        <td>
                                            <a href="usuarios/editar/{{$usuario->id}}"  class="btn btn-sm btn-primary"><i class="fa fa-edit"></i> Editar</a>
                                            <a href="#"  class="btn btn-sm btn-default disabled"><i class="fa fa-trash-o"></i> Eliminar</a>
                                        </td>
                                    @endif
                                
                                    @if(Auth::user()->es_admin == 1)
                                        <td>{{ $usuario->nombre }}</td>
                                        <td>{{ $usuario->email }}</td>
                                        <td>
                                            <a href="usuarios/editar/{{$usuario->id}}"  class="btn btn-sm btn-primary"><i class="fa fa-edit"></i> Editar</a>
                                            <a href="usuarios/eliminar/{{$usuario->id}}"  class="btn btn-sm btn-default"><i class="fa fa-trash-o"></i> Eliminar</a>
                                        </td>
                                    @endif

                        </tr>
                        @endforeach
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@show

@section('js')
<script>
	$(function(){
	  $(".tablesorter").tablesorter();
	});
</script>
@stop