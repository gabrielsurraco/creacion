<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDonacionesCompras extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('donaciones_compras', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('paciente_id');
			$table->string("descripcion",100);
			$table->integer('cantidad');
			$table->string("montototal",15);
			$table->date('fecha');
			$table->integer('procedencia');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists("donaciones_compras");
		
	}

}
