<?php


class Juguete extends Eloquent{
	// <editor-fold desc="Configuración" defaultstate="collapsed">

	protected $guarded = array("id", "created_at", "updated_at");

	protected $table = 'juguetes_asignados';
	
	public function paciente()
    {
        return $this->belongsTo('Paciente', 'paciente_id');
    }
	
	public function getFechaOtorgamientoAttribute() {
		if(!isset($this->attributes['fecha_otorgamiento']))
			return "";
		return DateTime::createFromFormat('Y-m-d', $this->attributes['fecha_otorgamiento'])->format('d/m/Y');
	}
	
	public function setFechaOtorgamientoAttribute($value) {
		if(empty($value))
			$this->attributes['fecha_otorgamiento'] = date("Y-m-d");
		elseif ( preg_match("/^[0-9]{2}\\/[0-9]{2}\\/[0-9]{4}$/", $value ) )
			$this->attributes['fecha_otorgamiento'] = DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
	}

}
