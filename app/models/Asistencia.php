<?php

use Carbon\Carbon;

class Asistencia extends Eloquent{
	protected $guarded = array("id", "created_at", "updated_at");
	protected $table = 'asistencias';
	
	
	public function paciente()
    {
        return $this->belongsTo('Paciente', 'paciente_id');
    }
	
	public function getFechaInicioAttribute() {
		if(!isset($this->attributes['fecha_inicio']))
			return "";
		return DateTime::createFromFormat('Y-m-d', $this->attributes['fecha_inicio'])->format('d/m/Y');
//		return '15/10/2014';
	}
	
	public function setFechaInicioAttribute($value) {
		if(empty($value))
			$this->attributes['fecha_inicio'] = date("Y-m-d");
		elseif ( preg_match("/^[0-9]{2}\\/[0-9]{2}\\/[0-9]{4}$/", $value ) )
			$this->attributes['fecha_inicio'] = DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
	}
	
	public function getFechaCierreAttribute() {
		if(!isset($this->attributes['fecha_cierre']))
			return "";
		return DateTime::createFromFormat('Y-m-d', $this->attributes['fecha_cierre'])->format('d/m/Y');
//		return '15/10/2014';
	}
	
	public function setFechaCierreAttribute($value) {
		if(empty($value))
			$this->attributes['fecha_cierre'] = date("Y-m-d");
		elseif ( preg_match("/^[0-9]{2}\\/[0-9]{2}\\/[0-9]{4}$/", $value ) )
			$this->attributes['fecha_cierre'] = DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
	}



}
