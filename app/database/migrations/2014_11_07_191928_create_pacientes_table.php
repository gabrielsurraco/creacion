<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacientesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pacientes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nombre', 60);
			$table->date('fecha_ingreso');
			$table->date('fecha_nacimiento');
			$table->string('dni', 10);
			$table->string('nombre_padre', 60)->nullable();
			$table->string('dni_padre', 10)->nullable();
			$table->enum('estudio_padre', array('ninguno', 'primaria', 'secundaria', 'terciario', 'universitario'));
			$table->enum('estudio_padre_estado', array('incompleto', 'completo'));
			$table->string('nombre_madre', 60)->nullable();
			$table->string('dni_madre', 10)->nullable();
			$table->enum('estudio_madre', array('ninguno', 'primaria', 'secundaria', 'terciario', 'universitario'));
			$table->enum('estudio_madre_estado', array('incompleto', 'completo'));
			$table->string('nombre_tutor', 60)->nullable();
			$table->string('dni_tutor', 10)->nullable();
			$table->enum('estudio_tutor', array('ninguno', 'primaria', 'secundaria', 'terciario', 'universitario'));
			$table->enum('estudio_tutor_estado', array('incompleto', 'completo'));
//			$table->string('hermanos', 400)->nullable();
			$table->enum('ocupacion_vivienda', array('propia', 'terreno_fiscal', 'laboral','prestada','temporal'));
			$table->string('direccion', 100);
			$table->string('tel_contacto', 50);
			$table->string('cel_contacto', 50);
			$table->string('cel_contacto_2', 50);
			$table->enum('medio_devida', array('urbano', 'rural'));
			$table->string('distancia_camino',50)->nullable();
			$table->integer('diagnostico_id')->nullable();
			$table->integer('lugar_tratamiento_id')->nullable();
			$table->integer('localidad_id')->nullable();
			$table->boolean('techo_zinc')->default(false);
			$table->boolean('techo_carton')->default(false);
			$table->string('techo_otro', 100)->nullable();
			$table->boolean('pared_madera')->default(false);
			$table->boolean('pared_material')->default(false);
			$table->string('pared_otro', 100)->nullable();
			$table->boolean('piso_ceramica')->default(false);
			$table->boolean('piso_alisado')->default(false);
			$table->string('piso_otro', 100)->nullable();
			$table->boolean('agua_pozo_balde')->default(false);
			$table->boolean('agua_canilla_publica')->default(false);
			$table->string('agua_otro', 100)->nullable();
			$table->boolean('sanitario_interno')->default(false);
			$table->boolean('sanitario_externo')->default(false);
			$table->boolean('sanitario_completo')->default(false);
			$table->string('sanitario_otro', 100)->nullable();
			$table->integer('nro_habitantes_casa');
			$table->integer('nro_habitaciones');
			$table->integer('nro_personas_que_trabajan');
			$table->boolean('trabajo_fijo')->default(false);
			$table->boolean('trabajo_temporario')->default(false);
			$table->boolean('trabajo_porsucuenta')->default(false);
			$table->string('trasladado', 100)->nullable();
			$table->boolean('estado_paleativo')->default(false);
			$table->boolean('fallecio')->default(false);
			$table->boolean('prioridad_alta')->default(false);
			$table->boolean('prioridad_media')->default(false);
			$table->string('observaciones', 500)->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists("pacientes");
	}

}
