<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRoperosAsignados extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('roperos_asignados', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('paciente_id');
			$table->date('fecha_otorgamiento');
			$table->integer('cantidad');
			$table->enum('tipo', array('nuevo','usado'));
			$table->string("observaciones",400);

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists("roperos_asignados");
	}

}
