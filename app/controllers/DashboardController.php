<?php

use Carbon\Carbon;

class DashboardController extends BaseController {

	// <editor-fold desc="Procesamiento" defaultstate="collapsed">

	public function index() {
		return View::make('layouts.master');
	}

	public function getPrincipal() {
		
		$usuario = Auth::user();

		if (!($usuario->es_admin ) && !($usuario->es_default )) {
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}
		
		$colaboradores = Colaborador::where(DB::raw('DAY(fecha_nacimiento)'), '=', Carbon::today()->day)->where(DB::raw('MONTH(fecha_nacimiento)'), '=', Carbon::today()->month)->get();
		$colaboradores_con_pagos = Colaborador::with(['pagos' => function($query) {
				$query->where('fecha_pago',">", Carbon::today()->subMonths(6)->toDateString());
		}])->where('es_socio',1)->get();
		
//		return $colaboradores_con_pagos;die;
		$this->layout->content = View::make("dashboard.principal")->with(compact("colaboradores","colaboradores_con_pagos"));
	}

}
