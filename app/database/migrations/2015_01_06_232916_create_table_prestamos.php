<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePrestamos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('prestamos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('paciente_id')->nullable();
			$table->integer('colaborador_id')->nullable();
			$table->string("descripcion",200);
			$table->string("descripcion_devolucion",200)->nullable();
			$table->integer('cantidad')->nullable();
			$table->date('fecha_salida');
			$table->date('fecha_devolucion')->nullable();
			
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists("prestamos");
	}

}
