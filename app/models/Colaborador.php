<?php

use Carbon\Carbon;

class Colaborador extends Eloquent{
	protected $guarded = array("id", "created_at", "updated_at");
	protected $table = 'colaboradores';
	
	protected $fillable = array(
		'nombre',
		'dni',
		'fecha_nacimiento',
		'profesion',
		'domicilio',
		'localidad_id',
		'tel_contacto',
		'email',
		'lugar_de_pago',
		'fecha_ingreso',
		'es_socio',
		'es_voluntario',
		'es_benefactor',
		'colabora_en'
	);
	
	public $autoHydrateEntityFromInput = true;	// hydrates on new entries' validation

	public $forceEntityHydrationFromInput = true; // hydrates whenever validation is called
	
	
	public function getFechaNacimientoAttribute() {
		if(!isset($this->attributes['fecha_nacimiento']))
			return "";
		return DateTime::createFromFormat('Y-m-d', $this->attributes['fecha_nacimiento'])->format('d/m/Y');
	}
	
	public function setFechaNacimientoAttribute($value) {
		if(empty($value))
			$this->attributes['fecha_nacimiento'] = date("Y-m-d");
		elseif ( preg_match("/^[0-9]{2}\\/[0-9]{2}\\/[0-9]{4}$/", $value ) )
			$this->attributes['fecha_nacimiento'] = DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
	}
	
	public function getFechaIngresoAttribute() {
		if(!isset($this->attributes['fecha_ingreso']))
			return "";
		return DateTime::createFromFormat('Y-m-d', $this->attributes['fecha_ingreso'])->format('d/m/Y');
	}
	
	public function setFechaIngresoAttribute($value) {
		if(empty($value))
			$this->attributes['fecha_ingreso'] = date("Y-m-d");
		elseif ( preg_match("/^[0-9]{2}\\/[0-9]{2}\\/[0-9]{4}$/", $value ) )
			$this->attributes['fecha_ingreso'] = DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
	}
	
	public function pagos() {
		return $this->hasMany('Pago', 'colaborador_id');
	}
	
	public function prestamos() {
		return $this->hasMany('Prestamo', 'colaborador_id');
	}



}
