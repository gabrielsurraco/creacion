<?php

use Carbon\Carbon;

class Derivacion extends Eloquent{
	protected $guarded = array("id", "created_at", "updated_at");
	protected $table = 'derivaciones';
	
	
	
	public function getFechaAttribute() {
		if(!isset($this->attributes['fecha']))
			return "";
		return DateTime::createFromFormat('Y-m-d', $this->attributes['fecha'])->format('d/m/Y');
	}
	
	public function setFechaAttribute($value) {
		if(empty($value))
			$this->attributes['fecha'] = date("Y-m-d");
		elseif ( preg_match("/^[0-9]{2}\\/[0-9]{2}\\/[0-9]{4}$/", $value ) )
			$this->attributes['fecha'] = DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
	}
	
	public function paciente()
    {
        return $this->belongsTo('Paciente', 'paciente_id');
    }



}
