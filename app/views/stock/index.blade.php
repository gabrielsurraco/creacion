@section('content')

<h3>Secci&oacute;n Stock</h3><hr/>

<div class="content">
	<div class="row">
		<div class="form-group col-lg-3">
			@if(Auth::user()->es_admin)
				{{HTML::link("stock/actualizar","Actualizar Stock",array("class"=>"btn btn-primary"))}}
			@else
				{{HTML::link("#","Actualizar Stock",array("class"=>"btn btn-primary disabled"))}}
			@endif
			
		</div>
	</div>
    <div class="row">
        <div class="col-lg-8">
			<div class="table-responsive">
                <table class="table table-hover table-striped tablesorter">
                    <thead>
                        <tr>
                            <th>Art&iacute;culo</th>
                            <th style="text-align: center;">Cantidad en Stock</th>
                            <th>Actualizado por</th>
                            <th>Fecha de Actualización</th>
                            
                        </tr>
                    </thead>
                    <tbody>
						@foreach($stock as $s)
                                <tr >     
                                    <td>{{ ucfirst($s->tipo) }}</td>
                                    <td style="text-align: center;">{{ $s->cantidad }}</td>
                                    <td>{{ $s->usuario_actualizo }}</td>
                                    <td>{{ $s->updated_at }}</td>
                                </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@show

@section('js')
<script>
	$(function(){
	  $(".tablesorter").tablesorter();
	});
</script>
@stop