@section('content')

<h3>Asignaci&oacute;n de Alimento (Con control de Stock)</h3><hr/>

<div class="content">
    {{ Form::open(array('action' => 'BeneficiosController@postAlimento', 'method' => 'POST')) }}
	{{ Form::hidden('id_paciente',$id) }}
	<div class="row">
		<div class="form-group col-md-3">
			{{ Form::label('fecha_otorgamiento', 'Fecha de Asignación') }}
			{{ Form::text('fecha_otorgamiento', null, array('placeholder' => 'Fecha de Asignación', 'class' => 'form-control datepicker' , 'autocomplete' => 'off')) }}
			<p class="alert-danger">{{$errors->first("fecha_otorgamiento")}}</p>
		</div>
		<div class="form-group col-md-3">
			{{ Form::label('cantidad', 'Cantidad Alimento') }}
			{{ Form::number('cantidad', null, array('placeholder' => 'Cantidad...', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
			<p class="alert-danger">{{$errors->first("cantidad")}}</p>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-6">
			{{ Form::label('observaciones', 'Descripción') }}
			{{ Form::textarea('observaciones', null, array('placeholder' => 'Descripción...', 'class' => 'form-control' , 'autocomplete' => 'off', 'rows' => '2')) }}
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-3">
			{{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}
			<?php echo HTML::link('pacientes/beneficios/'.$id.'','Cancelar',['class' => 'btn btn-default']) ?>
		</div>
	</div>
	{{ Form::close() }}
</div>
@show

@section('js')
<script>
	$(function(){
	  $(".tablesorter").tablesorter();
	});
</script>
@stop