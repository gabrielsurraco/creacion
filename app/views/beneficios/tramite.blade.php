@section('content')

<h3>Registro de Trámites</h3><hr/>

<div class="content">
    {{ Form::open(array('action' => 'BeneficiosController@postTramite', 'method' => 'POST')) }}
	{{ Form::hidden('id_paciente',$id) }}
	<div class="row">
		<div class="form-group col-md-3">
			{{ Form::label('fecha', 'Fecha del Trámite') }}
			{{ Form::text('fecha', null, array('placeholder' => 'dd/mm/aaaa', 'class' => ' form-control datepicker' , 'autocomplete' => 'off')) }}
			<p class="alert-danger">{{$errors->first("fecha")}}</p>
		</div>
		<div class="form-group col-md-3">
			{{ Form::label('monto', 'Monto (Opcional)') }}
			{{ Form::text('monto', null, array('placeholder' => '$ 0.00', 'class' => ' form-control' , 'autocomplete' => 'off')) }}
			<p class="alert-danger">{{$errors->first("monto")}}</p>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-6">
			{{ Form::label('descripcion', 'Descripción') }}
			{{ Form::textarea('descripcion', null, array('placeholder' => 'Descripción del trámite...', 'class' => 'form-control' , 'autocomplete' => 'off', 'rows' => '2')) }}
			<p class="alert-danger">{{$errors->first("descripcion")}}</p>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-3">
			{{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}
			<?php echo HTML::link('pacientes/beneficios/'.$id.'','Cancelar',['class' => 'btn btn-default']) ?>
		</div>
	</div>
	{{ Form::close() }}
</div>
@show

@section('js')
<script>
	$(function(){
	  $(".tablesorter").tablesorter();
	});
</script>
@stop