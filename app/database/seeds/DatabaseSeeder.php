<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		// Usuarios
		$this->call('UsuariosTableSeeder');
        $this->command->info('Usuarios creados');
		$this->command->info('Administrador - email:gabriel_surraco@hotmail.com clave:admin');
		
		// Diagnosticos
		$this->call('DiagnosticosTableSeeder');
        $this->command->info('Diagnosticos creados');
		
		// Institutos
		$this->call('InstitutosTableSeeder');
        $this->command->info('Institutos creados');
		
		//Localidades
		$this->call('LocalidadesTableSeeder');
        $this->command->info('Localidades creadas');
		
		//Stock
		$this->call('StockTableSeeder');
        $this->command->info('Stock creado');

		// $this->call('UserTableSeeder');
	}

}
