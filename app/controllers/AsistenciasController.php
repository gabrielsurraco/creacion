<?php

use Carbon\Carbon;
//use Maatwebsite\Excel\Excel;

class AsistenciasController extends BaseController {
	
//	public function generar_informe($filtros) {
//		if (!empty($filtros['fecha_desde']) && !empty($filtros['fecha_hasta'])) {
//			$query = Asistencia::select("fecha_asistencia","nro_chicos","nro_adultos","observaciones")->where('fecha_asistencia', ">=", DateTime::createFromFormat('d/m/Y', $filtros['fecha_desde'])->format('Y-m-d'));
//			$query->where('fecha_asistencia', "<=", DateTime::createFromFormat('d/m/Y', $filtros['fecha_hasta'])->format('Y-m-d'));
//			$query->orderBy("fecha_asistencia", "desc");
//		} else {
//			$query = Asistencia::select("fecha_asistencia","nro_chicos","nro_adultos","observaciones")->orderBy("fecha_asistencia", "desc");
//		}
//
//		$asistencias = $query->get();
//		
//		Excel::create('Asistencias', function($excel) use($asistencias) 
//		{
//			$excel->sheet('Sheetname', function($sheet) use($asistencias) {
//				
//				$sheet->fromArray($asistencias);
//
//			});
//			
//		})->export('xls');
//	}

	public function postIndex() {
		return $this->getIndex();
	}

	public function getIndex($imprimir = null) {
		$usuario = Auth::user();

		if (!($usuario->es_admin) && !($usuario->es_default)) {
			return Redirect::to('pacientes')->with('error', 'Acceso no permitido');
		}
		
		
		if(isset($imprimir)){
			if (Request::isMethod('post')) 
				$filtros = Input::all();
			else
				$filtros = Session::get('asistencias.index.filtros', array());
			
			$this->generar_informe($filtros);
		}
		
		
		// Recupero filtros
		if (Request::isMethod('post')) {
			$filtros = Input::all();
		} else {
			
			if (Input::has('page')) {
				$filtros = Session::get('asistencias.index.filtros', array());
			} else {
				$filtros = array();
				Session::forget('asistencias.index.filtros');
			}
			
		}

		if (!empty($filtros['fecha_desde']) && !empty($filtros['fecha_hasta'])) {
			$query = Asistencia::where('fecha_asistencia', ">=", DateTime::createFromFormat('d/m/Y', $filtros['fecha_desde'])->format('Y-m-d'));
			$query->where('fecha_asistencia', "<=", DateTime::createFromFormat('d/m/Y', $filtros['fecha_hasta'])->format('Y-m-d'));
			$query->orderBy("fecha_asistencia", "desc");
		} else {
			$query = Asistencia::orderBy("fecha_asistencia", "desc");
		}

		$asistencias = $query->paginate(5);
		
		// Guardo filtro en sesion
		if (!Input::has('page') && count($filtros) > 0) {
			Session::put('asistencias.index.filtros', $filtros);
		}

		$this->layout->content = View::make('asistencias/index', compact("usuario", "asistencias"));
	}
	
	public function getCrear($id = null){
		$usuario = Auth::user();
		
		if (!($usuario->es_admin)) {
			return Redirect::to('asistencias')->with('error', 'Acceso no permitido');
		}
		
		$this->layout->content = View::make('asistencias/registro')->with(compact("id"));
		
	}
	
	public function postCrear(){
		$usuario = Auth::user();
		
		if (!($usuario->es_admin)) {
			return Redirect::to('asistencias')->with('error', 'Acceso no permitido');
		}
		
		$inputs = Input::All();
		
		$reglas = array(
			'fecha_inicio' => 'required',
			'fecha_cierre' => 'required',
			'nro_asistencias' => 'required|regex:/^\d*\.?\d*$/'
		);

		$mensajes = array(
			'required' => 'Debe completar el campo!',
			'regex' => 'No puede ser negativo'
		);

		$validar = Validator::make($inputs, $reglas, $mensajes);
		
		if ($validar->fails()) {
			return Redirect::back()->withErrors($validar)->withInput();
		} else {
			$asistencia = new Asistencia();
			
			$asistencia->paciente_id = $inputs['paciente_id'];
			$asistencia->fecha_inicio = $inputs['fecha_inicio'];
			$asistencia->fecha_cierre = $inputs['fecha_cierre'];
			$asistencia->nro_asistencias = $inputs['nro_asistencias'];
			$asistencia->observaciones = $inputs['observaciones'];

			$asistencia->save();
			
			Session::flash('success', 'El registro ha sido guardado exitosamente!');
			return Redirect::to('pacientes/info/'.strip_tags($inputs['paciente_id']));
		}
		
	}

	public function getEditar($id = null) {
		
		$usuario = Auth::user();
		$view = View::make('asistencias/editar');
		// Validaciones
		if (!($usuario->es_admin)) {
			return Redirect::to('asistencias')->with('error', 'Acceso no permitido');
		}
		
		
//		if (empty($id)) {
//			
//			$view->with(array(
//				"accion" => "add"
//				, "id" => ""
//			));
//			
//		} else {
			$datos = Asistencia::findOrFail($id);
//			return $datos;
//			$accion = "update";
			$view->with(compact("datos"));
//		}
		

		$this->layout->content = $view;
	}

	// Editar un registro
	public function postEditar($id = null) {
		$inputs = Input::All();

		$usuario = Auth::user();
		// Validaciones
		if (!($usuario->es_admin)) {
			return Redirect::to('asistencias')->with('error', 'Acceso no permitido');
		}

		$reglas = array(
			'fecha_inicio' => 'required',
			'fecha_cierre' => 'required',
			'nro_asistencias' => 'required|regex:/^\d*\.?\d*$/'
		);

		$mensajes = array(
			'required' => 'Debe completar el campo!',
			'regex' => 'No puede ser negativo'
		);

		$validar = Validator::make($inputs, $reglas, $mensajes);

		if ($validar->fails()) {
			return Redirect::back()->withErrors($validar)->withInput();
		} else {
//			if (empty($id))
//				$asistencia = new Asistencia();
//			else
			$asistencia = Asistencia::findOrFail($inputs['asistencia_id']);
			$aux_paciente_id = $asistencia->paciente_id;
			$asistencia->fecha_inicio = $inputs['fecha_inicio'];
			$asistencia->fecha_cierre = $inputs['fecha_cierre'];
			$asistencia->nro_asistencias = $inputs['nro_asistencias'];
			$asistencia->observaciones = $inputs['observaciones'];

			$asistencia->save();
			
			Session::flash('success', 'El registro ha sido guardado exitosamente!');
			return Redirect::to('pacientes/info/'.$aux_paciente_id);
		}
	}

}
