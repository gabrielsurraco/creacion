<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use Carbon\Carbon;

class Paciente extends Eloquent implements UserInterface, RemindableInterface{
	// <editor-fold desc="Configuración" defaultstate="collapsed">

	protected $guarded = array("id", "created_at", "updated_at");

	protected $table = 'pacientes';
	
	protected $fillable = array(
		'nombre', 
		'fecha_ingreso', 
		'fecha_nacimiento', 
		'dni', 
		'nombre_padre', 
		'dni_padre', 
		'estudio_padre',
		'estudio_padre_estado', 
		'nombre_madre', 
		'dni_madre', 
		'estudio_madre',
		'estudio_madre_estado', 
		'nombre_tutor', 
		'dni_tutor', 
		'estudio_tutor',
		'estudio_tutor_estado', 
		'ocupacion_vivienda', 
		'direccion', 
		'tel_contacto', 
		'cel_contacto', 
		'cel_contacto_2', 
		'medio_devida', 
		'distancia_camino', 
		'diagnostico_id', 
		'lugar_tratamiento_id', 
		'localidad_id', 
		'techo_zinc', 
		'techo_carton', 
		'techo_otro', 
		'pared_madera', 
		'pared_material', 
		'pared_otro', 
		'piso_ceramica', 
		'piso_alisado', 
		'piso_otro', 
		'agua_pozo_balde', 
		'agua_canilla_publica',
		'agua_otro', 
		'sanitario_interno', 
		'sanitario_externo', 
		'sanitario_completo', 
		'sanitario_otro', 
		'nro_habitantes_casa', 
		'nro_habitaciones', 
		'nro_personas_que_trabajan', 
		'trabajo_fijo', 
		'trabajo_temporario', 
		'trabajo_porsucuenta', 
		'trasladado', 
		'estado_paleativo', 
		'prioridad_alta',
		'fallecio', 
		'prioridad_media', 
		'observaciones'
	);
	
	public $autoHydrateEntityFromInput = true;	// hydrates on new entries' validation

	public $forceEntityHydrationFromInput = true; // hydrates whenever validation is called
	
//	public static $rules = array(
//		"nombre" => 'required'
//			, "dni" => 'required|integer'
//			, 'fecha_nacimiento'=>'required'
//			, 'fecha_ingreso'=>'required'
//	);
//
//	public static $customMessages = array(
//		'required' => 'El campo es requerido',
//		'integer'  => 'Debe ser un numero entero',
//		'min'      => 'Debe ingresar un numero válido',
//		'numeric' => 'Debe ingresar un número'
//	);
	
//	public static function activarValidacionesAdmin(){
//		$rules = array(
//			"nombre" => 'required'
//			, "dni" => 'required|integer'
//			, 'fecha_nacimiento'=>'required'
//			, 'fecha_ingreso'=>'required'
//		);
//		
//		foreach($rules as $field => $rule){
//			if(!isset(self::$rules[$field]))
//				self::$rules[$field] = $rule;
//		}
//	}
	// </editor-fold>

	public function hermanos() {
		return $this->hasMany('Hermano', 'paciente_id');
	}
	
	public function alimentos_asignados() {
		return $this->hasMany('Alimento', 'paciente_id');
	}
	
	public function roperos_asignados() {
		return $this->hasMany('Ropero', 'paciente_id');
	}
	
	public function juguetes_asignados() {
		return $this->hasMany('Juguete', 'paciente_id');
	}
	
	public function derivaciones() {
		return $this->hasMany('Derivacion', 'paciente_id');
	}
	
	public function asistencias() {
		return $this->hasMany('Asistencia', 'paciente_id');
	}
	
	public function donacionesycompras() {
		return $this->hasMany('Donacionycompra', 'paciente_id');
	}
	
	public function pasajes() {
		return $this->hasMany('Pasaje', 'paciente_id');
	}
	
	public function tramites() {
		return $this->hasMany('Tramite', 'paciente_id');
	}
	
	public function prestamos() {
		return $this->hasMany('Prestamo', 'paciente_id');
	}
	

	// <editor-fold desc="Auth" defaultstate="collapsed">
	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	public function getRememberToken()
	{
		return $this->remember_token;
	}

	public function setRememberToken($value)
	{
		$this->remember_token = $value;
	}

	public function getRememberTokenName()
	{
		return 'remember_token';
	}
	
	public function getFechaIngresoAttribute() {
		if(!isset($this->attributes['fecha_ingreso']))
			return "";
		
		if( preg_match("/^[0-9]{2}\\/[0-9]{2}\\/[0-9]{4}$/", $this->attributes['fecha_ingreso'] )){
			return DateTime::createFromFormat('Y-m-d', $this->attributes['fecha_ingreso'])->format('d/m/Y');
		}else{
			return "";
		}
//		return '15/10/2014';
	}
	
	public function setFechaIngresoAttribute($value) {
		if(empty($value))
			$this->attributes['fecha_ingreso'] = date("Y-m-d");
		elseif ( preg_match("/^[0-9]{2}\\/[0-9]{2}\\/[0-9]{4}$/", $value ) )
			$this->attributes['fecha_ingreso'] = DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
	}
	
	public function getFechaNacimientoAttribute() {
		if(!isset($this->attributes['fecha_nacimiento']))
			return "";
		return DateTime::createFromFormat('Y-m-d', $this->attributes['fecha_nacimiento'])->format('d/m/Y');
//		return '15/10/2014';
	}
	
	public function setFechaNacimientoAttribute($value) {
		if(empty($value))
			$this->attributes['fecha_nacimiento'] = date("Y-m-d");
		elseif ( preg_match("/^[0-9]{2}\\/[0-9]{2}\\/[0-9]{4}$/", $value ) )
			$this->attributes['fecha_nacimiento'] = DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
	}
	
	public function getFechaFinAttribute() {
		if(!isset($this->attributes['fecha_fin']))
			return "";
		return DateTime::createFromFormat('Y-m-d', $this->attributes['fecha_fin'])->format('d/m/Y');
//		return '15/10/2014';
	}
	
	public function setFechaFinAttribute($value) {
		if(empty($value))
			$this->attributes['fecha_fin'] = date("Y-m-d");
		elseif ( preg_match("/^[0-9]{2}\\/[0-9]{2}\\/[0-9]{4}$/", $value ) )
			$this->attributes['fecha_fin'] = DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
	}
	// </editor-fold



}
