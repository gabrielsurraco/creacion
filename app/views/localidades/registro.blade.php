@section('content')

<h3>Edici&oacute;n de Localidades</h3><hr/>

<div class="content">
	{{ Form::open(array('method' => 'POST'), array('role' => 'form')) }}
	@if($accion=="update")
		{{ Form::hidden('id', $id) }}
	@endif
	<div class="row">
		<div class="form-group col-md-4">
			{{ Form::label('nombre', 'Nombre de la Localidad') }}
			@if($accion=="add")
			{{ Form::text('nombre', null, array('placeholder' => 'Introduzca el nombre de la Localidad', 'class' => 'form-control')) }}
			@else
			{{ Form::text('nombre', $datos->nombre, array('placeholder' => 'Introduzca el nombre de la Localidad', 'class' => 'form-control')) }}
			@endif
			<p class="alert-danger">
				{{$errors->first("nombre")}}
			</p>
		</div>
	</div>
        
            {{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}
            {{ HTML::link("localidades","Cancelar",array("class"=>"btn btn-default")) }}
	{{ Form::close() }}
   
</div>
@stop