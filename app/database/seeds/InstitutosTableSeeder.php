<?php

class InstitutosTableSeeder extends Seeder {

	public function run() {
		DB::table('institutos')->delete();
		Instituto::create(array(
			'nombre' => 'Hospital 1'
		));
		
		Instituto::create(array(
			'nombre' => 'Hospital 2'
		));
		
		Instituto::create(array(
			'nombre' => 'Hospital 3'
		));
		
	}

}
