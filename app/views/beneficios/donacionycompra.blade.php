@section('content')

<h3>Registro de Donaciones y/o Compras</h3><hr/>

<div class="content">
    {{ Form::open(array('action' => 'BeneficiosController@postDonacionycompra', 'method' => 'POST')) }}
	{{ Form::hidden('id_paciente',$id) }}
	
	<div class="row">
		<div class="col-md-3">
			{{ Form::label('fecha','Fecha') }}
			{{ Form::text('fecha', null, array('placeholder' => 'dd/mm/aaaa', 'class' => 'form-group form-control datepicker' , 'autocomplete' => 'off')) }}
			<p class="alert-danger">{{$errors->first("fecha")}}</p>
		</div>
		<div class="col-md-3">
			{{ Form::label('descripcion','Descripción') }}
			{{ Form::text('descripcion', null, array('placeholder' => 'Descripción...', 'class' => 'form-group form-control', 'autocomplete' => 'off')) }}
			<p class="alert-danger">{{$errors->first("descripcion")}}</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			{{ Form::label('cantidad','Cantidad') }}
			{{ Form::number('cantidad', null, array('placeholder' => 'Cantidad', 'class' => 'form-group form-control', 'autocomplete' => 'off')) }}
			<p class="alert-danger">{{$errors->first("cantidad")}}</p>
		</div>
		<div class="col-md-3">
			{{ Form::label('montototal','Monto Total (Opcional)') }}
			{{ Form::text('montototal', null, array('placeholder' => '$0.00', 'class' => 'form-group form-control', 'autocomplete' => 'off')) }}
			<p class="alert-danger">{{$errors->first("montototal")}}</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			{{ Form::label('procedencia','Procedencia') }}
			{{ Form::select('procedencia', ["Compra", "Donacion"], null, ['class' => 'form-group form-control']) }}
		</div>
	</div>
	
	<div class="row">
		<div class="form-group col-md-3">
			{{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}
			<?php echo HTML::link('pacientes/beneficios/'.$id.'','Cancelar',['class' => 'btn btn-default']) ?>
		</div>
	</div>
	{{ Form::close() }}
</div>
@show

@section('js')
<script>
	$(function(){
	  $(".tablesorter").tablesorter();
	});
</script>
@stop