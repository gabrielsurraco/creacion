@section('content')

<h3>Notas</h3><hr/>

<div class="content">
    <div class="row">
        <div class="col-lg-6">
            <ul>
				<li><a href="notas/download/1"><i class="icon-download-alt"> </i> Ficha del Paciente </a></li>
				<li><a href="notas/download/2"><i class="icon-download-alt"> </i> Nota Oficial Creaci&oacute;n </a></li>
			</ul>
        </div>
    </div>
</div>
@show

@section('js')
<script>
	$(function(){
	  $(".tablesorter").tablesorter();
	});
</script>
@stop