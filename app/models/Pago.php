<?php

use Carbon\Carbon;

class Pago extends Eloquent{
	protected $guarded = array("id", "created_at", "updated_at");
	protected $table = 'pagos';
	
	
	
	public function getFechaPagoAttribute() {
		if(!isset($this->attributes['fecha_pago']))
			return "";
		return DateTime::createFromFormat('Y-m-d', $this->attributes['fecha_pago'])->format('d/m/Y');
	}
	
	public function setFechaPagoAttribute($value) {
		if(empty($value))
			$this->attributes['fecha_pago'] = date("Y-m-d");
		elseif ( preg_match("/^[0-9]{2}\\/[0-9]{2}\\/[0-9]{4}$/", $value ) )
			$this->attributes['fecha_pago'] = DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
	}
	
	public function colaborador()
    {
        return $this->belongsTo('Colaborador', 'colaborador_id');
    }



}
