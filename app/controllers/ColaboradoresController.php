<?php

class ColaboradoresController extends BaseController {

	public function getSendmailbirthday($id = null) {
		
		$colaborador = Colaborador::findOrFail($id);
		Mail::later( 100, 'colaboradores.mail.birthday', array('firstname' => $colaborador->nombre), function($message) use ($colaborador) {
			$message->to($colaborador->email, $colaborador->nombre)
					->subject('Feliz Cumpleaños!');
		});

		return Redirect::to('dashboard/principal')->with('success', 'Saludo enviado correctamente!');
	}

	public function getSendmailrememberfee($id = null) {
		$colaborador = Colaborador::findOrFail($id);
		Mail::later( 100, 'colaboradores.mail.cuota', array('firstname' => $colaborador->nombre), function($message) use ($colaborador) {
			$message->to($colaborador->email, $colaborador->nombre)
					->subject('Asociación Civil Creación!');
		});

		return Redirect::to('dashboard/principal')->with('success', 'Recordatorio enviado correctamente!');
	}

	public function getInfo($id = null) {
		$usuario = Auth::user();

		if (!($usuario->es_admin || $usuario->es_default)) {
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}

		$colaborador = Colaborador::findOrFail($id);
		$pagos = Pago::where("colaborador_id", $id)->orderBy("fecha_pago", "DESC")->limit(7)->get();
		$prestamos = Prestamo::where("colaborador_id", $id)->where("paciente_id", null)->where("fecha_devolucion", null)->get();

		$this->layout->content = View::make("colaboradores.info", compact("colaborador", "prestamos", "pagos"));
	}

	public function postAgregarpago() {
		$usuario = Auth::user();

		if (!($usuario->es_admin)) {
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}

		$inputs = Input::all();

		$reglas = array();

		$reglas['fecha_pago'] = 'required';

		$mensajes = array(
			'required' => 'Debe completar el campo!'
		);

		$validar = Validator::make($inputs, $reglas, $mensajes);

		if ($validar->fails()) {
			return Redirect::back()->withErrors($validar)->withInput();
		}

		$pago = new Pago();

		$pago->colaborador_id = $inputs["id"];
		$pago->fecha_pago = $inputs["fecha_pago"];
		if (!empty($inputs["monto"]))
			$pago->monto = $inputs["monto"];
		if (!empty($inputs["observaciones"]))
			$pago->observaciones = $inputs["observaciones"];

		if (!$pago->save()) {
			return Redirect::to('colaboradores')->with('error', 'No se pudo actualizar el pago');
		}

		return Redirect::to('colaboradores')->with('success', 'Pago Actualizado correctamente');
	}

	public function getAgregarpago($id = null) {
		$usuario = Auth::user();

		if (!($usuario->es_admin)) {
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}

		$id = strip_tags($id);

		$colaborador = Colaborador::findOrFail($id);

		$view = View::make('colaboradores/agregarpago');
		$view->with(compact("id", "usuario", "colaborador"));
//        if(empty($id)){
//            $view->with(array(
//                "accion" => "add"
//                , "id" => ""
//            ));
//        }else{
//            $datos = Usuario::findOrFail($id);
//            $accion = "update";
//            $view->with(compact("datos", "id", "accion"));
//        }
//        
		$this->layout->content = $view;
	}

	public function generar_informe($filtros) {
//		var_dump($filtros);die;
		if (isset($filtros['es_socio'])) {
			$query = Colaborador::select("nombre", "dni", "fecha_ingreso", "profesion", "domicilio", "localidad_id", "tel_contacto", "es_socio", "es_voluntario", "es_benefactor", "email", "lugar_de_pago", "colabora_en")->where("es_socio", true);
		} else {
			$query = Colaborador::select("nombre", "dni", "fecha_ingreso", "profesion", "domicilio", "localidad_id", "tel_contacto", "es_socio", "es_voluntario", "es_benefactor", "email", "lugar_de_pago", "colabora_en")->where("es_socio", false);
		}

		if (isset($filtros['es_voluntario'])) {
			$query->where("es_voluntario", true);
		} else {
			$query->where("es_voluntario", false);
		}

		if (isset($filtros['es_benefactor'])) {
			$query->where("es_benefactor", true);
		} else {
			$query->where("es_benefactor", false);
		}

		if (!empty($filtros['nombre']))
			$query->where("nombre", "LIKE", "%" . $filtros["nombre"] . "%");

		if (!empty($filtros['colabora_en']))
			$query->where("colabora_en", "LIKE", "%" . $filtros["colabora_en"] . "%");

		if (!empty($filtros['dni']))
			$query->where('dni', $filtros['dni']);

		$query->orderBy("nombre", "asc");

		$colaboradores = $query->get();

		foreach ($colaboradores as $colaborador) {
			if ($colaborador["es_socio"] == 1)
				$colaborador["es_socio"] = "Si";
			else
				$colaborador["es_socio"] = "No";

			if ($colaborador["es_voluntario"] == 1)
				$colaborador["es_voluntario"] = "Si";
			else
				$colaborador["es_voluntario"] = "No";

			if ($colaborador["es_benefactor"] == 1)
				$colaborador["es_benefactor"] = "Si";
			else
				$colaborador["es_benefactor"] = "No";
		}

		Excel::create('Colaboradores', function($excel) use($colaboradores) {
			$excel->sheet('Sheetname', function($sheet) use($colaboradores) {


//				$sheet->cells('A1:M1', function($cells) {
//					
//				});
//				$sheet->setFontBold(true);
				$sheet->fromArray($colaboradores);
			});
		})->export('xls');
	}

	public function postIndex() {
		return $this->getIndex();
	}

	public function getIndex($imprimir = null) {
		$usuario = Auth::user();

		if (!($usuario->es_admin || $usuario->es_default)) {
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}


		if (isset($imprimir)) {
			if (Request::isMethod('post'))
				$filtros = Input::all();
			else
				$filtros = Session::get('colaboradores.index.filtros', array());

			$this->generar_informe($filtros);
		}

		// Recupero filtros
		if (Request::isMethod('post')) {
			$filtros = Input::all();
//			return $filtros;
		} else {
			if (Input::has('page')) {
				$filtros = Session::get('colaboradores.index.filtros', array());
			} else {
				$filtros = array();
				Session::forget('colaboradores.index.filtros');
			}
		}




		if (isset($filtros['es_socio'])) {
			$query = Colaborador::where("es_socio", true);
		} else {
			$query = Colaborador::where("es_socio", false);
		}

		if (isset($filtros['es_voluntario'])) {
			$query->where("es_voluntario", true);
		} else {
			$query->where("es_voluntario", false);
		}

		if (isset($filtros['es_benefactor'])) {
			$query->where("es_benefactor", true);
		} else {
			$query->where("es_benefactor", false);
		}

		if (!empty($filtros['nombre']))
			$query->where("nombre", "LIKE", "%" . $filtros["nombre"] . "%");

		if (!empty($filtros['colabora_en']))
			$query->where("colabora_en", "LIKE", "%" . $filtros["colabora_en"] . "%");

		if (!empty($filtros['dni']))
			$query->where('dni', $filtros['dni']);
//		
		$query->orderBy("nombre", "asc");

		$colaboradores = $query->paginate(5);

		// Guardo filtro en sesion
		if (!Input::has('page') && count($filtros) > 0) {
			Session::put('colaboradores.index.filtros', $filtros);
		}

		$this->layout->content = View::make('colaboradores/index')->with(compact("colaboradores", 'filtros'));
	}

	public function getEditar($id) {

		$usuario = Auth::user();

		// Validaciones
		if (!($usuario->es_admin)) {
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}

		//Cargo Localidades
		$localidades = Localidad::all()->toArray();
		$lista_localidades = array();
		foreach ($localidades as $localidad) {
			$lista_localidades[$localidad["id"]] = $localidad["nombre"];
		}

		$colaborador = Colaborador::findOrFail($id);



		$this->layout->content = View::make('colaboradores/editar')->with(compact("colaborador", "lista_localidades"));
	}

	public function getCrear() {
		$usuario = Auth::user();
		if (!($usuario->es_admin)) {
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}

		//Cargo Localidades
		$localidades = Localidad::all()->toArray();
		$lista_localidades = array();
		foreach ($localidades as $localidad) {
			$lista_localidades[$localidad["id"]] = $localidad["nombre"];
		}

		$colaborador = new Colaborador();

//		Carbon::today()->addMonth()->day(1)->format("d/m/Y");

		$this->layout->content = View::make('colaboradores.editar')->with(compact("colaborador", "lista_localidades"));
	}

	public function postUpdate($id = null) {

		$usuario = Auth::user();

		if (!($usuario->es_admin)) {
			return Redirect::to('home')->with('error', 'Acceso no permitido');
		}

		$inputs = Input::all();
		$reglas = array();

		$reglas['nombre'] = 'required';
		if ($id == null)
			$reglas['dni'] = 'required|unique:colaboradores|numeric';
		else
			$reglas['dni'] = 'required|numeric';
		$reglas['fecha_ingreso'] = 'required';
		$reglas['fecha_nacimiento'] = 'required';
		$reglas['tel_contacto'] = 'required';
		$reglas['email'] = 'required';

		$mensajes = array(
			'required' => 'Debe completar el campo!',
			'unique' => 'El colaborador ya existe!',
			'numeric' => 'El dni debe ser un número'
		);

		$validar = Validator::make($inputs, $reglas, $mensajes);

		if ($validar->fails()) {
			return Redirect::back()->withErrors($validar)->withInput();
		}

		if (is_null($id)) {

			// Alta
			$colaborador = new Colaborador($inputs);
			if (!$colaborador->save()) {
				return Redirect::back()->withErrors($colaborador->errors())->withInput()->with(compact("colaborador"));
			}

			return Redirect::to('colaboradores')->with('success', 'El colaborador ha sido guardado correctamente');
		} else {

			// Modificación
			$colaborador = Colaborador::findOrFail($id);

			if ($usuario->es_admin) {
				
				$colaborador->fill(Input::only(
					'nombre', 'dni', 'fecha_nacimiento', 'profesion', 'domicilio', 'localidad_id', 'tel_contacto', 'email', 'lugar_de_pago', 'fecha_ingreso', 'es_socio', 'es_voluntario', 'es_benefactor', 'colabora_en')
				);
				
				try {
					if (!$colaborador->update($inputs))
						return Redirect::back()->withErrors($colaborador->errors());
					else
						return Redirect::to('colaboradores/editar/' . $colaborador->id)->with('success', 'El colaborador ha sido guardado correctamente');
				} catch (ValidationException $e) {
					return Redirect::back()->with('error', $e->getMessage())->withInput();
				}
			}
		}
	}

}
