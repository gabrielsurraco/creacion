@section('content')

<h3>Editado de Asistencia</h3><hr/>

<div class="content">
	{{ Form::open(array('method' => 'POST'), array('role' => 'form')) }}
	{{ Form::hidden("asistencia_id", $datos->id) }}
	<div class="row">
		<div class="form-group col-md-2">
			{{ Form::label('fecha_inicio', 'Fecha de Inicio') }}
			{{ Form::text('fecha_inicio', $datos->fecha_inicio, array('placeholder' => 'Ingrese Fecha', 'class' => 'form-control datepicker' , 'autocomplete' => 'off')) }}
			<p class="alert-danger">
				{{$errors->first("fecha_inicio")}}
			</p>
		</div>
		<div class="form-group col-md-2">
			{{ Form::label('fecha_cierre', 'Fecha de Cierre') }}
			{{ Form::text('fecha_cierre', $datos->fecha_cierre, array('placeholder' => 'Ingrese Fecha', 'class' => 'form-control datepicker' , 'autocomplete' => 'off')) }}
			<p class="alert-danger">
				{{$errors->first("fecha_cierre")}}
			</p>
		</div>
		<div class="form-group col-md-2">
			{{ Form::label('nro_asistencias', 'Número de Asistencias') }}
			{{ Form::number('nro_asistencias', $datos->nro_asistencias , array('placeholder' => 'Nro. de Asistencias', 'class' => 'form-control' , 'autocomplete' => 'off')) }}
			<p class="alert-danger">
				{{$errors->first("nro_asistencias")}}
			</p>
		</div>
	</div>
	<div class="row">
		<div class="form-group col-md-4">
			{{ Form::label('observaciones', 'Descripción (Opcional)') }}
			{{ Form::textarea('observaciones', $datos->observaciones, array('placeholder' => 'Descripcion...', 'class' => 'form-control' , 'autocomplete' => 'off', 'rows' => '4')) }}
		</div>
	</div>
    <div class="row">
		<div class="form-group col-md-3">
            {{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}
            {{ HTML::link("pacientes/info/".$datos->paciente_id,"Cancelar",array("class"=>"btn btn-default")) }}
		</div>
	</div>
	{{ Form::close() }}
   
</div>
@stop