<?php

class StockTableSeeder extends Seeder {

	public function run() {
		DB::table('stock')->delete();
		Stock::create(array(
			'tipo' => 'alimento',
			'cantidad' => 0,
			'usuario_actualizo' => 'gabriel_surraco@hotmail.com',
		));
		
		Stock::create(array(
			'tipo' => 'ropero_nuevo',
			'cantidad' => 0,
			'usuario_actualizo' => 'gabriel_surraco@hotmail.com',
		));
		
		Stock::create(array(
			'tipo' => 'ropero_usado',
			'cantidad' => 0,
			'usuario_actualizo' => 'gabriel_surraco@hotmail.com',
		));
		
		Stock::create(array(
			'tipo' => 'juguete_nuevo',
			'cantidad' => 0,
			'usuario_actualizo' => 'gabriel_surraco@hotmail.com',
		));
		
		Stock::create(array(
			'tipo' => 'juguete_usado',
			'cantidad' => 0,
			'usuario_actualizo' => 'gabriel_surraco@hotmail.com',
		));
		
		
	}

}
