<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHermanosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hermanos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('paciente_id');
			$table->string('nombre', 60);
			$table->string('dni', 60);
			$table->date('fecha_nacimiento');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists("hermanos");
	}

}
