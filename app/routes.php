<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */

// Login
Route::get('login', array('uses' => 'HomeController@showLogin'));
Route::post('login', array('uses' => 'HomeController@doLogin'));

// Acceso restringido
Route::group(array('before' => 'auth'), function() {
	// Rutas ad-hoc
	Route::get('/', function() {
		return Redirect::to('home');
	});
	Route::get('logout', array('uses' => 'HomeController@doLogout'));
	Route::get('home', array('uses' => 'DashboardController@index'));
//	Route::get('asistencias/generar', array('uses' => 'AsistenciasController@generar'));
	// Controllers

	Route::controller("asistencias", "AsistenciasController");
	Route::controller("dashboard", "DashboardController");

	Route::controller("beneficios", "BeneficiosController");
	Route::controller("stock", "StockController");
	Route::controller("notas", "NotasController");
	Route::controller("colaboradores", "ColaboradoresController");
	Route::controller("colaboradores", "ColaboradoresController", array('postUpdate' => 'colaboradores.update'));
	
	
//	Route::controller("colaboradores","ColaboradoresController", array('sendMailBirthday' => 'colaboradores.sendmailbirthday'));
//	Route::controller("colaboradores","ColaboradoresController", array('sendMailRememberFee' => 'colaboradores.sendmailrememberfee'));
//	Route::controller("colaboradores","ColaboradoresController", array('postAgregarpago' => 'colaboradores.agregarpago'));
	Route::controller("usuarios", "UsuariosController");
	Route::controller("diagnosticos", "DiagnosticosController");
	Route::controller("institutos", "InstitutosController");
	Route::controller("localidades", "LocalidadesController");
	Route::controller("pacientes", "PacientesController", array('postUpdate' => 'pacientes.update'));
	Route::controller("pacientes", "PacientesController");
});

// Composers
View::composer('layouts.master', function($view) {
	$view->with('usuario', Auth::user());
});
