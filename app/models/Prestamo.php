<?php


class Prestamo extends Eloquent{
	// <editor-fold desc="Configuración" defaultstate="collapsed">

	protected $guarded = array("id", "created_at", "updated_at");

	protected $table = 'prestamos';
	
	public function paciente()
    {
        return $this->belongsTo('Paciente', 'paciente_id');
    }
	
	public function colaborador()
    {
        return $this->belongsTo('Colaborador', 'colaborador_id');
    }
	
	public function getFechaSalidaAttribute() {
		if(!isset($this->attributes['fecha_salida']))
			return "";
		return DateTime::createFromFormat('Y-m-d', $this->attributes['fecha_salida'])->format('d/m/Y');
	}
	
	public function setFechaSalidaAttribute($value) {
		if(empty($value))
			$this->attributes['fecha_salida'] = date("Y-m-d");
		elseif ( preg_match("/^[0-9]{2}\\/[0-9]{2}\\/[0-9]{4}$/", $value ) )
			$this->attributes['fecha_salida'] = DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
	}
	
	public function getFechaDevolucionAttribute() {
		if(!isset($this->attributes['fecha_devolucion']))
			return "";
		return DateTime::createFromFormat('Y-m-d', $this->attributes['fecha_devolucion'])->format('d/m/Y');
	}
	
	public function setFechaDevolucionAttribute($value) {
		if(empty($value))
			$this->attributes['fecha_devolucion'] = date("Y-m-d");
		elseif ( preg_match("/^[0-9]{2}\\/[0-9]{2}\\/[0-9]{4}$/", $value ) )
			$this->attributes['fecha_devolucion'] = DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
	}

}
