@section('content')

<h3>Administraci&oacute;n de Usuarios</h3><hr/>

<div class="content">
	{{ Form::open(array('method' => 'POST'), array('role' => 'form')) }}
	@if($accion=="update")
		{{ Form::hidden('id', $id) }}
	@endif
	<div class="row">
		<div class="form-group col-md-4">
			{{ Form::label('nombre', 'Nombre completo') }}
			@if($accion=="add")
			{{ Form::text('nombre', null, array('placeholder' => 'Introduce tu nombre y apellido', 'class' => 'form-control')) }}
			@else
			{{ Form::text('nombre', $datos->nombre, array('placeholder' => 'Introduce tu nombre y apellido', 'class' => 'form-control')) }}
			@endif
			<p class="alert-danger">
				{{$errors->first("nombre")}}
			</p>
		</div>

		<div class="form-group col-md-4">

			{{ Form::label('email', 'Dirección de E-mail') }}
			@if($accion=="add")
			{{ Form::text('email', null, array('placeholder' => 'Introduce tu E-mail', 'class' => 'form-control', 'autocomplete' => 'off')) }}
			@else
			{{ Form::text('email',$datos->email, array('placeholder' => 'Introduce tu E-mail', 'class' => 'form-control', 'autocomplete' => 'off')) }}
			@endif

			<p class="alert-danger">
				{{$errors->first("email")}}
			</p>
		</div>
	</div>
	<div class="row">

		<div class="form-group col-md-4">
			{{ Form::label('password', 'Contraseña') }}
			{{ Form::password('password', array('class' => 'form-control', 'autocomplete' => 'off')) }}
			<p class="alert-danger">
				{{$errors->first("password")}}
			</p>
		</div>

		<div class="form-group col-md-4">
			{{ Form::label('password_confirmation', 'Confirmar contraseña') }}
			{{ Form::password('password_confirmation', array('class' => 'form-control')) }}
			<p class="alert-danger">
				{{$errors->first("password")}}
			</p>
		</div>
	</div>
        @if(Auth::user()->es_admin == 1)
        <div class="row">
                    <div class="form-group col-md-4">
                        {{Form::label('roles','Tipo de Usuario')}}
                        <div class="checkbox">
                            <label>

                                @if($accion=="add")
                                    {{ Form::checkbox('admin', true , false) }}
                                @else
                                    {{ Form::checkbox('admin', true , $datos->es_admin) }}
                                @endif
                                Administrador
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>

                                @if($accion=="add")
                                    {{ Form::checkbox('default', true , false) }}
                                @else
                                    {{ Form::checkbox('default', true , $datos->es_default) }}
                                @endif
                                Estándar
                            </label>
                        </div>
                        
                        
                    </div>
            </div>
        @endif
		<div class="row">
			<div class="form-group col-lg-4">
				{{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}
				{{ HTML::link("usuarios","Cancelar",array("class"=>"btn btn-default")) }}
			</div>
		</div>
            
	{{ Form::close() }}
   
</div>
@stop